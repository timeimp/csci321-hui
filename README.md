Building:
 * Open `Development/Sandbox/Sandbox.sln`.
 * Build all projects.
 
 * Open `Development/Apps/Apps.sln`.
 * Right click on the project you are developing.
 * Select `Set as startup project`.
 * Press `F5` to run the program.
