/*
===============================================================================

	Default.frag
		Generic fragment shader.

===============================================================================
*/
#version 110

// Uniforms.
uniform vec4		uColor;
uniform sampler2D 	uTexture0;
uniform bool		uIsColored;
uniform bool		uIsTextured;

varying	vec2		frag_TexCoord;

/*
===============================================================================
main
	Shader entry point.
===============================================================================
*/
void main()
{
	gl_FragColor = vec4( 1.0, 1.0, 1.0, 1.0 );
	
	if( uIsColored )
	{
		gl_FragColor = uColor;
	}
	
	if( uIsTextured )
	{
		gl_FragColor *= texture2D( uTexture0, frag_TexCoord );
	}
}
