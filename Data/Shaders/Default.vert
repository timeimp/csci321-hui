/*
===============================================================================

	Default.vert
		Generic vertex shader.

===============================================================================
*/
#version 110

// Vertex shader outputs.
varying	vec2		frag_TexCoord;

/*
===============================================================================
main
	Shader entry point.
===============================================================================
*/
void main()
{
	gl_Position = gl_ProjectionMatrix * gl_ModelViewMatrix * gl_Vertex;
	
	// Prepare fragment shader inputs.
	frag_TexCoord = gl_MultiTexCoord0.st;
}
