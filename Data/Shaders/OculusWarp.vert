uniform mat4	Texm;
varying	vec2	oTexCoord;

void main()
{
	gl_Position = gl_ProjectionMatrix * gl_ModelViewMatrix * gl_Vertex;
	oTexCoord = vec2(Texm * vec4(gl_MultiTexCoord0.xy, 0, 1));
}
