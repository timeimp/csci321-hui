/*
===============================================================================

	Cast.h
		Type casting utilities.

===============================================================================
*/

#ifndef __CAST_H__
#define __CAST_H__

namespace sandbox
{
	namespace framework
	{

		#define i2fl( x )			(float)( x )
		#define fl2i( x )			(int)( x )
		#define db2fl( x )			(float)( x )

	} // namespace framework;
} // namespace sandbox;

#endif // __CAST_H__