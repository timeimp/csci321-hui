/*
===============================================================================

	Sandbox.h
		Application-wide shared header.

===============================================================================
*/

#ifndef __SANDBOX_H__
#define __SANDBOX_H__

#include "System/Debug.h"
#include "Framework/Cast.h"

#define sbCLAMP( val, min, max )	( (val) < (min) ? (min) : (val) > (max) ? (max) : (val) )
#define sbMIN( a, b )				( (a) < (b) ? (a) : (b) )
#define sbMAX( a, b )				( (a) > (b) ? (a) : (b) )

#define sbFLAG(b)					( 1 << (b) )

#define sbTEST_FLAG16( f, b )		( (f) & (short)sbFLAG(b) )
#define sbSET_FLAG16( f, b, v )		( (v) ? ((f) |= (short)sbFLAG(b)) : ((f) &= (short)~sbFLAG(b)) )

#define sbTEST_FLAG32( f, b )		( (f) & (int)sbFLAG(b) )
#define sbSET_FLAG32( f, b, v )		( (v) ? ((f) |= (int)sbFLAG(b)) : ((f) &= (int)~sbFLAG(b)) )

#endif // __SANDBOX_H__