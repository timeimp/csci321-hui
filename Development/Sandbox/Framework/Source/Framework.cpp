/*
===============================================================================

	Framework.h
		High level framework related functions.

===============================================================================
*/

#include "Framework/Sandbox.h"
#include "Framework/Application.h"
#include "System/Timer.h"
#include "System/OpenGL.h"
#include "Render/Render.h"
#include "Render/Primitive.h"

using namespace sandbox::system;
using namespace sandbox::render;
using namespace sandbox::framework;

/*
===============================================================================
framework_init
	Readies the core framework elements before the application is created.
===============================================================================
*/
frameworkInit_t sandbox::framework::framework_init( const appInit_t &init )
{
	frameworkInit_t f;

	// Create the main window.
	f.pWindow = Window::Create( 0, 0, init.windowWidth, init.windowHeight, init.windowTitle );
	f.pWindow->Center();

	// Ready rendering.
	GlContext::Create( f.pWindow );
	Render::Create();
	Primitive::Create();

	Timer::Start();

	return f;
}

/*
===============================================================================
framework_destroy
	Destroys framework elements after application destruction.
===============================================================================
*/
void sandbox::framework::framework_destroy( frameworkInit_t *pFramework )
{
	sbAssert( pFramework );

	Primitive::Destroy();
	Render::Destroy();
	GlContext::Destroy();
	delete pFramework->pWindow;

	Timer::Stop();
}