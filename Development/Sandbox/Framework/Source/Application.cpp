/*
===============================================================================

	Application.cpp
		Implements the primitive rendering interface.

===============================================================================
*/

#include "Framework/Sandbox.h"
#include "Framework/Application.h"
#include "Render/Primitive.h"
#include "Render/Render.h"

#include <string>

using namespace std;
using namespace sandbox::drivers;
using namespace sandbox::framework;
using namespace sandbox::system;
using namespace sandbox::render;

/*
===============================================================================
	appInit_t implementation.
===============================================================================
*/

appInit_t::appInit_t()
{
	windowWidth = 640;
	windowHeight = 480;
	windowTitle = "Sandbox";

	argc = 0;
	argv = NULL;
}

/*
===============================================================================
	Application implementation.
===============================================================================
*/

Application *Application::_pApp = NULL;

/*
===============================================================================
Application::Application
	Initializes the application state.
===============================================================================
*/
Application::Application( const appInit_t &init )
{
	_init = init;
	_pApp = this;

	_tickCount = 0;
	_pWindow = NULL;
}

/*
===============================================================================
Application::Initialize
	Readies the application for use.
===============================================================================
*/
void Application::Initialize( const frameworkInit_t &framework )
{
	_pWindow = framework.pWindow;
	_pOculusRift = new OculusRift;

	// Ready the top level application.
	AppInitialize();
}

/*
===============================================================================
Application::Run
	Handles the runtime state of the application.
===============================================================================
*/
void Application::Run()
{
	tick_t nextFrameTick = 0;

	while( true )
	{
		// Process system events.
		if( !_pWindow->RunEvents() )
		{
			break;
		}

		if( nextFrameTick > Timer::AppTime() )
		{
			continue;
		}

		nextFrameTick = Timer::AppTime() + TICK_TIME_MS;

		// Run a game frame.
		if( !AppFrame() )
		{
			break;
		}

		// Display the last rendered frame to the screen.
		GlContext::Present();

		_tickCount++;
	}
}

/*
===============================================================================
Application::Destroy
	Cleans the application before returning control to the OS.
===============================================================================
*/
void Application::Destroy()
{
	delete _pOculusRift;

	// Destroy the top level application.
	AppDestroy();
}