/*
===============================================================================

	Application.h
		Class which handles base application functionality.
		Applications build using the framework must be derived off this class.

===============================================================================
*/

#ifndef __APPLICATION_H__
#define __APPLICATION_H__

#include "System/Timer.h"
#include "System/Window.h"
#include "Drivers/OculusRift.h"
#include "Framework/Framework.h"

#include <string>

namespace sandbox
{
	namespace framework
	{

		/*
		===============================================================================
		Application
			Implements common application behaviour.
		===============================================================================
		*/
		class Application
		{
		public:
			/*
			-------------------------------------------------------------------------
				Creation, destruction.
			-------------------------------------------------------------------------
			*/
							Application( const appInit_t &init );

			/*
			-------------------------------------------------------------------------
				Internals.
			-------------------------------------------------------------------------
			*/
			void			Initialize( const frameworkInit_t &framework );
			void			Run();
			void			Destroy();

			/*
			-------------------------------------------------------------------------
				Accessors.
			-------------------------------------------------------------------------
			*/
			inline system::tick_t	TickCount() const		{ return _tickCount; }
			inline system::Window *	AppWindow() const		{ return _pWindow; }
			inline drivers::OculusRift *GetOculusRift() const	{ return _pOculusRift; }

			static Application *GetApp()					{ return _pApp; }

		protected:
			/*
			-------------------------------------------------------------------------
				Derived class interface.
			-------------------------------------------------------------------------
			*/
			virtual bool	AppInitialize() = 0;
			virtual bool	AppFrame() = 0;
			virtual void	AppDestroy() = 0;

			static const int	TICKS_PER_SECOND = 60;
			static const int	TICK_TIME_MS = fl2i( ( 1.0f / TICKS_PER_SECOND ) * 1000.0f );

		private:
			/*
			-------------------------------------------------------------------------
				State.
			-------------------------------------------------------------------------
			*/
			appInit_t		_init;
			system::tick_t	_tickCount;
			system::Window *_pWindow;
			drivers::OculusRift *_pOculusRift;

			static Application *_pApp;
		};

		/*
		===============================================================================
		app_main
			Ensures core framework systems are initializaed before the application is created.
		===============================================================================
		*/
		template<class T>
		int app_main( const appInit_t &init )
		{
			// Ready the framework for use (OpenGL, system timer, etc).
			frameworkInit_t framework = framework_init( init );

			// Framwork is initialized, run the top level application.
			T *pApplication = new T( init );
			pApplication->Initialize( framework );
			pApplication->Run();
			pApplication->Destroy();
			delete pApplication;

			// Shutdown the framework.
			framework_destroy( &framework );

			return 0;
		}

	} // namespace framework;
} // namespace sandbox;

#endif // __APPLICATION_H__