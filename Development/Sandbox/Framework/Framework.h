/*
===============================================================================

	Framework.h
		High level framework related functions.

===============================================================================
*/

#ifndef __FRAMEWORK_H__
#define __FRAMEWORK_H__

#include "System/Window.h"

#include <string>

namespace sandbox
{
	namespace framework
	{

		/*
		===============================================================================
		appInit_t
			Data which assists the application with initialization.
		===============================================================================
		*/
		struct appInit_t
		{
							appInit_t();

			// Default window options.
			int				windowWidth, windowHeight;
			std::string		windowTitle;

			// Command line arguments.
			int				argc;
			char **			argv;
		};

		/*
		===============================================================================
		frameworkInit_t
			Framework created objects which are to be passed to the application.
		===============================================================================
		*/
		struct frameworkInit_t
		{
			system::Window *	pWindow;
		};
		
		frameworkInit_t framework_init( const appInit_t &init );
		void framework_destroy( frameworkInit_t *pFramework );

	} // namespace framework;
} // namespace sandbox;

#endif // __FRAMEWORK_H__