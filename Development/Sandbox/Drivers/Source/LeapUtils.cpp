/*
===============================================================================

	LeapHand.cpp
		Implements the Sandbox side hand class.

===============================================================================
*/

#include "Framework/Sandbox.h"
#include "Drivers/LeapMotion.h"
#include "System/Thread.h"

#include "LeapUtils.h"

using namespace Leap;
using namespace sandbox::math;
using namespace sandbox::system;
using namespace sandbox::drivers;

static const float DISTANCE_SCALE = 10.0f;

/*
===============================================================================
leap_vector_to_sandbox_point
	Converts a Leap SDK vector into Sandbox units.
===============================================================================
*/
Point3f sandbox::drivers::leap_vector_to_sandbox_point( const Leap::Vector &vector )
{
	float x = vector.x / DISTANCE_SCALE;
	float y = vector.y / DISTANCE_SCALE;
	float z = vector.z / DISTANCE_SCALE;
	return Point3f( x, y, z );
}

/*
===============================================================================
leap_vector_to_sandbox_angle
	Converts a Leap SDK angle into Sandbox units.
===============================================================================
*/
Angle3f sandbox::drivers::leap_vector_to_sandbox_angle( const Leap::Vector &angle )
{
	return Angle3f( angle.pitch(), angle.roll(), angle.yaw() );
}

/*
===============================================================================
leap_vector_to_sandbox_vector
	Converts a Leap SDK angle into Sandbox units.
===============================================================================
*/
Vector3f sandbox::drivers::leap_vector_to_sandbox_vector( const Leap::Vector &angle )
{
	return Vector3f( angle.x, angle.y, angle.z );
}

/*
===============================================================================
leap_distance_to_sandbox
	Converts a Leap SDK distance into Sandbox units.
===============================================================================
*/
float sandbox::drivers::leap_distance_to_sandbox( float dist )
{
	return dist / DISTANCE_SCALE;
}