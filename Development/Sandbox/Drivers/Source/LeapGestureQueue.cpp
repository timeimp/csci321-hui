/*
===============================================================================

	LeapGestureQueue.cpp
		Gesture accessor for the Sandbox application.

===============================================================================
*/

#include "Framework/Sandbox.h"
#include "Drivers/LeapMotion.h"
#include "System/Thread.h"

using namespace Leap;
using namespace sandbox::system;
using namespace sandbox::drivers;

/*
===============================================================================
	LeapGestureQueue implementation.
===============================================================================
*/

/*
===============================================================================
LeapGestureQueue::LeapGestureQueue
	Readies the LeapGestureQueue instance for use.
===============================================================================
*/
LeapGestureQueue::LeapGestureQueue()
{
	_mutex = Mutex::Create();
}

/*
===============================================================================
LeapGestureQueue::~LeapGestureQueue
	Cleans the LeapMotion instance.
===============================================================================
*/
LeapGestureQueue::~LeapGestureQueue()
{
	delete _mutex;
}

/*
===============================================================================
LeapGestureQueue::Push
	Adds a new gesture into the queue.
===============================================================================
*/
void LeapGestureQueue::Push( const Leap::Gesture &gesture )
{
	if( _mutex->IsLocked() )
	{
		_queue.push( gesture );
	}
}

/*
===============================================================================
LeapGestureQueue::Pop
	Provides access to, then removes the first gesture from the queue.
	Returns NULL if the queue is empty.
===============================================================================
*/
Gesture LeapGestureQueue::Pop()
{
	if( _mutex->IsLocked() && _queue.size() )
	{
		Gesture front = _queue.front();
		_queue.pop();
		return front;
	}

	return Gesture::invalid();
}