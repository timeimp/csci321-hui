/*
===============================================================================

	LeapUtils.h
		Shared Leap driver code.

===============================================================================
*/

#ifndef __LEAPUTILS_H__
#define __LEAPUTILS_H__

#include "Math/Vector.h"

#include <LeapSDK/include/Leap.h>

namespace sandbox
{
	namespace drivers
	{

		math::Point3f leap_vector_to_sandbox_point( const Leap::Vector &vector );
		math::Angle3f leap_vector_to_sandbox_angle( const Leap::Vector &angle );
		math::Vector3f leap_vector_to_sandbox_vector( const Leap::Vector &vector );
		float leap_distance_to_sandbox( float dist );

	} // namespace drivers;
} // namespace sandbox;

#endif // __LEAPUTILS_H__