/*
===============================================================================

	LeapMotion.cpp
		Application level Leap Motion device interface implementation.

===============================================================================
*/

#include "Framework/Sandbox.h"
#include "Drivers/LeapMotion.h"

#include <memory>
#include <iostream>

using namespace std;
using namespace Leap;
using namespace sandbox::system;
using namespace sandbox::drivers;

/*
===============================================================================
	LeapListener implementation.
===============================================================================
*/

/*
===============================================================================
LeapListener::LeapListener
	Initializes a LeapListener instance.

	NOTE:	`leap` has not been fully initialized when this constructor is called.
			Don't modify `leap` in any way.
===============================================================================
*/
LeapListener::LeapListener( LeapMotion *leap )
{
	// Ensure we have a valid pointer.
	sbAssert( leap );

	_leap = leap;
}

/*
===============================================================================
LeapListener::onInit
	Called when `LeapListener` is first attached to a `Controller`.
===============================================================================
*/
void LeapListener::onInit( const Controller &controller )
{
#ifdef sbDEBUG
	cout << "LeapListener::onInit" << endl;
#endif
}

/*
===============================================================================
LeapListener::onConnect
	Called when a Leap Motion device is detected by the Leap SDK.
===============================================================================
*/
void LeapListener::onConnect( const Controller &controller )
{
#ifdef sbDEBUG
	cout << "LeapListener::onConnect" << endl;
#endif

	controller.enableGesture( Gesture::TYPE_CIRCLE );
	controller.enableGesture( Gesture::TYPE_KEY_TAP );
	controller.enableGesture( Gesture::TYPE_SCREEN_TAP );
	controller.enableGesture( Gesture::TYPE_SWIPE );
}

/*
===============================================================================
LeapListener::onDisconnect
	Called when a Leap Motion device is disconnected from the Leap SDK.
===============================================================================
*/
void LeapListener::onDisconnect( const Controller &controller )
{
#ifdef sbDEBUG
	cout << "LeapListener::onDisconnect" << endl;
#endif
}

/*
===============================================================================
LeapListener::onExit
	Called when this listener is removed from a controller, or when the 
	controller is destroyed.
===============================================================================
*/
void LeapListener::onExit( const Controller &controller )
{
#ifdef sbDEBUG
	cout << "LeapListener::onExit" << endl;
#endif
}

/*
===============================================================================
LeapListener::onFrame
	Called when a new frame of hand and finger data is available.
===============================================================================
*/
void LeapListener::onFrame( const Controller &controller )
{
	BuildFrameData( controller );
	PlaceGesturesInQueue( controller );
}

/*
===============================================================================
LeapListener::onFrame
	Called when this application is placed in focus.
===============================================================================
*/
void LeapListener::onFocusGained( const Controller &controller )
{

}

/*
===============================================================================
LeapListener::onFrame
	Called when this application has lost focus.
===============================================================================
*/
void LeapListener::onFocusLost( const Controller &controller )
{

}

/*
===============================================================================
LeapListener::BuildFrameData
	Updates frame specific data for the Leap interface.
===============================================================================
*/
void LeapListener::BuildFrameData( const Controller &controller )
{
	// Build the new frame data.
	LeapFrame frame( controller );

	// Send the new data to the Leap interface.
	_leap->SetFrame( frame );
}

/*
===============================================================================
LeapListener::PlaceGesturesInQueue
	Called when this application has lost focus.
===============================================================================
*/
void LeapListener::PlaceGesturesInQueue( const Controller &controller )
{
	const Frame &frame = controller.frame();
	const GestureList &gestures = frame.gestures();

	// Gain access to the gesture queue.
	LeapGestureQueue *pQueue = _leap->GetGestureQueue();
	pQueue->Lock();
	
	for( int i = 0 ; i < gestures.count() ; i++ )
	{
		// Add this gesture to the gesture queue.
		pQueue->Push( gestures[i] );
	}

	// Allow other threads access to the gesture queue.
	pQueue->Unlock();
}

/*
===============================================================================
LeapListener::CreateGesture
	Instanciates the correct gesture object given a gesture type.
===============================================================================
*/
Gesture *LeapListener::CreateGesture( const Gesture &gesture ) const
{
	Gesture::Type gestureType = gesture.type();

	switch( gestureType )
	{
		case Gesture::TYPE_SWIPE:
			return new SwipeGesture( gesture );

		case Gesture::TYPE_CIRCLE:
			return new CircleGesture( gesture );

		case Gesture::TYPE_SCREEN_TAP:
			return new ScreenTapGesture( gesture );

		case Gesture::TYPE_KEY_TAP:
			return new KeyTapGesture( gesture );
	}

	// Unknown gesture type.
	sbAssert( 0 );
	return NULL;
}