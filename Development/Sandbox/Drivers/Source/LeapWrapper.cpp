/*
===============================================================================

	LeapWrapper.cpp
		Simple wrappers around Leap classes.

===============================================================================
*/

#include "Framework/Sandbox.h"
#include "Drivers/LeapMotion.h"
#include "System/Thread.h"

#include "LeapUtils.h"

using namespace Leap;
using namespace sandbox::system;
using namespace sandbox::drivers;
using namespace sandbox::math;

/*
===============================================================================
	LeapPointable implementation.
===============================================================================
*/

/*
===============================================================================
LeapPointable::LeapPointable
	Default constructor.
===============================================================================
*/
LeapPointable::LeapPointable()
{
	
}

/*
===============================================================================
LeapPointable::LeapPointable
	Converts a Leap SDK pointable to Sandbox units.
===============================================================================
*/
LeapPointable::LeapPointable( const Leap::Pointable &pointable )
{
	// Convert base measurements.
	_tipPosition = leap_vector_to_sandbox_point( pointable.tipPosition() );

	_angles = leap_vector_to_sandbox_angle( pointable.direction() );
	_direction = leap_vector_to_sandbox_vector( pointable.direction() );

	_width = leap_distance_to_sandbox( pointable.width() );
	_length = leap_distance_to_sandbox( pointable.length() );
	_id = pointable.id();
	_timeVisible = pointable.timeVisible();
	
	// Determine type.
	if( pointable.isFinger() )
	{
		_type = POINTABLE_FINGER;
	}
	else if( pointable.isTool() )
	{
		_type = POINTABLE_TOOL;
	}

	// Build the finger base position.
	_basePosition = _tipPosition - ( _direction * _length );
}

/*
===============================================================================
	LeapHand implementation.
===============================================================================
*/

/*
===============================================================================
LeapHand::LeapHand
	Default constructor.
===============================================================================
*/
LeapHand::LeapHand()
{
	
}

/*
===============================================================================
LeapHand::LeapHand
	Converts a Leap SDK hand to Sandbox units.
===============================================================================
*/
LeapHand::LeapHand( const Leap::Hand &hand )
{
	// Convert base units.
	_position = leap_vector_to_sandbox_point( hand.palmPosition() );
	_angles = leap_vector_to_sandbox_angle( hand.direction() );
	_id = hand.id();

	// Add all hands.
	const PointableList &pointables = hand.pointables();
	for( int i = 0 ; i < pointables.count() ; i++ )
	{
		const Pointable &pointable = pointables[i];
		if( pointable.isFinger() )
		{
			LeapPointable p( pointable );
			_pointables.push_back( p );
		}
	}
}