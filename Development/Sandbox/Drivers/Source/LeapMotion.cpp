/*
===============================================================================

	LeapMotion.cpp
		Application level Leap Motion device interface implementation.

===============================================================================
*/

#include "Framework/Sandbox.h"
#include "Drivers/LeapMotion.h"
#include "System/Thread.h"

using namespace Leap;
using namespace sandbox::system;
using namespace sandbox::drivers;

/*
===============================================================================
	LeapMotion implementation.
===============================================================================
*/

/*
===============================================================================
LeapMotion::LeapMotion
	Readies the LeapMotion instance for use.
===============================================================================
*/
LeapMotion::LeapMotion()
{
	// Create variables used by `_pListener` first.
	_pFrameMutex = Mutex::Create();
	_pGestureQueue = new LeapGestureQueue;
	_pFrame = new LeapFrame;

	_pListener = new LeapListener( this );

	// Listen for Leap Motion events.
	_controller.addListener( *_pListener );
}

/*
===============================================================================
LeapMotion::~LeapMotion
	Cleans the LeapMotion instance.
===============================================================================
*/
LeapMotion::~LeapMotion()
{
	_controller.removeListener( *_pListener );

	delete _pListener;

	// Destroy variables used by `_pListener` last.
	delete _pGestureQueue;
	delete _pFrame;
	delete _pFrameMutex;
}

/*
===============================================================================
LeapMotion::GetFrame
	Makes a copy of the last retrieved frame data.
===============================================================================
*/
LeapFrame LeapMotion::GetFrame()
{
	// Get sole access to the frame data.
	_pFrameMutex->Lock();
		
	LeapFrame copy = *_pFrame;

	// Release the frame data for other threads.
	_pFrameMutex->Unlock();

	return copy;
}

/*
===============================================================================
LeapMotion::SetFrame
	Updates the Leap frame data.
===============================================================================
*/
void LeapMotion::SetFrame( const LeapFrame &frame )
{
	// Get sole access to the frame data.
	_pFrameMutex->Lock();
		
	*_pFrame = frame;

	// Release the frame data for other threads.
	_pFrameMutex->Unlock();
}