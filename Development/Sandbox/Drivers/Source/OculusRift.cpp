/*
===============================================================================

	LeapMotion.cpp
		Application level Leap Motion device interface implementation.

===============================================================================
*/

#include "Framework/Sandbox.h"
#include "Drivers/OculusRift.h"
#include "Framework/Application.h"

using namespace OVR;
using namespace OVR::Util;
using namespace OVR::Util::Render;
using namespace sandbox;
using namespace sandbox::math;
using namespace sandbox::framework;
using namespace sandbox::drivers;
using namespace sandbox::system;

/*
===============================================================================
	OculusRift implementation.
===============================================================================
*/

/*
===============================================================================
OculusRift::OculusRift
	Establishes a connection to the Oculus Rift device.
===============================================================================
*/
OculusRift::OculusRift()
{
	// Initialize pointers.
	_pStereoConfig = NULL;
	_pSensorFusion = NULL;
	_pSensor = NULL;
	_pDevice = NULL;
	_pDeviceManager = NULL;

	SetDefaultHmdInfo();

	// Connect to the Rift device.
	CreateDevice();
	AttachSensor();

	InitStereoConfig();
	CalculateEyeOffsets();
}

/*
===============================================================================
OculusRift::~OculusRift
	Establishes a connection to the Oculus Rift device.
===============================================================================
*/
OculusRift::~OculusRift()
{
	if( _pStereoConfig )
	{
		delete _pStereoConfig;
	}

	if( _pSensorFusion )
	{
		delete _pSensorFusion;
	}

	if( _pSensor )
	{
		_pSensor->Release();
	}

	if( _pDevice )
	{
		_pDevice->Release();
	}

	if( _pDeviceManager )
	{
		_pDeviceManager->Release();
	}

	System::Destroy();
}

/*
===============================================================================
OculusRift::GetHalfIpd
	Provides access to the halved InterPulilary Distance.
	Basically from Pupil to nose bridge.
===============================================================================
*/
float OculusRift::GetHalfIpd() const
{
	return _halfIpd;
}

/*
===============================================================================
OculusRift::GetVerticalFov
	
===============================================================================
*/
float OculusRift::GetVerticalFov() const
{
	return _pStereoConfig->GetYFOVRadians();
}

/*
===============================================================================
OculusRift::GetProjectionMatrix
	Builds the projection matrix for the given Oculus eye.
===============================================================================
*/
math::Matrix4f OculusRift::GetProjectionMatrix( oculusView_e view, float zNear, float zFar ) const
{
	_pStereoConfig->SetClippingPlanes( zNear, zFar );	// Wherever you are.

	StereoEyeParams params = GetStereoEyeParams( view );

	// Need to transpose the matrix since our Matrix4f is backwards.
	OVR::Matrix4f m = params.Projection.Transposed();
	return math::Matrix4f(	m.M[0][0], m.M[1][0], m.M[2][0], m.M[3][0], 
							m.M[0][1], m.M[1][1], m.M[2][1], m.M[3][1], 
							m.M[0][2], m.M[1][2], m.M[2][2], m.M[3][2], 
							m.M[0][3], m.M[1][3], m.M[2][3], m.M[3][3] );
}

/*
===============================================================================
OculusRift::GetViewMatrixOffset
	Determines the view offset for the given Oculus eye.
===============================================================================
*/
math::Matrix4f OculusRift::GetViewMatrixOffset( oculusView_e view ) const
{
	StereoEyeParams params = GetStereoEyeParams( view );

	// Need to transpose the matrix since our Matrix4f is backwards.
	OVR::Matrix4f m = params.ViewAdjust.Transposed();
	return math::Matrix4f(	m.M[0][0], m.M[1][0], m.M[2][0], m.M[3][0], 
							m.M[0][1], m.M[1][1], m.M[2][1], m.M[3][1], 
							m.M[0][2], m.M[1][2], m.M[2][2], m.M[3][2], 
							m.M[0][3], m.M[1][3], m.M[2][3], m.M[3][3] );
}

/*
===============================================================================
OculusRift::GetOrientation
	Provides access to the current direction of the Rift.
===============================================================================
*/
math::Angle3f OculusRift::GetOrientation() const
{
	if( !_pDevice || !_pSensorFusion )
	{
		// No device attached, therefore we have no angles.
		return math::Angle3f();
	}

	Quatf hmdOrientation = _pSensorFusion->GetPredictedOrientation();

	// Get the current direction in radians.
	float pitch, roll, yaw;
	hmdOrientation.GetEulerAngles<Axis_X, Axis_Z, Axis_Y>( &pitch, &roll, &yaw );

	return math::Angle3f( pitch, roll, yaw );
}

/*
===============================================================================
OculusRift::SetDistortionViewport
	Informs the Oculus of the current viewport that is being rendered.
===============================================================================
*/
void OculusRift::SetDistortionViewport( const render::viewport_t &viewport, const render::viewport_t &fullscreen )
{
	_viewport = viewport;
	_fullscreenVp = fullscreen;
}

/*
===============================================================================
OculusRift::GetDistortionLensCenter
	Calculates the center position of the lens.
===============================================================================
*/
void OculusRift::GetDistortionLensCenter( float *pX, float *pY ) const
{
	// Get the dimensions of the viewport as decimals.
	float fracX = i2fl( _viewport._x ) / i2fl( _fullscreenVp._width );
	float fracY = i2fl( _viewport._y ) / i2fl( _fullscreenVp._height );
	float fracW = i2fl( _viewport._width ) / i2fl( _fullscreenVp._width );
	float fracH = i2fl( _viewport._height ) / i2fl( _fullscreenVp._height );

	*pX = fracX + ( fracW + _xCenterOffset * 0.5f ) * 0.5f;
	*pY = fracY + ( fracH * 0.5f );
}

/*
===============================================================================
OculusRift::GetDistortionScreenCenter
	Calculates the center position of the lens.
===============================================================================
*/
void OculusRift::GetDistortionScreenCenter( float *pX, float *pY ) const
{
	// Get the dimensions of the viewport as decimals.
	float fracX = i2fl( _viewport._x ) / i2fl( _fullscreenVp._width );
	float fracY = i2fl( _viewport._y ) / i2fl( _fullscreenVp._height );
	float fracW = i2fl( _viewport._width ) / i2fl( _fullscreenVp._width );
	float fracH = i2fl( _viewport._height ) / i2fl( _fullscreenVp._height );

	*pX = fracX + ( fracW * 0.5f );
	*pY = fracY + ( fracH * 0.5f );
}

/*
===============================================================================
OculusRift::GetDistortionScale
	Provides access to the Oculus computed distortion scales.
===============================================================================
*/
void OculusRift::GetDistortionScale( float *pX, float *pY ) const
{
	// Get the dimensions of the viewport as decimals.
	float fracW = i2fl( _viewport._width ) / i2fl( _fullscreenVp._width );
	float fracH = i2fl( _viewport._height ) / i2fl( _fullscreenVp._height );

	float aspect = i2fl( _viewport._width ) / i2fl( _viewport._height );

	float scale = 1.0f / _pStereoConfig->GetDistortionScale();

	*pX = ( fracW * 0.5f ) * scale;
	*pY = ( fracH * 0.5f ) * scale * aspect;
}

/*
===============================================================================
OculusRift::GetDistortionScaleIn
	Provides access to the Oculus computed distortion scales.
===============================================================================
*/
void OculusRift::GetDistortionScaleIn( float *pX, float *pY ) const
{
	// Get the dimensions of the viewport as decimals.
	float fracW = i2fl( _viewport._width ) / i2fl( _fullscreenVp._width );
	float fracH = i2fl( _viewport._height ) / i2fl( _fullscreenVp._height );

	float aspect = i2fl( _viewport._width ) / i2fl( _viewport._height );

	float scale = 1.0f / _pStereoConfig->GetDistortionScale();

	*pX = ( 2.0f / fracW );
	*pY = ( 2.0f / fracH ) / aspect;
}

/*
===============================================================================
OculusRift::GetDistortionK
	Provides access to the Oculus computed distortion coefficients.
===============================================================================
*/
void OculusRift::GetDistortionK( float k[4] ) const
{
	k[0] = _pStereoConfig->GetDistortionK( 0 );
	k[1] = _pStereoConfig->GetDistortionK( 1 );
	k[2] = _pStereoConfig->GetDistortionK( 2 );
	k[3] = _pStereoConfig->GetDistortionK( 3 );
}

/*
===============================================================================
OculusRift::GetDistortionChromaticAbb
	Provides access to the Oculus computed chromatic abberation coefficients.
===============================================================================
*/
void OculusRift::GetDistortionChromaticAbb( float k[4] ) const
{
	const DistortionConfig &config = _pStereoConfig->GetDistortionConfig();
	k[0] = config.ChromaticAberration[0];
	k[1] = config.ChromaticAberration[1];
	k[2] = config.ChromaticAberration[2];
	k[3] = config.ChromaticAberration[3];
}

/*
===============================================================================
OculusRift::GetDistortionMatrix
	Builds the `Texm` matrix for the warp shader.
===============================================================================
*/
math::Matrix4f OculusRift::GetDistortionMatrix() const
{
	// Get the dimensions of the viewport as decimals.
	float fracX = i2fl( _viewport._x ) / i2fl( _fullscreenVp._width );
	float fracY = i2fl( _viewport._y ) / i2fl( _fullscreenVp._height );
	float fracW = i2fl( _viewport._width ) / i2fl( _fullscreenVp._width );
	float fracH = i2fl( _viewport._height ) / i2fl( _fullscreenVp._height );

	return math::Matrix4f(	fracW,	0.0f,	0.0f,	0.0f,
							0.0f,	fracH,	0.0f,	0.0f,
							0.0f,	0.0f,	0.0f,	0.0f,
							fracX,	fracY,	0.0f,	1.0f );
}

/*
===============================================================================
OculusRift::SetDefaultHmdInfo
	Attempts to connect to a Rift device connected to the computer.
===============================================================================
*/
void OculusRift::SetDefaultHmdInfo()
{
	_hmdInfo.HResolution = 1280;
	_hmdInfo.VResolution = 800;
	_hmdInfo.HScreenSize = 0.14975999f;
	_hmdInfo.VScreenSize = 0.093599997f;
	_hmdInfo.VScreenCenter = 0.046799999f;
	_hmdInfo.EyeToScreenDistance = 0.041000001f;
	_hmdInfo.LensSeparationDistance = 0.063500002f;
	_hmdInfo.InterpupillaryDistance = 0.064999998f;
	_hmdInfo.DistortionK[0] = 1.0f;
	_hmdInfo.DistortionK[1] = 0.22f;
	_hmdInfo.DistortionK[2] = 0.23999999f;
	_hmdInfo.DistortionK[3] = 0.0f;
	_hmdInfo.ChromaAbCorrection[0] = 0.99599999f;
	_hmdInfo.ChromaAbCorrection[1] = -0.0040000002f;
	_hmdInfo.ChromaAbCorrection[2] = 1.0140001f;
	_hmdInfo.ChromaAbCorrection[3] = 0.0f;
	_hmdInfo.DesktopX = 0;
	_hmdInfo.DesktopY = 0;
	_hmdInfo.DisplayDeviceName[0] = NULL;
	_hmdInfo.DisplayId = 0;
}

/*
===============================================================================
OculusRift::CreateDevice
	Attempts to connect to a Rift device connected to the computer.
===============================================================================
*/
void OculusRift::CreateDevice()
{
	// Ready LibOVR for use.
	Log *pLog = Log::ConfigureDefaultLog( LogMask_All );
	System::Init( pLog );

	// Get a list of attached devices.
	_pDeviceManager = DeviceManager::Create();
	DeviceEnumerator<HMDDevice> pEnum = _pDeviceManager->EnumerateDevices<HMDDevice>();

	// Default to the first device.
	_pDevice = pEnum.CreateDevice();
	if( _pDevice )
	{
		// If `_pDevice` is NULL it retains the values set in `SetDefaultHmdInfo`.
		_pDevice->GetDeviceInfo( &_hmdInfo );
	}
}

/*
===============================================================================
OculusRift::AttachSensor
	Readies the Rift sensors for use.
===============================================================================
*/
void OculusRift::AttachSensor()
{
	if( !_pDevice )
	{
		// No device? No sensor.
		return;
	}

	_pSensor = _pDevice->GetSensor();
	if( _pSensor )
	{
		_pSensorFusion = new SensorFusion( _pSensor );
	}
}

/*
===============================================================================
OculusRift::InitStereoConfig
	Readies the StereoConfig helper class.
===============================================================================
*/
void OculusRift::InitStereoConfig()
{
	_pStereoConfig = new StereoConfig;

	const Application *pApp = Application::GetApp();
	const Window *pWindow = pApp->AppWindow();
	sbAssert( pWindow );
	const Viewport viewport( 0, 0, pWindow->GetWidth(), pWindow->GetHeight() );

	// Set the stereo configuration settings.
	_pStereoConfig->SetHMDInfo( _hmdInfo );
	_pStereoConfig->SetFullViewport( viewport );
	_pStereoConfig->SetStereoMode( Stereo_LeftRight_Multipass );
	_pStereoConfig->SetDistortionFitPointVP( -1.0f, 0.0f );

	// Configure proper Distortion Fit.
	// For 7" screen, fit to touch left side of the view, leaving a bit of invisible
	// screen on the top (saves on rendering cost).
	// For smaller screens (5.5"), fit to the top.
	if( _hmdInfo.HScreenSize > 0.0f )
	{
		if( _hmdInfo.HScreenSize > 0.140f ) // 7"
			_pStereoConfig->SetDistortionFitPointVP( -1.0f, 0.0f );
		else
			_pStereoConfig->SetDistortionFitPointVP( 0.0f, 1.0f );
	}
}

/*
===============================================================================
OculusRift::CalculateEyeOffsets
	Readies the StereoConfig helper class.
===============================================================================
*/
void OculusRift::CalculateEyeOffsets()
{
	// Get the base eye offset.
	_halfIpd = _hmdInfo.InterpupillaryDistance / 2.0f;

	// Calculate the eye projection offsets.
	//const float viewCenter			= _hmdInfo.HScreenSize / 4.0f;
	//const float eyeProjectionShift	= viewCenter -  (_hmdInfo.LensSeparationDistance / 2.0f );
	//_projectionCenterOffset	= 4.0f * eyeProjectionShift / viewCenter;

	const float lensOffset = _hmdInfo.LensSeparationDistance * 0.5f;
	const float lensShift = ( _hmdInfo.HScreenSize * 0.25f ) - lensOffset;
	_xCenterOffset = 4.0f * lensShift / _hmdInfo.HScreenSize;

	// Fudge the units to look nice in game.
	//_halfIpd = _halfIpd * 10.0f;
	//_projectionCenterOffset = _projectionCenterOffset * 5.0f;
}

/*
===============================================================================
OculusRift::GetStereoEyeParams
	Provides access to the eye params in the Rift stereo helper.
===============================================================================
*/
StereoEyeParams OculusRift::GetStereoEyeParams( oculusView_e view ) const
{
	StereoEye eye = StereoEye_Center;

	switch( view )
	{
		case VIEW_LEFT:
			eye = StereoEye_Left; 
			break;

		case VIEW_RIGHT:
			eye = StereoEye_Right; 
			break;

		default:
			break;
	}

	return _pStereoConfig->GetEyeRenderParams( eye );
}