/*
===============================================================================

	LeapFrame.cpp
		Contains frame specific Leap data.

===============================================================================
*/

#include "Framework/Sandbox.h"
#include "Drivers/LeapMotion.h"
#include "System/Thread.h"

using namespace Leap;
using namespace sandbox::system;
using namespace sandbox::drivers;

/*
===============================================================================
	LeapFrame implementation.
===============================================================================
*/

/*
===============================================================================
LeapFrame::LeapFrame
	Default constructor.
===============================================================================
*/
LeapFrame::LeapFrame()
{
}

/*
===============================================================================
LeapFrame::LeapFrame
	Reads and converts Leap frame information to Sandbox.
===============================================================================
*/
LeapFrame::LeapFrame( const Leap::Controller &controller )
{
	SetHands( controller );
}

/*
===============================================================================
LeapFrame::~LeapFrame
	Releases frame resources.
===============================================================================
*/
LeapFrame::~LeapFrame()
{
}

/*
===============================================================================
LeapFrame::SetHands
	Adds hand data to this frame.
===============================================================================
*/
void LeapFrame::SetHands( const Controller &controller )
{
	const Frame &frame = controller.frame();
	const HandList &hands = frame.hands();

	for( int i = 0 ; i < hands.count() ; i++ )
	{
		LeapHand leapHand( hands[i] );
		_hands.push_back( leapHand );
	}
}