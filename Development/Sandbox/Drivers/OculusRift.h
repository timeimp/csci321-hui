/*
===============================================================================

	OculusRift.h
		Application level Oculus Rift interface.

===============================================================================
*/

#ifndef __OCULUSRIFT_H__
#define __OCULUSRIFT_H__

#include "Math/Matrix.h"
#include "Render/Render.h"

#include <LibOVR/Include/OVR.h>

namespace sandbox
{
	namespace drivers
	{

		/*
		===============================================================================
		OculusRift
			Main Oculus Rift interface.
		===============================================================================
		*/
		class OculusRift
		{
		public:
			enum oculusView_e
			{
				VIEW_CENTER,
				VIEW_LEFT,
				VIEW_RIGHT,

				VIEW_COUNT
			};

			/*
			-------------------------------------------------------------------------
				Setup, destruction.
			-------------------------------------------------------------------------
			*/
							OculusRift();
							~OculusRift();

			/*
			-------------------------------------------------------------------------
				Accessors.
			-------------------------------------------------------------------------
			*/
			float			GetHalfIpd() const;
			float			GetVerticalFov() const;
			math::Matrix4f	GetProjectionMatrix( oculusView_e view, float zNear, float zFar ) const;
			math::Matrix4f	GetViewMatrixOffset( oculusView_e view ) const;
			math::Angle3f	GetOrientation() const;

			/*
			-------------------------------------------------------------------------
				Warp shader parameters.
			-------------------------------------------------------------------------
			*/
			void			SetDistortionViewport( const render::viewport_t &viewport, const render::viewport_t &fullscreen );
			void			GetDistortionLensCenter( float *pX, float *pY ) const;
			void			GetDistortionScreenCenter( float *pX, float *pY ) const;
			void			GetDistortionScale( float *pX, float *pY ) const;
			void			GetDistortionScaleIn( float *pX, float *pY ) const;
			void			GetDistortionK( float k[4] ) const;
			void			GetDistortionChromaticAbb( float k[4] ) const;
			math::Matrix4f	GetDistortionMatrix() const;

		private:
			/*
			-------------------------------------------------------------------------
				Initialization.
			-------------------------------------------------------------------------
			*/
			void			SetDefaultHmdInfo();
			void			CreateDevice();
			void			AttachSensor();
			void			InitStereoConfig();
			void			CalculateEyeOffsets();

			/*
			-------------------------------------------------------------------------
				Private accessors.
			-------------------------------------------------------------------------
			*/
			OVR::Util::Render::StereoEyeParams GetStereoEyeParams( oculusView_e view ) const;

			// Device handles.
			OVR::DeviceManager *	_pDeviceManager;
			OVR::HMDDevice *	_pDevice;
			OVR::SensorDevice *	_pSensor;
			OVR::SensorFusion *	_pSensorFusion;

			// Device information.
			OVR::HMDInfo	_hmdInfo;
			OVR::Util::Render::StereoConfig *_pStereoConfig;

			render::viewport_t _viewport;
			render::viewport_t _fullscreenVp;

			// Eye offsets.
			float			_halfIpd;
			float			_xCenterOffset;
		};

	} // namespace drivers;
} // namespace sandbox;

#endif __OCULUSRIFT_H__