/*
===============================================================================

	LeapMotion.h
		Application level Leap Motion device interface. Hides all the Leap Motion threading.

===============================================================================
*/

#ifndef __LEAPMOTION_H__
#define __LEAPMOTION_H__

#include "System/Thread.h"
#include "Math/Vector.h"

#include <queue>
#include <memory>
#include <LeapSDK/include/Leap.h>

namespace sandbox
{
	namespace drivers
	{

		// Forward declaration.
		class LeapListener;
		class LeapGestureQueue;
		class LeapHand;
		class LeapFrame;

		/*
		===============================================================================
		LeapMotion
			Main Leap Motion interface.
		===============================================================================
		*/
		class LeapMotion
		{
		public:
			/*
			-------------------------------------------------------------------------
				Creation\destruction.
			-------------------------------------------------------------------------
			*/
							LeapMotion();
							~LeapMotion();

			/*
			-------------------------------------------------------------------------
				Accessors.
			-------------------------------------------------------------------------
			*/
			inline LeapGestureQueue *GetGestureQueue()	{ return _pGestureQueue; }
			LeapFrame		GetFrame();

			/*
			-------------------------------------------------------------------------
				Mutators.
			-------------------------------------------------------------------------
			*/
			void			SetFrame( const LeapFrame &frame );

		private:
			LeapGestureQueue *	_pGestureQueue;
			LeapListener *	_pListener;

			LeapFrame *		_pFrame;
			system::Mutex *	_pFrameMutex;

			Leap::Controller _controller;
		};

		/*
		===============================================================================
		LeapListener
			Used by the Leap SDK to give programs access to Leap frame data.
		===============================================================================
		*/
		class LeapListener : public Leap::Listener
		{
		public:
			/*
			-------------------------------------------------------------------------
				Creation\destruction.
			-------------------------------------------------------------------------
			*/
							LeapListener( class LeapMotion *leap );

			/*
			-------------------------------------------------------------------------
				Leap::Listener interface.
			-------------------------------------------------------------------------
			*/
			virtual void	onInit( const Leap::Controller &controller );
			virtual void	onConnect( const Leap::Controller &controller );
			virtual void	onDisconnect( const Leap::Controller &controller );
			virtual void	onExit( const Leap::Controller &controller );
			virtual void	onFrame( const Leap::Controller &controller );
			virtual void	onFocusGained( const Leap::Controller &controller );
			virtual void	onFocusLost( const Leap::Controller &controller );

		private:
			/*
			-------------------------------------------------------------------------
				Frame processing.
			-------------------------------------------------------------------------
			*/
			void			BuildFrameData( const Leap::Controller &controller );
			void			PlaceGesturesInQueue( const Leap::Controller &controller );
			Leap::Gesture *	CreateGesture( const Leap::Gesture &gesture ) const;

			class LeapMotion *_leap;
		};

		/*
		===============================================================================
		LeapGestureQueue
			Keeps track of all gestures entered by the user.
			Before accessing or modifying stored gestures the `LeapGestureQueue` must 
			first be locked.
			To access gestures call `Pop` in a loop. `Pop` will return NULL when there
			are no more gestures enqueued.
			When finished processing gestures the `LeapGestureQueue` must be Unlocked.
		===============================================================================
		*/
		class LeapGestureQueue
		{
		public:
			/*
			-------------------------------------------------------------------------
				Creation\destruction.
			-------------------------------------------------------------------------
			*/
							LeapGestureQueue();
							~LeapGestureQueue();

			/*
			-------------------------------------------------------------------------
				Mutex control.
			-------------------------------------------------------------------------
			*/
			void			Lock()						{ _mutex->Lock(); }
			void			Unlock()					{ _mutex->Unlock(); }

			/*
			-------------------------------------------------------------------------
				Mutators.
			-------------------------------------------------------------------------
			*/
			void			Push( const Leap::Gesture &pGesture );
			Leap::Gesture 	Pop();

		private:
			std::queue<Leap::Gesture>	_queue;
			system::Mutex *	_mutex;
		};

		/*
		===============================================================================
		LeapPointable
			A single finger attached to this hand. Basically a Leap::LeapPointable, 
			but using Sandbox units.
		===============================================================================
		*/
		class LeapPointable
		{
		public:
			enum pointable_e
			{
				POINTABLE_FINGER,
				POINTABLE_TOOL,
			};

			/*
			-------------------------------------------------------------------------
				Creation\destruction.
			-------------------------------------------------------------------------
			*/
							LeapPointable();
							LeapPointable( const Leap::Pointable &pointable );

			/*
			-------------------------------------------------------------------------
				Accessors.
			-------------------------------------------------------------------------
			*/
			const math::Point3f	&GetTipPosition() const	{ return _tipPosition; }
			const math::Point3f &GetBasePosition() const	{ return _basePosition; }

			const math::Angle3f	&GetAngles() const		{ return _angles; }
			const math::Vector3f &GetDirection() const	{ return _direction; }

			float			GetWidth() const			{ return _width; }
			float			GetLength() const			{ return _length; }
			int				GetId()	const				{ return _id; }
			float			GetTimeVisible() const		{ return _timeVisible; }
			pointable_e		GetType() const				{ return _type; }

		private:
			math::Point3f	_tipPosition;
			math::Point3f	_basePosition;

			math::Angle3f	_angles;
			math::Vector3f	_direction;

			float			_width;
			float			_length;
			int				_id;
			float			_timeVisible;
			pointable_e		_type;
		};

		/*
		===============================================================================
		LeapHand
			A hand in the Leaps field of view. Is basically a Leap::Head, but using
			Sandbox units.
		===============================================================================
		*/
		class LeapHand
		{
		public:
			/*
			-------------------------------------------------------------------------
				Creation\destruction.
			-------------------------------------------------------------------------
			*/
							LeapHand();
							LeapHand( const Leap::Hand &hand );

			/*
			-------------------------------------------------------------------------
				Accessors.
			-------------------------------------------------------------------------
			*/
			const math::Point3f	&GetPosition() const	{ return _position; }
			const math::Angle3f	&GetAngles() const		{ return _angles; }
			int				GetId()	const				{ return _id; }
			const std::vector<LeapPointable> &GetPointables() const	{ return _pointables; }

		private:
			math::Point3f	_position;
			math::Angle3f	_angles;
			int				_id;

			std::vector<LeapPointable> _pointables;
		};

		/*
		===============================================================================
		LeapFrame
			Contains all information about the last frame retrieved from the Leap.
		===============================================================================
		*/
		class LeapFrame
		{
		public:
			/*
			-------------------------------------------------------------------------
				Creation\destruction.
			-------------------------------------------------------------------------
			*/
							LeapFrame();
							LeapFrame( const Leap::Controller &controller );
							~LeapFrame();

			/*
			-------------------------------------------------------------------------
				Accessors.
			-------------------------------------------------------------------------
			*/
			const std::vector<LeapHand> &GetHands()		{ return _hands; }

		private:
			/*
			-------------------------------------------------------------------------
				Setup.
			-------------------------------------------------------------------------
			*/
			void			SetHands( const Leap::Controller &controller );
			void			AddSingleHand( const Leap::Hand &hand );

			/*
			-------------------------------------------------------------------------
				Frame state.
			-------------------------------------------------------------------------
			*/
			std::vector<LeapHand>	_hands;
		};

	} // namespace drivers;
} // namespace sandbox;

#endif // __LEAPMOTION_H__