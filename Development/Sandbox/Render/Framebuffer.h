/*
===============================================================================

	Framebuffer.h
		A texture that the renderer can draw to.

===============================================================================
*/

#ifndef __FRAMEBUFFER_H__
#define __FRAMEBUFFER_H__

#include "Render/Texture.h"
#include "Math/Point.h"

namespace sandbox
{
	namespace render
	{

		/*
		===============================================================================
		Framebuffer
			A texture that OpenGL can render to.
		===============================================================================
		*/
		class Framebuffer
		{
		public:
			/*
			-------------------------------------------------------------------------
				Creation\destruction.
			-------------------------------------------------------------------------
			*/
							Framebuffer();
							Framebuffer( unsigned int width, unsigned int height, bool attachDepth = true );
							~Framebuffer();

			/*
			-------------------------------------------------------------------------
				Mutators.
			-------------------------------------------------------------------------
			*/
			void			Init( unsigned int width, unsigned int height, bool attachDepth = true );

			/*
			-------------------------------------------------------------------------
				Accessors.
			-------------------------------------------------------------------------
			*/
			GLuint			GetName() const				{ return _framebufferName; }
			unsigned int	GetWidth() const			{ return _texture.GetWidth(); }
			unsigned int	GetHeight() const			{ return _texture.GetHeight(); }
			const Texture *	GetTexture() const			{ return &_texture; }
			math::Point2f	GetMaxTc() const			{ return math::Point2f( _npotS, _npotT ); }

		private:
			void			CreateColorTexture( unsigned int width, unsigned int height );
			void			CreateDepthTexture( unsigned int width, unsigned int height );
			void			CreateFramebuffer();

			GLuint			_framebufferName;
			GLuint			_depthBufferName;
			Texture			_texture;

			float			_npotS, _npotT;
		};

	} // namespace render;
} // namespace sandbox;

#endif // __FRAMEBUFFER_H__