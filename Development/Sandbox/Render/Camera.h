/*
===============================================================================

	Camera.h
		Abstracts the viewport and matrix.

===============================================================================
*/

#ifndef __CAMERA_H__
#define __CAMERA_H__

#include "Math/Point.h"

namespace sandbox
{
	namespace render
	{

		/*
		===============================================================================
		viewport_t
			Basic rectangle container.
		===============================================================================
		*/
		struct viewport_t
		{
							viewport_t( int x = 0, int y = 0, int w = 0, int h = 0 )
							{
								Set( x, y, w, h );
							}

							void Set( int x, int y, int w, int h )
							{
								_x = x;
								_y = y;
								_width = w;
								_height = h;
							}

							int _x, _y, _width, _height;
		};

		/*
		===============================================================================
		Camera
			High level representation of the viewport and view-projection matrix.
		===============================================================================
		*/
		class Camera
		{
		public:
			/*
			-------------------------------------------------------------------------
				Creation\destruction.
			-------------------------------------------------------------------------
			*/
							Camera();
			
			/*
			-------------------------------------------------------------------------
				Mutators.
			-------------------------------------------------------------------------
			*/
			void			SetPosition( const math::Point3f &position );
			void			SetAngles( const math::Angle3f &angles );
			void			SetViewport( const viewport_t &viewport );
			void			SetViewportSize( int xPos, int yPos, int width, int height );
			void			SetViewportFullscreen( bool fullscreen );
			void			SetClippingPlane( float zNear, float zFar );
			void			SetFov( float verticalFov );

			/*
			-------------------------------------------------------------------------
				Accessors.
			-------------------------------------------------------------------------
			*/
			const math::Point3f &GetPosition() const	{ return _position; }
			const math::Angle3f &GetAngles() const		{ return _angles; }
			const viewport_t &GetViewport() const		{ return _viewport; }

			float			GetAspectRatio() const		{ return (float)_viewport._width / (float)_viewport._height; }
			float			GetZNear() const			{ return _zNear; }
			float			GetZFar() const				{ return _zFar; }
			float			GetVerticalFov() const		{ return _verticalFov; }
			bool			IsFullscreen() const		{ return _fullScreenViewport; }

		private:
			math::Point3f	_position;
			math::Angle3f	_angles;
			viewport_t		_viewport;
			float			_zNear, _zFar;
			float			_verticalFov;

			bool			_fullScreenViewport;
		};

	} // namespace render;
} // namespace sandbox;

#endif // __CAMERA_H__