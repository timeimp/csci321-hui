/*
===============================================================================

	Shader.cpp
		OpenGL 2.0 shader and program stuff.

===============================================================================
*/

#include "Framework/Sandbox.h"
#include "Render/Shader.h"
#include "System/OpenGL.h"

#include "RenderPrivate.h"

#include <fstream>
#include <iostream>

using namespace std;
using namespace sandbox::math;
using namespace sandbox::render;
using namespace sandbox::system;

/*
===============================================================================
	Shader implementation.
===============================================================================
*/

/*
===============================================================================
Shader::Shader
	Makes a copy of the shaders code.
===============================================================================
*/
Shader::Shader( bool isVertex, const std::string &string )
{
	bool shouldCompile = true;

	_shaderName = 0;

	if( ReadShaderFromFile( string ) )
	{
		// Successfully read the shader code in. Compile it.
		Compile( isVertex );
	}
}

/*
===============================================================================
Shader::~Shader
	Destroys this shader. Doesn't check if it is attached to programs.
===============================================================================
*/
Shader::~Shader()
{
	glDeleteShader( _shaderName );
}

/*
===============================================================================
Shader::IsValid
	Determines if this is a valid compiled shader.
===============================================================================
*/
bool Shader::IsValid()
{
	return _shaderName != 0;
}

/*
===============================================================================
Shader::ReadShaderFromFile
	Reads shader code from the given file.
===============================================================================
*/
bool Shader::ReadShaderFromFile( const std::string &path )
{
	ifstream fin( path );

	if( !fin.good() )
	{
		// Failed to open the file.
		return false;
	}

	// Reserve memory for this shader code all at once.
	fin.seekg( 0, ios::end );
	const ifstream::pos_type endPos = fin.tellg();
	_shaderCode.reserve( (unsigned int)endPos );

	fin.seekg( 0, ios::beg );

	// Read the file into the string.
	_shaderCode.assign( istreambuf_iterator<char>( fin ), istreambuf_iterator<char>() );

	fin.close();
	return true;
}

/*
===============================================================================
Shader::Compile
	Builds the shader code.
===============================================================================
*/
bool Shader::Compile( bool isVertex )
{
	// Create the OpenGL shader object.
	GLenum type = isVertex ? GL_VERTEX_SHADER : GL_FRAGMENT_SHADER;
	_shaderName = glCreateShader( type );
	sbAssert( _shaderName );
	glErrorCheck();

	// Send the shader source code to OpenGL.
	const char *pShaderCode = _shaderCode.c_str();
	GLint length = _shaderCode.length();
	glShaderSource( _shaderName, 1, &pShaderCode, &length );
	glErrorCheck();

	// Compile the shader.
	glCompileShader( _shaderName );
	glErrorCheck();

	// Check if the shader compiled.
	GLint compiled;
	glGetShaderiv( _shaderName, GL_COMPILE_STATUS, &compiled );
	glErrorCheck();

	// Didn't compile, record the error.
	if( compiled == GL_FALSE )
	{
		DumpErrorLog();

#ifdef sbDEBUG
		// Debug builds halt here.
		sbAssert( 0 );
#endif

		// Clear the shader memory.
		glDeleteShader( _shaderName );
		_shaderName = 0;

		return false;
	}

	return true;
}

/*
===============================================================================
Shader::DumpErrorLog
	Builds the shader code.
===============================================================================
*/
void Shader::DumpErrorLog()
{
	GLchar log[32768];
	GLint len;

	glGetShaderInfoLog( _shaderName, 32768, &len, log );
	glErrorCheck();

	_errorString.assign( log );

	// Debug builds halt here.
	cout << "================================================================================" << endl;
	cout << "================================================================================" << endl;
	cout << log << endl;
	cout << "================================================================================" << endl;
	cout << "================================================================================" << endl;
	cout << "Shader compile error!" << endl;
}

/*
===============================================================================
	VertexShader implementation.
===============================================================================
*/

/*
===============================================================================
VertexShader::VertexShader
	Loads the vertex shader into memory.
===============================================================================
*/
VertexShader::VertexShader( const string &string ) :
	Shader( true, string )
{
}
	
/*
===============================================================================
	FragmentShader implementation.
===============================================================================
*/

/*
===============================================================================
FragmentShader::FragmentShader
	Loads the fragment shader into memory.
===============================================================================
*/
FragmentShader::FragmentShader( const string &string ) :
	Shader( false, string )
{
}

/*
===============================================================================
	Program implementation.
===============================================================================
*/

/*
===============================================================================
Program::Program
	Sets the program as invalid.
===============================================================================
*/
Program::Program()
{
	_programName = 0;
}

/*
===============================================================================
Program::Program
	Links the shader program.
===============================================================================
*/
Program::Program( const string &title, const VertexShader &vertex, const FragmentShader &frag, const std::string &uniforms )
{
	_programName = 0;

	Init( title, vertex, frag, uniforms );
}

/*
===============================================================================
Program::~Program
	Links the shader program.
===============================================================================
*/
Program::~Program()
{
	DetachShaders();

	// Destroy the program.
	if( _programName )
	{
		glDeleteProgram( _programName );
	}
}

/*
===============================================================================
Program::Program
	Links the shader program.
===============================================================================
*/
void Program::Init( const string &title, const VertexShader &vertex, const FragmentShader &frag, const std::string &uniforms )
{
	_programTitle = title;

	CreateProgram( vertex, frag );
	CacheCustomUniforms( uniforms );
	CacheInternalUniforms();
}

/*
===============================================================================
Program::SetUniformPoint2f
	Uploads a Point2f variable to the given uniform.
===============================================================================
*/
void Program::SetUniformPoint2f( const std::string &uniform, const Point2f &p )
{
	SetUniform2fv( uniform, p.Base() );
}

/*
===============================================================================
Program::SetUniformPoint3f
	Uploads a Point3f variable to the given uniform.
===============================================================================
*/
void Program::SetUniformPoint3f( const std::string &uniform, const Point3f &p )
{
	SetUniform3fv( uniform, p.Base() );
}

/*
===============================================================================
Program::SetUniformPoint4f
	Uploads a Point4f variable to the given uniform.
===============================================================================
*/
void Program::SetUniformPoint4f( const std::string &uniform, const Point4f &p )
{
	SetUniform4fv( uniform, p.Base() );
}

/*
===============================================================================
Program::SetUniformColor3f
	Uploads a Color3f variable to the given uniform.
===============================================================================
*/
void Program::SetUniformColor3f( const std::string &uniform, const Color3f &c )
{
	SetUniform3fv( uniform, c.Base() );
}

/*
===============================================================================
Program::SetUniformColor4f
	Uploads a Color4f variable to the given uniform.
===============================================================================
*/
void Program::SetUniformColor4f( const std::string &uniform, const Color4f &c )
{
	SetUniform4fv( uniform, c.Base() );
}

/*
===============================================================================
Program::SetUniformVector3f
	Uploads a Vector3f variable to the given uniform.
===============================================================================
*/
void Program::SetUniformVector3f( const std::string &uniform, const Vector3f &v )
{
	SetUniform3fv( uniform, v.Base() );
}

/*
===============================================================================
Program::SetUniformMatrix4f
	Uploads a Matrix4f variable to the given uniform.
===============================================================================
*/
void Program::SetUniformMatrix4f( const std::string &uniform, const Matrix4f &m )
{
	GLint loc = GetUniformLocation( uniform );

	if( loc != -1 )
	{
		glUniformMatrix4fv( loc, 1, GL_FALSE, m.Get() );
		glErrorCheck();
	}
}

/*
===============================================================================
Program::SetUniform1f
	Uploads a single float to the given uniform.
===============================================================================
*/
void Program::SetUniform1f( const std::string &uniform, float v )
{
	GLint loc = GetUniformLocation( uniform );

	if( loc != -1 )
	{
		glUniform1f( loc, v );
		glErrorCheck();
	}
}

/*
===============================================================================
Program::SetUniform2f
	Uploads two floats to the given uniform.
===============================================================================
*/
void Program::SetUniform2f( const std::string &uniform, float v0, float v1 )
{
	GLint loc = GetUniformLocation( uniform );

	if( loc != -1 )
	{
		glUniform2f( loc, v0, v1 );
		glErrorCheck();
	}
}

/*
===============================================================================
Program::SetUniform2fv
	Uploads two floats to the given uniform.
===============================================================================
*/
void Program::SetUniform2fv( const std::string &uniform, const float *v )
{
	GLint loc = GetUniformLocation( uniform );

	if( loc != -1 )
	{
		glUniform2fv( loc, 1, v );
		glErrorCheck();
	}
}

/*
===============================================================================
Program::SetUniform3f
	Uploads three floats to the given uniform.
===============================================================================
*/
void Program::SetUniform3f( const std::string &uniform, float v0, float v1, float v2 )
{
	GLint loc = GetUniformLocation( uniform );

	if( loc != -1 )
	{
		glUniform3f( loc, v0, v1, v2 );
		glErrorCheck();
	}
}

/*
===============================================================================
Program::SetUniform3fv
	Uploads three floats to the given uniform.
===============================================================================
*/
void Program::SetUniform3fv( const std::string &uniform, const float *v )
{
	GLint loc = GetUniformLocation( uniform );

	if( loc != -1 )
	{
		glUniform3fv( loc, 1, v );
		glErrorCheck();
	}
}

/*
===============================================================================
Program::SetUniform4f
	Uploads four floats to the given uniform.
===============================================================================
*/
void Program::SetUniform4f( const std::string &uniform, float v0, float v1, float v2, float v3 )
{
	GLint loc = GetUniformLocation( uniform );

	if( loc != -1 )
	{
		glUniform4f( loc, v0, v1, v2, v3 );
		glErrorCheck();
	}
}

/*
===============================================================================
Program::SetUniform4fv
	Uploads four floats to the given uniform.
===============================================================================
*/
void Program::SetUniform4fv( const std::string &uniform, const float *v )
{
	GLint loc = GetUniformLocation( uniform );

	if( loc != -1 )
	{
		glUniform4fv( loc, 1, v );
		glErrorCheck();
	}
}

/*
===============================================================================
Program::SetUniform1i
	Uploads a single integer to the given uniform.
===============================================================================
*/
void Program::SetUniform1i( const std::string &uniform, int v )
{
	GLint loc = GetUniformLocation( uniform );

	if( loc != -1 )
	{
		glUniform1i( loc, v );
		glErrorCheck();
	}
}

/*
===============================================================================
Program::SetUniform2iv
	Uploads two integers to the given uniform.
===============================================================================
*/
void Program::SetUniform2iv( const std::string &uniform, const int *v )
{
	GLint loc = GetUniformLocation( uniform );

	if( loc != -1 )
	{
		glUniform2iv( loc, 1, v );
		glErrorCheck();
	}
}

/*
===============================================================================
Program::SetUniform3iv
	Uploads three integers to the given uniform.
===============================================================================
*/
void Program::SetUniform3iv( const std::string &uniform, const int *v )
{
	GLint loc = GetUniformLocation( uniform );

	if( loc != -1 )
	{
		glUniform3iv( loc, 1, v );
		glErrorCheck();
	}
}

/*
===============================================================================
Program::SetUniform4iv
	Uploads four integers to the given uniform.
===============================================================================
*/
void Program::SetUniform4iv( const std::string &uniform, const int *v )
{
	GLint loc = GetUniformLocation( uniform );

	if( loc != -1 )
	{
		glUniform4iv( loc, 1, v );
		glErrorCheck();
	}
}

/*
===============================================================================
Program::CreateProgram
	Links the given shaders to the program.
===============================================================================
*/
void Program::CreateProgram( const VertexShader &vertex, const FragmentShader &frag )
{
	// Create the shader program.
	_programName = glCreateProgram();
	sbAssert( _programName );
	glErrorCheck();

	// Submit the vertex and fragment shaders to the program.
	glAttachShader( _programName, vertex.GetName() );
	glErrorCheck();
	glAttachShader( _programName, frag.GetName() );
	glErrorCheck();

	// Ready the shader program for use.
	glLinkProgram( _programName );
	glErrorCheck();
}

/*
===============================================================================
Program::CacheUniforms
	Finds and stores the index of each given uniform.
===============================================================================
*/
void Program::CacheCustomUniforms( const std::string &uniforms )
{
	string::size_type lastPos = 0;

	// Find all uniform names (delimited by ';').
	string::size_type pos = uniforms.find( ';', 0 );
	while( pos != string::npos )
	{
		// Extract the current uniform name.
		string uniform = uniforms.substr( lastPos, pos - lastPos );
		
		// Find the uniform in the shader.
		GLint location = glGetUniformLocation( _programName, uniform.c_str() );

		if( location != -1 )
		{
			// Place the name and location into the map for later use.
			pair<string, int> p( uniform, location );
			_uniformMap.insert( p );
		}

		// Find the next uniform.
		lastPos = pos + 1;
		pos = uniforms.find( ';', lastPos );
	}
}

/*
===============================================================================
Program::CacheInternalUniforms
	Finds and stores the index of Sandbox specific uniforms.
===============================================================================
*/
void Program::CacheInternalUniforms()
{
	const GLchar *internalUniformNames[INTERNAL_UNIFORM_COUNT] =
	{
		"uColor",
		"uTexture0",
		"uIsColored",
		"uIsTextured",
	};

	for( int i = 0 ; i < INTERNAL_UNIFORM_COUNT ; i++ )
	{
		_internalUniforms[i] = glGetUniformLocation( _programName, internalUniformNames[i] );

		// Add to the map.
		if( _internalUniforms[i] != -1 )
		{
			// Place the name and location into the map for later use.
			pair<string, int> p( internalUniformNames[i], _internalUniforms[i] );
			_uniformMap.insert( p );
		}
	}
}

/*
===============================================================================
Program::GetUniformLocation
	Finds a precached uniform location.
===============================================================================
*/
GLint Program::GetUniformLocation( const std::string &uniform ) const
{
	// Find the uniform in the map.
	uniformMap_t::const_iterator it = _uniformMap.find( uniform );

	if( it == _uniformMap.end() )
	{
		// Didn't find the uniform in the map.
		cout << "Failed to find uniform \"" << uniform << "\" in program \"" << _programTitle << "\"!" << endl;
		return 0;
	}

	return it->second;
}

/*
===============================================================================
Program::DetachShaders
	Unlinks the shaders from this program.
===============================================================================
*/
void Program::DetachShaders()
{
	GLuint shaders[2];
	GLsizei count;

	// Get the shaders that we active in this program.
	glGetAttachedShaders( _programName, 2, &count, shaders );
	glErrorCheck();

	// Detach them.
	for( GLsizei i = 0 ; i < count ; i++ )
	{
		glDetachShader( _programName, shaders[i] );
		glErrorCheck();
	}
}