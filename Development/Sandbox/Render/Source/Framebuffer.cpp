/*
===============================================================================

	Framebuffer.cpp
		Abstracts the viewport and matrix.

===============================================================================
*/

#include "Framework/Sandbox.h"
#include "Render/Framebuffer.h"
#include "System/OpenGL.h"
#include "Math/Math.h"

#include "RenderPrivate.h"

using namespace sandbox::math;
using namespace sandbox::render;

/*
===============================================================================
	Framebuffer implementation.
===============================================================================
*/

/*
===============================================================================
Framebuffer::Framebuffer
	Default constructor.
===============================================================================
*/
Framebuffer::Framebuffer()
{
	_depthBufferName = 0;
}

/*
===============================================================================
Framebuffer::Framebuffer
	Default constructor.
===============================================================================
*/
Framebuffer::Framebuffer( unsigned int width, unsigned int height, bool attachDepth /*= true*/ )
{
	Init( width, height, attachDepth );
}

/*
===============================================================================
Framebuffer::~Framebuffer
	Destroys the OpenGL FB.
===============================================================================
*/
Framebuffer::~Framebuffer()
{
	if( _depthBufferName )
	{
		glDeleteRenderbuffersEXT( 1, &_depthBufferName );
		glErrorCheck();
	}

	glDeleteFramebuffersEXT( 1, &_framebufferName );
	glErrorCheck();
}
	
/*
===============================================================================
Framebuffer::Init
	Readies the framebuffer for use.
===============================================================================
*/
void Framebuffer::Init( unsigned int width, unsigned int height, bool attachDepth /*= true*/ )
{
	_npotS = 1.0f;
	_npotT = 1.0f;

#if 0
	if( !GlExtensions::NonPow2TextureSupport() )
	{
		// Non power of two textures aren't supported.
		// Ensure the framebuffer is a power of two.
		unsigned int newWidth = npow2( width );
		unsigned int newHeight = npow2( height );

		_npotS = i2fl( width ) / i2fl( newWidth );
		_npotT = i2fl( height ) / i2fl( newHeight );

		width = newWidth;
		height = newHeight;
	}
#endif 

	// Create specified buffers.
	_depthBufferName = 0;
	CreateColorTexture( width, height );
	if( attachDepth )
	{
		CreateDepthTexture( width, height );
	}

	// Attach the buffers to the new framebuffer.
	CreateFramebuffer();
}

/*
===============================================================================
Framebuffer::CreateColorTexture
	Initializes the texture that will be rendered to.
===============================================================================
*/
void Framebuffer::CreateColorTexture( unsigned int width, unsigned int height )
{
	_texture.Reserve( width, height );
	_texture.SetFiltered( false );
	_texture.SetRepeatable( false );
}

/*
===============================================================================
Framebuffer::CreateDepthTexture
	Initializes the texture that will be rendered to.
===============================================================================
*/
void Framebuffer::CreateDepthTexture( unsigned int width, unsigned int height )
{
	glGenRenderbuffersEXT( 1, &_depthBufferName );
	glErrorCheck();

	glBindRenderbufferEXT( GL_RENDERBUFFER_EXT, _depthBufferName );
	glErrorCheck();

	glRenderbufferStorageEXT( GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT, width, height );
	glErrorCheck();

	glBindRenderbufferEXT( GL_RENDERBUFFER_EXT, 0 );
	glErrorCheck();
}

/*
===============================================================================
Framebuffer::CreateFramebuffer
	Readies the actual framebuffer for use.
===============================================================================
*/
void Framebuffer::CreateFramebuffer()
{
	glGenFramebuffersEXT( 1, &_framebufferName );
	glErrorCheck();

	GLuint previousFb = gRenderState.GetCurrentFramebuffer();
	glBindFramebufferEXT( GL_FRAMEBUFFER_EXT, _framebufferName );
	glErrorCheck();

	// Attach the specified buffers.
	glFramebufferTexture2DEXT( GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, _texture.GetName(), 0 );
	glErrorCheck();
	if( _depthBufferName != 0 )
	{
		glFramebufferRenderbufferEXT( GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, _depthBufferName );
		glErrorCheck();
	}

	// Ensure the FB configuration is valid.
	GLenum error = glCheckFramebufferStatusEXT( GL_FRAMEBUFFER_EXT );
	sbAssert( error == GL_FRAMEBUFFER_COMPLETE_EXT );

	// Rebind the old buffer.
	glBindFramebufferEXT( GL_FRAMEBUFFER_EXT, previousFb );
	glErrorCheck();
}