/*
===============================================================================

	Camera.cpp
		Abstracts the viewport and matrix.

===============================================================================
*/

#include "Framework/Sandbox.h"
#include "Render/Camera.h"
#include "Math/Math.h"

using namespace sandbox::math;
using namespace sandbox::render;

/*
===============================================================================
	Camera implementation.
===============================================================================
*/

/*
===============================================================================
Camera::Camera
	Sets the cameras default values.
===============================================================================
*/
Camera::Camera() :
	_viewport( 0, 0, 640, 480 )
{
	_fullScreenViewport = true;

	_position.Set( 0.0f, 0.0f, 0.0f );
	_angles.Set( 0.0f, 0.0f, 0.0f );

	_zNear = 1.0f;
	_zFar = 2000.0f;

	_verticalFov = d2r( 65.0f );
}

/*
===============================================================================
Camera::SetPosition
	Updates the cameras location in the world.
===============================================================================
*/
void Camera::SetPosition( const Point3f &position )
{
	_position = position;
}

/*
===============================================================================
Camera::SetAngles
	Updates the cameras orientation.
===============================================================================
*/
void Camera::SetAngles( const Angle3f &angles )
{
	_angles = angles;
}

/*
===============================================================================
Camera::SetViewport
	Sets the cameras on-screen drawing position.
===============================================================================
*/
void Camera::SetViewport( const viewport_t &viewport )
{
	_viewport = viewport;
}

/*
===============================================================================
Camera::SetViewport
	Sets the cameras on-screen drawing position.
===============================================================================
*/
void Camera::SetViewportSize( int xPos, int yPos, int width, int height )
{
	_viewport._x = sbMAX( xPos, 0 );
	_viewport._y = sbMAX( yPos, 0 );
	_viewport._width = sbMAX( width, 0 );
	_viewport._height = sbMAX( height, 0 );
}

/*
===============================================================================
Camera::SetViewportFullscreen
	Sets the camera to use all available window space, or custom defined by
	`SetViewportSize`.
===============================================================================
*/
void Camera::SetViewportFullscreen( bool fullscreen )
{
	_fullScreenViewport = fullscreen;
}

/*
===============================================================================
Camera::SetClippingPlane
	Sets the near and far Z clipping values.
===============================================================================
*/
void Camera::SetClippingPlane( float zNear, float zFar )
{
	_zNear = zNear;
	_zFar = zFar;
}

/*
===============================================================================
Camera::SetFov
	Sets the vertical FOV for the camera.
===============================================================================
*/
void Camera::SetFov( float verticalFov )
{
	_verticalFov = verticalFov;
}