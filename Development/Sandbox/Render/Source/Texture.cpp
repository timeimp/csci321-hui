/*
===============================================================================

	Texture.cpp
		Stateful OpenGL texture wrapper.

===============================================================================
*/

#include "Framework/Sandbox.h"
#include "Render/Texture.h"
#include "Render/Render.h"

#include "RenderPrivate.h"

using namespace sandbox::render;
using namespace sandbox::system;

/*
===============================================================================
	Texture implementation.
===============================================================================
*/

/*
===============================================================================
Texture::Texture
	Default constructor
===============================================================================
*/
Texture::Texture()
{
	_width = _height = 0;
	_filtered = false;
	_repeated = false;

	glGenTextures( 1, &_textureName );
	glErrorCheck();
}

/*
===============================================================================
Texture::Texture
	Fills a texture with the given pixel data.
===============================================================================
*/
Texture::Texture( bool generateMipmaps, unsigned int width, unsigned int height, pixelFormat_e format, void *pPixels )
{
	_width = _height = 0;
	_filtered = false;
	_repeated = false;

	glGenTextures( 1, &_textureName );
	glErrorCheck();

	UploadPixels( generateMipmaps, width, height, format, pPixels );
}

/*
===============================================================================
Texture::~Texture
	Frees this OpenGL texture.
===============================================================================
*/
Texture::~Texture()
{
	glDeleteTextures( 1, &_textureName );
	glErrorCheck();
}

/*
===============================================================================
Texture::SetFiltered
	Toggles the textures mipmapped state.
===============================================================================
*/
void Texture::SetFiltered( bool filtered )
{
	if( filtered == _filtered )
	{
		// No change in state.
		return;
	}

	// Set this texture as active.
	Render &render = Render::Get();
	render.BindTexture( *this );

	if( filtered )
	{
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
		glErrorCheck();

		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
		glErrorCheck();
	}
	else
	{
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
		glErrorCheck();

		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
		glErrorCheck();
	}

	_filtered = filtered;
}

/*
===============================================================================
Texture::SetRepeatable
	Toggles the the textures wraparound state.
===============================================================================
*/
void Texture::SetRepeatable( bool repeat )
{
	if( repeat == _repeated )
	{
		// No change in state.
		return;
	}

	// Set this texture as active.
	Render &render = Render::Get();
	render.BindTexture( *this );

	// Determine the OpenGL state to use.
	GLenum glMode = repeat ? GL_REPEAT : GL_CLAMP_TO_EDGE;

	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, glMode );
	glErrorCheck();

	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, glMode );
	glErrorCheck();

	_repeated = repeat;
}

/*
===============================================================================
Texture::Reserve
	Allocates enough memory to contain an image of the given size.
===============================================================================
*/
void Texture::Reserve( unsigned int width, unsigned int height )
{
	// Set this texture as active.
	Render &render = Render::Get();
	render.BindTexture( *this );

	// Upload pixel data.
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL );
	glErrorCheck();

	// Save state.
	_width = width;
	_height = height;
}

/*
===============================================================================
Texture::UploadPixels
	Gives the texture new pixel data.
===============================================================================
*/
void Texture::UploadPixels( bool generateMipmaps, unsigned int width, unsigned int height, Texture::pixelFormat_e format, void *pPixels )
{
	sbAssert( pPixels );

	// Set this texture as active.
	Render &render = Render::Get();
	render.BindTexture( *this );

	// Ensure this texture will build mipmaps.
	GLenum mipmapState = generateMipmaps ? GL_TRUE : GL_FALSE;
	glTexParameteri( GL_TEXTURE_2D, GL_GENERATE_MIPMAP, mipmapState );
	glErrorCheck();

	// Upload pixel data.
	GLenum glFmt = PixelFormatToGl( format );
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, glFmt, GL_UNSIGNED_BYTE, pPixels );
	glErrorCheck();

	// Save state.
	_width = width;
	_height = height;
}

/*
===============================================================================
Texture::PixelFormatToGl
	Converts a `pixelFormat_e` to its OpenGL equal.
===============================================================================
*/
GLenum Texture::PixelFormatToGl( Texture::pixelFormat_e fmt )
{
	switch( fmt )
	{
		case FORMAT_RGB:
			return GL_RGB;

		case FORMAT_RGBA:
			return GL_RGBA;
	}

	// Invalid format.
	sbAssert( 0 );
	return GL_RGB;
}