/*
===============================================================================

	RenderPrivate.h
		Private renderer definitions.

===============================================================================
*/

#ifndef __RENDERPRIVATE_H__
#define __RENDERPRIVATE_H__

#include "System/OpenGL.h"
#include "Render/Render.h"

namespace sandbox
{
	namespace render
	{

		// Speed sapping OpenGL error checking.
#ifndef sbDEBUG
		#define glErrorCheck()	
#else
		void glErrorCheck();
#endif

		/*
		===============================================================================
		RenderState
			Simple POD type which contains the renders current state.
		===============================================================================
		*/
		class RenderState
		{
		public:
							RenderState();

			/*
			-------------------------------------------------------------------------
				Program management.
			-------------------------------------------------------------------------
			*/
			GLint			GetCurrentProgramName() const;
			Program *		GetCurrentProgram() const;
			void			SetCurrentProgram( const Program &program );
			void			PushProgram( const Program &program );
			void			PopProgram();
			void			ClearProgram();

			/*
			-------------------------------------------------------------------------
				Texture management.
			-------------------------------------------------------------------------
			*/
			GLuint			GetCurrentFramebuffer() const;
			void			SetCurrentFramebuffer( GLuint fb );
			void			PushFramebuffer( GLuint fb );
			void			PopFramebuffer();

			/*
			-------------------------------------------------------------------------
				Matrix management.
			-------------------------------------------------------------------------
			*/
			bool			IsMatrixDirty( Render::matrix_e type );
			void			SetMatrixDirty( Render::matrix_e type, bool dirty );

			/*
			-------------------------------------------------------------------------
				Misc.
			-------------------------------------------------------------------------
			*/
			void			SetCurrentColor3f( const math::Color3f &color );
			void			SetCurrentColor4f( const math::Color4f &color );
			const math::Color4f &GetCurrentColor() const;

			GLuint			currentTexture;

		private:
			std::vector<GLuint>	_fbStack;
			std::vector<Program *>	_programStack;
			bool			_matrixDirty[Render::MATRIX_COUNT];
			math::Color4f	_currentColor;
		};

		/*
		===============================================================================
		GlExtensions
			Simple POD type which contains the renders current state.
		===============================================================================
		*/
		class GlExtensions
		{
		public:
			static bool		NonPow2TextureSupport();

		private:
			static bool		IsExtensionSupported( const char *pExtension );
		};

		extern RenderState gRenderState;

	} // namespace render;
} // namespace sandbox;


#endif // __RENDERPRIVATE_H__