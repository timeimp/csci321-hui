/*
===============================================================================

	Render.cpp
		OpenGL abstraction layer implementation.

===============================================================================
*/

#include "Framework/Sandbox.h"
#include "Render/Render.h"
#include "System/OpenGL.h"
#include "Framework/Application.h"
#include "System/Window.h"
#include "Render/Vertex.h"

#include "RenderPrivate.h"

#include <stdlib.h>

using namespace std;
using namespace sandbox::math;
using namespace sandbox::system;
using namespace sandbox::render;
using namespace sandbox::framework;

/*
===============================================================================
	Render implemenation.
===============================================================================
*/

Render *Render::_pRender = NULL;

/*
===============================================================================
Render::Render
	Creates the singleton instance.
===============================================================================
*/
Render::Render()
{
	InitOpenGL();
	InitMatrices();
}

/*
===============================================================================
Render::~Render
	Cleans the singleton instance.
===============================================================================
*/
Render::~Render()
{
}

/*
===============================================================================
Render::Create
	Creates the singleton instance.
===============================================================================
*/
void Render::Create()
{
	if( !_pRender )
	{
		_pRender = new Render();
	}
}

/*
===============================================================================
Render::Get
	Provides access to the singleton instance.
===============================================================================
*/
Render &Render::Get()
{
	return *_pRender;
}

/*
===============================================================================
Render::Destroy
	Cleans the primitive singleton.
===============================================================================
*/
void Render::Destroy()
{
	if( !_pRender )
	{
		delete _pRender;
	}
}

/*
===============================================================================
Render::Clear
	Wipes the current backbuffer.
===============================================================================
*/
void Render::Clear( int flags )
{
	GLenum glFlags = 0;

	// Must clear at least 1 buffer.
	sbAssert( flags );

	if( sbTEST_FLAG32( flags, CLEAR_COLOR ) )
	{
		glFlags |= GL_COLOR_BUFFER_BIT;
	}

	if( sbTEST_FLAG32( flags, CLEAR_DEPTH ) )
	{
		glFlags |= GL_DEPTH_BUFFER_BIT;
	}

	glClear( glFlags );
	glErrorCheck();
}

/*
===============================================================================
Render::SetCameraActive
	Updates the viewport and view matrices for the given camera.
===============================================================================
*/
void Render::SetCameraActive( const Camera &camera )
{
	// Update the projection matrix.
	float aspectRatio = camera.GetAspectRatio();
	float verticalFov = camera.GetVerticalFov();
	float zNear = camera.GetZNear();
	float zFar = camera.GetZFar();
	Matrix4f proj = Matrix4f::BuildPerspective( verticalFov, aspectRatio, zNear, zFar );
	SetMatrixf( MATRIX_PROJECTION, proj );

	// Update the view matrix.
	Matrix4f view;
	view.TranslatePoint3f( camera.GetPosition() );
	view.RotateAngle3f( camera.GetAngles() );

	// Update the viewport.
	if( camera.IsFullscreen() )
	{
		const Application *pApp = Application::GetApp();
		const Window *pWindow = pApp->AppWindow();

		int width, height;
		pWindow->GetSize( &width, &height );

		glViewport( 0, 0, width, height );
	}
	else
	{
		const viewport_t &viewport = camera.GetViewport();
		glViewport( viewport._x, viewport._y, viewport._width, viewport._height );
	}
}

/*
===============================================================================
Render::DrawTexturedScreenOverlay
	Draws a colored texture over the top of the screen.
===============================================================================
*/
void Render::DrawTexturedScreenOverlay( const Texture &texture, const Point2f &tc0, const Point2f &tc1, const Color4f &color )
{
	// Set the new texture.
	GLuint previousTexture = gRenderState.currentTexture;
	BindTexture( texture );

	gRenderState.SetCurrentColor4f( color );

	// Reset all matrices to the indentity matrix.
	Matrix4f m;
	PushMatrix( MATRIX_MODEL );
	PushMatrix( MATRIX_VIEW );
	PushMatrix( MATRIX_PROJECTION );
	SetMatrixf( MATRIX_MODEL, m );
	SetMatrixf( MATRIX_VIEW, m );
	SetMatrixf( MATRIX_PROJECTION, m );

	// Upload data to OpenGL.
	UploadMatrices();
	SetProgramInternalUniforms( RENDER_COLORED_BIT | RENDER_TEXTURED_BIT );

	// Specify the vertices to draw.
	glBegin( GL_TRIANGLE_STRIP );
		glTexCoord2f( tc0.GetX(), tc0.GetY() );
		glVertex3f( -1.0f,	-1.0f,	0.0f );

		glTexCoord2f( tc1.GetX(), tc0.GetY() );
		glVertex3f( 1.0f,	-1.0f,	0.0f );

		glTexCoord2f( tc0.GetX(), tc1.GetY() );
		glVertex3f( -1.0f,	1.0f,	0.0f );

		glTexCoord2f( tc1.GetX(), tc1.GetY() );
		glVertex3f( 1.0f,	1.0f,	0.0f );
	glEnd();

	// Revert all matrices to their previous state.
	PopMatrix( MATRIX_MODEL );
	PopMatrix( MATRIX_VIEW );
	PopMatrix( MATRIX_PROJECTION );

	// Revert texture.
	glBindTexture( GL_TEXTURE_2D, previousTexture );
}

/*
===============================================================================
Render::DrawColoredScreenOverlay
	Draws a color over the top of the screen.
===============================================================================
*/
void Render::DrawColoredScreenOverlay( const Color4f &color )
{
	gRenderState.SetCurrentColor4f( color );

	// Reset all matrices to the indentity matrix.
	Matrix4f m;
	PushMatrix( MATRIX_MODEL );
	PushMatrix( MATRIX_VIEW );
	PushMatrix( MATRIX_PROJECTION );
	SetMatrixf( MATRIX_MODEL, m );
	SetMatrixf( MATRIX_VIEW, m );
	SetMatrixf( MATRIX_PROJECTION, m );

	UploadMatrices();
	SetProgramInternalUniforms( RENDER_COLORED_BIT );

	glBegin( GL_TRIANGLE_STRIP );
		glVertex3f( -1.0f,	-1.0f,	0.0f );
		glVertex3f( 1.0f,	-1.0f,	0.0f );
		glVertex3f( -1.0f,	1.0f,	0.0f );
		glVertex3f( 1.0f,	1.0f,	0.0f );
	glEnd();

	// Revert all matrices to their previous state.
	PopMatrix( MATRIX_MODEL );
	PopMatrix( MATRIX_VIEW );
	PopMatrix( MATRIX_PROJECTION );
}

/*
===============================================================================
Render::DrawLine
	Draws a line between the given points.
===============================================================================
*/
void Render::DrawLine( const Point3f &v0, const Point3f &v1, const Color4f &color )
{
	gRenderState.SetCurrentColor4f( color );

	UploadMatrices();
	SetProgramInternalUniforms( RENDER_COLORED_BIT );

	glBegin( GL_LINES );
		glVertex3fv( v0.Base() );
		glVertex3fv( v1.Base() );
	glEnd();
}

/*
===============================================================================
Render::DrawIndexed
	Draws the currently bound vertex buffer with the given indices.
===============================================================================
*/
void Render::DrawIndexed( drawMode_e mode, renderFlags_e flags, const int *pIndices, int count )
{
	UploadMatrices();
	SetProgramInternalUniforms( flags );

	glVertexAttribPointer( 0, Vertex::VERTEX_COMPONENTS, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid *)Vertex::GetPointOffset() );
	glEnableVertexAttribArray( 0 );
	glDrawElements( GL_TRIANGLES, 36, GL_UNSIGNED_INT, pIndices );
}

/*
===============================================================================
Render::PushMatrix
	Adds a new matrix onto the given stack.
===============================================================================
*/
void Render::PushMatrix( matrix_e type )
{
	sbAssert( type < MATRIX_COUNT );

	vector<Matrix4f> &stack = _matrices[type];

	// Duplicate the top level matrix.
	Matrix4f temp = stack.back();
	stack.push_back( temp );

	gRenderState.SetMatrixDirty( type, true );
}

/*
===============================================================================
Render::PopMatrix
	Removes the top of the given matrix stack.
===============================================================================
*/
void Render::PopMatrix( matrix_e type )
{
	sbAssert( type < MATRIX_COUNT );

	vector<Matrix4f> &stack = _matrices[type];

	if( stack.size() == 1 )
	{
		// Can't remove the last matrix.
		return;
	}

	stack.pop_back();

	gRenderState.SetMatrixDirty( type, true );
}

/*
===============================================================================
Render::SetMatrixf
	Updates the values of the specified matrix.
===============================================================================
*/
void Render::SetMatrixf( matrix_e type, const Matrix4f &m )
{
	sbAssert( type < MATRIX_COUNT );

	// Assign the new matrix to the top of the matrix stack.
	Matrix4f &topMatrix = _matrices[type].back();
	topMatrix = m;

	gRenderState.SetMatrixDirty( type, true );
}

/*
===============================================================================
Render::UploadMatrices
	Sends the top level matrices to OpenGL.
===============================================================================
*/
void Render::UploadMatrices()
{
	if( gRenderState.IsMatrixDirty( MATRIX_VIEW ) || gRenderState.IsMatrixDirty( MATRIX_MODEL ) )
	{
		// Build the model-view matrix.
		const Matrix4f &modelMatrix = _matrices[MATRIX_MODEL].back();
		const Matrix4f &viewMatrix = _matrices[MATRIX_VIEW].back();
		Matrix4f modelViewMatrix = modelMatrix * viewMatrix;
		modelViewMatrix.Transpose();

		// Upload the model-view matrix.
		glMatrixMode( GL_MODELVIEW );
		glLoadMatrixf( modelViewMatrix.Get() );

		gRenderState.SetMatrixDirty( MATRIX_VIEW, FALSE );
		gRenderState.SetMatrixDirty( MATRIX_MODEL, FALSE );
	}

	if( gRenderState.IsMatrixDirty( MATRIX_PROJECTION ) )
	{
		// Make a local copy of the projection matrix and convert it to OpenGL's order.
		Matrix4f projectionMatrix = _matrices[MATRIX_PROJECTION].back();
		projectionMatrix.Transpose();

		// Upload the model-view matrix.
		glMatrixMode( GL_PROJECTION );
		glLoadMatrixf( projectionMatrix.Get() );

		gRenderState.SetMatrixDirty( MATRIX_PROJECTION, FALSE );
	}
}

/*
===============================================================================
Render::BindProgram
	Sets a shader program as active.
===============================================================================
*/
void Render::BindProgram( const Program &prog )
{
	GLint progName = prog.GetName();

	if( gRenderState.GetCurrentProgramName() == progName )
	{
		// Attempting to rebind the same program twice.
		return;
	}

	glUseProgram( progName );
	glErrorCheck();

	gRenderState.SetCurrentProgram( prog );
}

/*
===============================================================================
Render::UnbindProgram
	Removes the active shader program.
===============================================================================
*/
void Render::UnbindProgram()
{
	glUseProgram( 0 );
	glErrorCheck();

	gRenderState.ClearProgram();
}

/*
===============================================================================
Render::BindTexture
	Sets a texture as active in sampler 0.
===============================================================================
*/
void Render::BindTexture( const Texture &texture )
{
	GLuint texName = texture.GetName();

	if( gRenderState.currentTexture == texName )
	{
		// No state change.
		return;
	}

	glBindTexture( GL_TEXTURE_2D, texName );
	glErrorCheck();

	// Set this texture as the top of the matrix stack.
	gRenderState.currentTexture = texName;
}

/*
===============================================================================
Render::BindTextureFromFramebuffer
	Sets a texture as active in sampler 0.
===============================================================================
*/
void Render::BindTextureFromFramebuffer( const Framebuffer &fb )
{
	BindTexture( *fb.GetTexture() );
}

/*
===============================================================================
Render::UnbindTexture
	Removes the active texture object.
===============================================================================
*/
void Render::UnbindTexture()
{
	glBindTexture( GL_TEXTURE_2D, 0 );
	glErrorCheck();

	gRenderState.currentTexture = 0;
}

/*
===============================================================================
Render::PushFramebuffer
	Sets the given framebuffer as active.
===============================================================================
*/
void Render::PushFramebuffer( const Framebuffer &fb )
{
	// Add a new dummy value to the top of the stack.
	// Ensure it is overwritten in `BindFramebuffer`.
	gRenderState.PushFramebuffer( -1 );

	BindFramebuffer( fb );
}

/*
===============================================================================
Render::PopFramebuffer
	Removes the active framebuffer object.
===============================================================================
*/
void Render::PopFramebuffer()
{
	// Get and remove the new stack top.
	gRenderState.PopFramebuffer();
	GLuint newBind = gRenderState.GetCurrentFramebuffer();

	// Bind the new stack top.
	glBindFramebufferEXT( GL_FRAMEBUFFER_EXT, newBind );
	glErrorCheck();
}

/*
===============================================================================
Render::BindFramebuffer
	Sets the active framebuffer to render to.
===============================================================================
*/
void Render::BindFramebuffer( const Framebuffer &fb )
{
	GLuint fbName = fb.GetName();

	if( gRenderState.GetCurrentFramebuffer() == fbName )
	{
		// No state change.
		return;
	}

	glBindFramebufferEXT( GL_FRAMEBUFFER_EXT, fbName );
	glErrorCheck();

	// Set this texture as the top of the matrix stack.
	gRenderState.SetCurrentFramebuffer( fbName );
}

/*
===============================================================================
Render::UnbindTexture
	Removes the active texture object.
===============================================================================
*/
void Render::UnbindFramebuffer()
{
	glBindFramebufferEXT( GL_FRAMEBUFFER_EXT, 0 );
	glErrorCheck();

	gRenderState.SetCurrentFramebuffer( 0 );
}

/*
===============================================================================
Render::InitMatrices
	Sets the initial matrix values.
===============================================================================
*/
void Render::InitMatrices()
{
	// Push identity matrices onto all stacks.
	for( int i = 0 ; i < MATRIX_COUNT ; i++ )
	{
		Matrix4f identity;
		_matrices[i].push_back( identity );
	}
}

/*
===============================================================================
Render::InitOpenGL
	Sets the initial OpenGL state.
===============================================================================
*/
void Render::InitOpenGL()
{
	glEnable( GL_DEPTH_TEST );
	glEnable( GL_ALPHA_TEST );
	glEnable( GL_BLEND );

	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
}

/*
===============================================================================
Render::SetProgramInternalUniforms
	Uploads the internal uniforms to the current program.
===============================================================================
*/
void Render::SetProgramInternalUniforms( int flags )
{
	Program *pProgram = gRenderState.GetCurrentProgram();

	// We must always be drawing in shader mode.
	sbAssert( pProgram );

	const GLint *pUniforms = pProgram->GetInternalUniformArray();

	if( pUniforms[Program::INTERNAL_UNIFORM_IS_COLORED] != -1 )
	{
		int value = sbTEST_FLAG32( flags, RENDER_COLORED ) > 0;
		pProgram->SetUniform1i( "uIsColored", value );

		if( value )
		{
			const Color4f &color = gRenderState.GetCurrentColor();
			pProgram->SetUniform4fv( "uColor", color.Base() );
		}
	}

	if( pUniforms[Program::INTERNAL_UNIFORM_IS_TEXTURED] != -1 )
	{
		int value = sbTEST_FLAG32( flags, RENDER_TEXTURED ) > 0;
		pProgram->SetUniform1i( "uIsTextured", value );

		if( value )
		{
			glEnable( GL_TEXTURE_2D );
		}
		else
		{
			glDisable( GL_TEXTURE_2D );
		}
	}
}