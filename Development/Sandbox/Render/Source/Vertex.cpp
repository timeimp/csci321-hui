/*
===============================================================================

	Vertex.cpp
		Implements the vertex class.

===============================================================================
*/

#include "Framework/Sandbox.h"
#include "Render/Vertex.h"

using namespace sandbox::render;
using namespace sandbox::math;

/*
===============================================================================
	Vertex implementation
===============================================================================
*/

/*
===============================================================================
Vertex::SetPoint
	Updates the point at which the vertex is drawn;
===============================================================================
*/
void Vertex::SetPoint( const Point3f &point )
{
	_point = point;
}

/*
===============================================================================
Vertex::SetTexCoord
	Updates a single texture coordinate of a vertex;
===============================================================================
*/
void Vertex::SetTexCoord( int idx, const Point2f &tc )
{
	sbAssert( idx < MAX_TEXCOORDS );
	_texcoords[idx] = tc;
}

/*
===============================================================================
Vertex::SetColor
	Updates the color of a vertex;
===============================================================================
*/
void Vertex::SetColor( const Color3f &color )
{
	_color = color;
}