/*
===============================================================================

	Primitive.cpp
		Implements the primitive rendering interface.

===============================================================================
*/

#include "Framework/Sandbox.h"
#include "Render/Primitive.h"
#include "Render/Render.h"
#include "System/OpenGL.h"
#include "Math/Math.h"

#include "RenderPrivate.h"

#include <stdlib.h>

using namespace sandbox::render;
using namespace sandbox::math;

/*
===============================================================================
	Primitive implemenation.
===============================================================================
*/

Primitive *Primitive::_pPrimitive = NULL;

/*
===============================================================================
Primitive::Primitive
	Creates the singleton instance.
===============================================================================
*/
Primitive::Primitive()
{
	CreateRectangularPrim();
	CreateCocoon();
}

/*
===============================================================================
Primitive::Create
	Creates the singleton instance.
===============================================================================
*/
void Primitive::Create()
{
	if( !_pPrimitive )
	{
		_pPrimitive = new Primitive();
	}
}

/*
===============================================================================
Primitive::Get
	Provides access to the singleton instance.
===============================================================================
*/
Primitive &Primitive::Get()
{
	return *_pPrimitive;
}

/*
===============================================================================
Primitive::Destroy
	Cleans the primitive singleton.
===============================================================================
*/
void Primitive::Destroy()
{
	if( !_pPrimitive )
	{
		delete _pPrimitive;
	}
}

/*
===============================================================================
Primitive::DrawPlaneSegment
	Draws a cube in the given position at the given orientation.
===============================================================================
*/
void Primitive::DrawPlaneSegment( const PlaneSegment &plane, const Color4f &color ) const
{
	const Angle3f angles = vector_to_angle( plane.GetNormal() );
	const Point3f center = plane.GetPoint();

	float xSize, zSize;
	plane.GetExtent( &xSize, &zSize );

	DrawRectangularPrism( center, angles, color, xSize, 0.0f, zSize );
}

/*
===============================================================================
Primitive::DrawCube
	Draws a cube in the given position at the given orientation.
===============================================================================
*/
void Primitive::DrawCube( const Point3f &center, const Angle3f &angles, const Color4f &color, float size ) const
{
	DrawRectangularPrism( center, angles, color, size, size, size );
}

/*
===============================================================================
Primitive::DrawRectangularPrism
	Draws a rectangular prism in the given position at the given orientation.
===============================================================================
*/
void Primitive::DrawRectangularPrism( const Point3f &center, const Angle3f &angles, const Color4f &color, float width, float height, float depth ) const
{
	// Build the model matrix.
	Matrix4f model;
	model.TranslatePoint3f( center );
	model.RotateAngle3f( angles );
	model.Scale( width, height, depth );

	// Update the renderer model matrix.
	Render &render = Render::Get();
	render.PushMatrix( Render::MATRIX_MODEL );
	render.SetMatrixf( Render::MATRIX_MODEL, model );

	// Draw the prism.
	gRenderState.SetCurrentColor4f( color );
	glBindBuffer( GL_ARRAY_BUFFER, _rectVertexBuffer );
	render.DrawIndexed( Render::DRAW_TRIANGLE_LIST, Render::RENDER_COLORED_BIT, _rectPrismIndices, 36 );

	// Revert the model matrix.
	render.PopMatrix( Render::MATRIX_MODEL );
}

/*
===============================================================================
Primitive::DrawLine
	Draws a line between the given points.
===============================================================================
*/
void Primitive::DrawLine( const Point3f &start, const Point3f &end, const Color4f &color ) const
{
	Render &render = Render::Get();
	render.DrawLine( start, end, color );
}

/*
===============================================================================
Primitive::DrawSphere
	Draws a sphere in the given position.
===============================================================================
*/
void Primitive::DrawSphere( const Point3f &center, const Color4f &color, float size ) const
{
	Angle3f angles;

	DrawCocoon( center, angles, color, size, size, size );
}

/*
===============================================================================
Primitive::DrawCocoon
	Draws a stretched sphere in the given position.
===============================================================================
*/
void Primitive::DrawCocoon( const Point3f &center, const Angle3f &angles, const Color4f &color, float width, float height, float depth ) const
{
	
}

/*
===============================================================================
Primitive::CreateRectangularPrim
	Initializes the rectangular prism vertices.
===============================================================================
*/
void Primitive::CreateRectangularPrim()
{
	Point3f points[8] = 
	{
		Point3f( -0.5f,		0.5f,	-0.5f ),
		Point3f( 0.5f,		0.5f,	-0.5f ),
		Point3f( 0.5,		-0.5f,	-0.5f ),
		Point3f( -0.5f,		-0.5f,	-0.5f ),
		Point3f( -0.5f,		0.5f,	0.5f ),
		Point3f( 0.5f,		0.5f,	0.5f ),
		Point3f( 0.5f,		-0.5f,	0.5f ),
		Point3f( -0.5f,		-0.5f,	0.5f ),
	};

	for( int i = 0 ; i < 8 ; i++ )
	{
		_rectPrismVerts[i].SetPoint( points[i] );
	}

	_rectPrismIndices[0] = 3;
	_rectPrismIndices[1] = 2;
	_rectPrismIndices[2] = 1;
	_rectPrismIndices[3] = 0;
	_rectPrismIndices[4] = 3;
	_rectPrismIndices[5] = 1;

	_rectPrismIndices[6] = 1;
	_rectPrismIndices[7] = 2;
	_rectPrismIndices[8] = 5;
	_rectPrismIndices[9] = 2;
	_rectPrismIndices[10] = 6;
	_rectPrismIndices[11] = 5;

	_rectPrismIndices[12] = 5;
	_rectPrismIndices[13] = 6;
	_rectPrismIndices[14] = 7;
	_rectPrismIndices[15] = 7;
	_rectPrismIndices[16] = 4;
	_rectPrismIndices[17] = 5;

	_rectPrismIndices[18] = 4;
	_rectPrismIndices[19] = 0;
	_rectPrismIndices[20] = 1;
	_rectPrismIndices[21] = 4;
	_rectPrismIndices[22] = 1;
	_rectPrismIndices[23] = 5;

	_rectPrismIndices[24] = 7;
	_rectPrismIndices[25] = 3;
	_rectPrismIndices[26] = 2;
	_rectPrismIndices[27] = 7;
	_rectPrismIndices[28] = 2;
	_rectPrismIndices[29] = 1;

	_rectPrismIndices[30] = 0;
	_rectPrismIndices[31] = 3;
	_rectPrismIndices[32] = 7;
	_rectPrismIndices[33] = 0;
	_rectPrismIndices[34] = 4;
	_rectPrismIndices[35] = 7;

	glGenBuffers( 1, &_rectVertexBuffer );
	glBindBuffer( GL_ARRAY_BUFFER, _rectVertexBuffer );
	glBufferData( GL_ARRAY_BUFFER, sizeof(Vertex) * 8, _rectPrismVerts, GL_STATIC_DRAW );
}

/*
===============================================================================
Primitive::CreateCocoon
	Initializes the cocoon vertices.
===============================================================================
*/
void Primitive::CreateCocoon()
{
	
}