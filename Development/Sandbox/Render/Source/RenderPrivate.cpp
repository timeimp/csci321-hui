/*
===============================================================================

	Render.cpp
		OpenGL abstraction layer implementation.

===============================================================================
*/

#include "Framework/Sandbox.h"
#include "System/OpenGL.h"

#include "RenderPrivate.h"

using namespace sandbox::math;
using namespace sandbox::system;
using namespace sandbox::render;

RenderState sandbox::render::gRenderState;

/*
===============================================================================
glErrorCheck
	Checks the last recorded OpenGL error.
===============================================================================
*/
void sandbox::render::glErrorCheck()
{
	GLenum error = glGetError();
	sbAssert( error == GL_NO_ERROR );
}

/*
===============================================================================
	RenderState implementation.
===============================================================================
*/

/*
===============================================================================
RenderState::RenderState
	Sets the render state variable to match the default OpenGL state values.
===============================================================================
*/
RenderState::RenderState()
{
	currentTexture = -1;
	_currentColor.Set( 1.0f, 1.0f, 1.0f, 1.0f );
	
	for( int i = 0 ; i < Render::MATRIX_COUNT ; i++ )
	{
		_matrixDirty[i] = true;
	}
}

GLuint RenderState::GetCurrentFramebuffer() const
{
	return _fbStack.empty() ? 0 : _fbStack.front();
}

void RenderState::SetCurrentFramebuffer( GLuint fb )
{
	if( _fbStack.empty() )
	{
		// Stack is empty. Must place new value.
		_fbStack.push_back( fb );
	}
	else
	{
		// Overwrite the existing top value.
		_fbStack.front() = fb;
	}
}

void RenderState::PushFramebuffer( GLuint fb )
{
	_fbStack.push_back( fb );
}

void RenderState::PopFramebuffer()
{
	if( !_fbStack.empty() )
	{
		_fbStack.pop_back();
	}
}

GLint RenderState::GetCurrentProgramName() const
{
	return _programStack.empty() ? 0 : _programStack.front()->GetName();
}

Program *RenderState::GetCurrentProgram() const
{
	return _programStack.empty() ? NULL : _programStack.front();
}

void RenderState::SetCurrentProgram( const Program &program )
{
	if( _programStack.empty() )
	{
		// Stack is empty. Must place new value.
		Program *pProgram = (Program *)&program;
		_programStack.push_back( pProgram );
	}
	else
	{
		// Overwrite the existing top value.
		_programStack.front() = (Program *)&program;
	}
}

void RenderState::PushProgram( const Program &program )
{
	Program *pProgram = (Program *)&program;
	_programStack.push_back( pProgram );
}

void RenderState::PopProgram()
{
	if( !_programStack.empty() )
	{
		_programStack.pop_back();
	}
}

void RenderState::ClearProgram()
{
	if( !_programStack.empty() )
	{
		_programStack.front() = NULL;
	}
}

bool RenderState::IsMatrixDirty( Render::matrix_e type )
{
	return _matrixDirty[type];
}

void RenderState::SetMatrixDirty( Render::matrix_e type, bool dirty )
{
	_matrixDirty[type] = dirty;
}

void RenderState::SetCurrentColor3f( const Color3f &color )
{
	_currentColor.Set( color.GetX(), color.GetY(), color.GetZ(), 1.0f );
}

void RenderState::SetCurrentColor4f( const Color4f &color )
{
	_currentColor = color;
}

const Color4f &RenderState::GetCurrentColor() const
{
	return _currentColor;
}

/*
===============================================================================
	GlExtensions implementation.
===============================================================================
*/

/*
===============================================================================
GlExtensions::NonPow2TextureSupport
	Determines if the current machine supports non power of 2 textures.
===============================================================================
*/
bool GlExtensions::NonPow2TextureSupport()
{
#ifdef sbDISABLE_ATI_NPOT
	const char *vendor = (const char *)glGetString( GL_VENDOR );
	if( !strncmp( vendor, "ATI", 3 ) )
	{
		// ATi "supports" non power of two textures in their X1600 GL drivers.
		// Just say that no ATi card supports NPOT.
		return false;
	}
#endif // sbDISABLE_ATI_NPOT

	static bool supported = IsExtensionSupported( "GL_ARB_texture_non_power_of_two" );
	return supported;
}

/*
===============================================================================
GlExtensions::IsExtensionSupported
	Determines if the current machine supports non power of 2 textures.
===============================================================================
*/
bool GlExtensions::IsExtensionSupported( const char *pExtension )
{
	const GLubyte *pExtList = glGetString( GL_EXTENSIONS );
	return strstr( (char *)pExtList, pExtension ) != NULL;
}