/*
===============================================================================

	Render.h
		Public renderer interface.

===============================================================================
*/

#ifndef __RENDER_H__
#define __RENDER_H__

#include "Math/Point.h"
#include "Math/Matrix.h"
#include "Render/Camera.h"
#include "Render/Shader.h"
#include "Render/Texture.h"
#include "Render/Framebuffer.h"

#include <vector>

namespace sandbox
{
	namespace render
	{

		/*
		===============================================================================
		Render
			High level rendering methods.
		===============================================================================
		*/
		class Render
		{
		public:
			enum matrix_e
			{
				MATRIX_MODEL,
				MATRIX_VIEW,
				MATRIX_PROJECTION,

				MATRIX_COUNT
			};

			enum drawMode_e
			{
				DRAW_TRIANGLE_LIST,

				DRAW_COUNT
			};

			enum renderFlags_e
			{
				RENDER_COLORED,
				RENDER_TEXTURED,

				RENDER_COLORED_BIT			= sbFLAG( RENDER_COLORED ),
				RENDER_TEXTURED_BIT			= sbFLAG( RENDER_TEXTURED ),
			};

			enum clearFlags_e
			{
				CLEAR_COLOR,
				CLEAR_DEPTH,

				CLEAR_COLOR_BIT				= sbFLAG( CLEAR_COLOR ),
				CLEAR_DEPTH_BIT				= sbFLAG( CLEAR_DEPTH ),
			};

			/*
			-------------------------------------------------------------------------
				Singleton.
			-------------------------------------------------------------------------
			*/
			static void		Create();
			static Render &	Get();
			static void		Destroy();

			/*
			-------------------------------------------------------------------------
				Camera.
			-------------------------------------------------------------------------
			*/
			void			Clear( int flags );
			void			SetCameraActive( const Camera &camera );

			/*
			-------------------------------------------------------------------------
				Drawing.
			-------------------------------------------------------------------------
			*/
			void			DrawTexturedScreenOverlay( const Texture &texture, const math::Point2f &tc0, const math::Point2f &tc1, const math::Color4f &color );
			void			DrawColoredScreenOverlay( const math::Color4f &color );
			void			DrawLine( const math::Point3f &v0, const math::Point3f &v1, const math::Color4f &color );
			void			DrawIndexed( drawMode_e mode, renderFlags_e flags, const int *pIndices, int count );

			/*
			-------------------------------------------------------------------------
				Matrix manipulation.
			-------------------------------------------------------------------------
			*/
			void			PushMatrix( matrix_e type );
			void			PopMatrix( matrix_e type );
			void			SetMatrixf( matrix_e type, const math::Matrix4f &m );

			/*
			-------------------------------------------------------------------------
				Shader manipulation.
			-------------------------------------------------------------------------
			*/
			void			BindProgram( const Program &prog );
			void			UnbindProgram();

			/*
			-------------------------------------------------------------------------
				Texture manipulation.
			-------------------------------------------------------------------------
			*/
			void			BindTexture( const Texture &texture );
			void			BindTextureFromFramebuffer( const Framebuffer &fb );
			void			UnbindTexture();

			/*
			-------------------------------------------------------------------------
				Framebuffer manipulation.
			-------------------------------------------------------------------------
			*/
			void			PushFramebuffer( const Framebuffer &fb );
			void			PopFramebuffer();
			void			BindFramebuffer( const Framebuffer &fb );
			void			UnbindFramebuffer();

		private:
			/*
			-------------------------------------------------------------------------
				Singleton.
			-------------------------------------------------------------------------
			*/
							Render();
							~Render();
			static Render *	_pRender;

			/*
			-------------------------------------------------------------------------
				Initialization.
			-------------------------------------------------------------------------
			*/
			void			InitMatrices();
			void			InitOpenGL();

			/*
			-------------------------------------------------------------------------
				Drawing.
			-------------------------------------------------------------------------
			*/
			void			SetProgramInternalUniforms( int flags );

			/*
			-------------------------------------------------------------------------
				Matrix states.
			-------------------------------------------------------------------------
			*/
			void			UploadMatrices();

			std::vector<math::Matrix4f>	_matrices[MATRIX_COUNT];
		};

	} // namespace render;
} // namespace sandbox;

#endif // __RENDER_H__