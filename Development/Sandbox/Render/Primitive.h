/*
===============================================================================

	Primitive.h
		Primitive object rendering interface.

===============================================================================
*/

#ifndef __PRIMITIVE_H__
#define __PRIMITIVE_H__

#include "Math/Point.h"
#include "Math/Plane.h"
#include "Render/Vertex.h"
#include "System/OpenGL.h"

namespace sandbox
{
	namespace render
	{

		/*
		===============================================================================
		Primitive
			Renders a primitive in the given position.
		===============================================================================
		*/
		class Primitive
		{
		public:
			/*
			-------------------------------------------------------------------------
				Singleton.
			-------------------------------------------------------------------------
			*/
			static void		Create();
			static Primitive &Get();
			static void		Destroy();

			/*
			-------------------------------------------------------------------------
				Drawing.
			-------------------------------------------------------------------------
			*/
			void			DrawPlaneSegment( const math::PlaneSegment &plane, const math::Color4f &color ) const;
			void			DrawCube( const math::Point3f &center, const math::Angle3f &angles, const math::Color4f &color, float size ) const;
			void			DrawRectangularPrism( const math::Point3f &center, const math::Angle3f &angles, const math::Color4f &color, float width, float height, float depth ) const;
			void			DrawLine( const math::Point3f &start, const math::Point3f &end, const math::Color4f &color ) const;
			void			DrawSphere( const math::Point3f &center, const math::Color4f &color, float size ) const;
			void			DrawCocoon( const math::Point3f &center, const math::Angle3f &angles, const math::Color4f &color, float width, float height, float depth ) const;

		private:
			/*
			-------------------------------------------------------------------------
				Initialization.
			-------------------------------------------------------------------------
			*/
			void			CreateRectangularPrim();
			void			CreateCocoon();

			/*
			-------------------------------------------------------------------------
				Singleton.
			-------------------------------------------------------------------------
			*/
							Primitive();
			static Primitive *_pPrimitive;

			/*
			-------------------------------------------------------------------------
				Vertices.
			-------------------------------------------------------------------------
			*/
			GLuint			_rectVertexBuffer;
			Vertex			_rectPrismVerts[8];
			int				_rectPrismIndices[36];

			GLuint			_cocoonBuffer;
			Vertex			_cocoonVerts[8];
			int				_cocoonIndices[36];
		};

	} // namespace render;
} // namespace sandbox;

#endif // __PRIMITIVE_H__