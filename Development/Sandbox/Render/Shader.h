/*
===============================================================================

	Shader.h
		Abstracts the viewport and matrix.

===============================================================================
*/

#ifndef __SHADER_H__
#define __SHADER_H__

#include "Math/Matrix.h"
#include "Math/Vector.h"
#include "System/OpenGL.h"

#include <string>
#include <map>

namespace sandbox
{
	namespace render
	{

		/*
		===============================================================================
		Shader
			Common shader interface.
		===============================================================================
		*/
		class Shader
		{
		public:
			/*
			-------------------------------------------------------------------------
				Creation\destruction.
			-------------------------------------------------------------------------
			*/
							Shader( bool isVertex, const std::string &string );
							~Shader();

			/*
			-------------------------------------------------------------------------
				Accessors.
			-------------------------------------------------------------------------
			*/
			bool			IsValid();
			GLuint			GetName() const			{ return _shaderName; }

			/*
			-------------------------------------------------------------------------
				Mutators.
			-------------------------------------------------------------------------
			*/
			std::string		GetCompileLog()			{ return _errorString; }

		protected:
			GLuint			_shaderName;
			std::string		_shaderCode;
			std::string		_errorString;

		private:
			bool			ReadShaderFromFile( const std::string &path );
			bool			Compile( bool isVertex );
			void			DumpErrorLog();
		};

		/*
		===============================================================================
		VertexShader
			
		===============================================================================
		*/
		class VertexShader : public Shader
		{
		public:
			/*
			-------------------------------------------------------------------------
				Creation\destruction.
			-------------------------------------------------------------------------
			*/
							VertexShader( const std::string &string );
		};

		/*
		===============================================================================
		FragmentShader
			
		===============================================================================
		*/
		class FragmentShader : public Shader
		{
		public:
			/*
			-------------------------------------------------------------------------
				Creation\destruction.
			-------------------------------------------------------------------------
			*/
							FragmentShader( const std::string &string );
		};

		/*
		===============================================================================
		Program
			A vertex and fragment shader linked together.
		===============================================================================
		*/
		class Program
		{
		public:
			enum internalUniform_e
			{
				// Properties.
				INTERNAL_UNIFORM_COLOR,
				INTERNAL_UNIFORM_TEXTURE0,

				// Rendering hints.
				INTERNAL_UNIFORM_IS_COLORED,
				INTERNAL_UNIFORM_IS_TEXTURED,

				INTERNAL_UNIFORM_COUNT
			};

			/*
			-------------------------------------------------------------------------
				Creation\destruction.
			-------------------------------------------------------------------------
			*/
							Program();
							Program( const std::string &programName, const VertexShader &vertex, const FragmentShader &frag, const std::string &uniforms );
							~Program();

			/*
			-------------------------------------------------------------------------
				Accessors.
			-------------------------------------------------------------------------
			*/
			GLint			GetName() const					{ return _programName; }
			const GLint *	GetInternalUniformArray() const	{ return _internalUniforms; }
			int				GetInternalUniformArraySize() const	{ return sizeof(_internalUniforms); }

			/*
			-------------------------------------------------------------------------
				Initialization.
			-------------------------------------------------------------------------
			*/
			void			Init( const std::string &title, const VertexShader &vertex, const FragmentShader &frag, const std::string &uniforms );

			/*
			-------------------------------------------------------------------------
				Uniform uploading.
			-------------------------------------------------------------------------
			*/
			void			SetUniformPoint2f( const std::string &uniform, const math::Point2f &p );
			void			SetUniformPoint3f( const std::string &uniform, const math::Point3f &p );
			void			SetUniformPoint4f( const std::string &uniform, const math::Point4f &p );
			void			SetUniformColor3f( const std::string &uniform, const math::Color3f &c );
			void			SetUniformColor4f( const std::string &uniform, const math::Color4f &c );
			void			SetUniformVector3f( const std::string &uniform, const math::Vector3f &v );
			void			SetUniformMatrix4f( const std::string &uniform, const math::Matrix4f &m );

			void			SetUniform1f( const std::string &uniform, float v );
			void			SetUniform2f( const std::string &uniform, float v0, float v1 );
			void			SetUniform2fv( const std::string &uniform, const float *v );
			void			SetUniform3f( const std::string &uniform, float v0, float v1, float v2 );
			void			SetUniform3fv( const std::string &uniform, const float *v );
			void			SetUniform4f( const std::string &uniform, float v0, float v1, float v2, float v3 );
			void			SetUniform4fv( const std::string &uniform, const float *v );

			void			SetUniform1i( const std::string &uniform, int v );
			void			SetUniform2iv( const std::string &uniform, const int *v );
			void			SetUniform3iv( const std::string &uniform, const int *v );
			void			SetUniform4iv( const std::string &uniform, const int *v );

		private:
			/*
			-------------------------------------------------------------------------
				Initialization.
			-------------------------------------------------------------------------
			*/
			void			CreateProgram( const VertexShader &vertex, const FragmentShader &frag );
			void			CacheCustomUniforms( const std::string &uniforms );
			void			CacheInternalUniforms();

			/*
			-------------------------------------------------------------------------
				Private accessors.
			-------------------------------------------------------------------------
			*/
			GLint			GetUniformLocation( const std::string &uniform ) const;

			/*
			-------------------------------------------------------------------------
				Destruction.
			-------------------------------------------------------------------------
			*/
			void			DetachShaders();

			GLuint			_programName;
			std::string		_programTitle;

			typedef std::map<std::string, GLint> uniformMap_t;
			uniformMap_t	_uniformMap;

			GLint			_internalUniforms[INTERNAL_UNIFORM_COUNT];
		};

	} // namespace render;
} // namespace sandbox;

#endif // __SHADER_H__