/*
===============================================================================

	Texture.h
		A stateful OpenGL texture.

===============================================================================
*/

#ifndef __TEXTURE_H__
#define __TEXTURE_H__

#include "System/OpenGL.h"

namespace sandbox
{
	namespace render
	{

		/*
		===============================================================================
		Texture
			A stateful OpenGL texture wrapper.
		===============================================================================
		*/
		class Texture
		{
		public:
			enum pixelFormat_e
			{
				FORMAT_RGB,
				FORMAT_RGBA,

				FORMAT_COUNT
			};

			/*
			-------------------------------------------------------------------------
				Creation\destruction.
			-------------------------------------------------------------------------
			*/
							Texture();
							Texture( bool generateMipmaps, unsigned int width, unsigned int height, pixelFormat_e format, void *pPixels );
							~Texture();

			/*
			-------------------------------------------------------------------------
				Accessors.
			-------------------------------------------------------------------------
			*/
			GLuint			GetName() const			{ return _textureName; }
			unsigned int	GetWidth() const		{ return _width; }
			unsigned int	GetHeight() const		{ return _height; }
			bool			IsFiltered() const		{ return _filtered; }

			/*
			-------------------------------------------------------------------------
				Mutators.
			-------------------------------------------------------------------------
			*/
			void			SetFiltered( bool mipmapped );
			void			SetRepeatable( bool repeat );
			void			Reserve( unsigned int width, unsigned int height );
			void			UploadPixels( bool generateMipmaps, unsigned int width, unsigned int height, pixelFormat_e format, void *pPixels );

		private:
			GLenum			PixelFormatToGl( pixelFormat_e fmt );

			GLuint			_textureName;

			unsigned int	_width, _height;
			bool			_filtered, _repeated;
		};

	} // namespace render;
} // namespace sandbox;

#endif // __TEXTURE_H__