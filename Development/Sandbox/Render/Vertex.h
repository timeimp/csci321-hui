/*
===============================================================================

	Vertex.h
		Defines the vertex object.

===============================================================================
*/

#ifndef __VERTEX_H__
#define __VERTEX_H__

#include "Math/Point.h"

#include <stddef.h>

namespace sandbox
{
	namespace render
	{

		/*
		===============================================================================
		Vertex
			A description of a single point that will be rendered to the screen.
		===============================================================================
		*/
		class Vertex
		{
		public:
			/*
			-------------------------------------------------------------------------
				Mutators.
			-------------------------------------------------------------------------
			*/
			void			SetPoint( const math::Point3f &point );
			void			SetTexCoord( int idx, const math::Point2f &tc );
			void			SetColor( const math::Color3f &color );

			static inline int	GetPointOffset()		{ return offsetof( Vertex, _point ); }

			/*
			-------------------------------------------------------------------------
				Misc information.
			-------------------------------------------------------------------------
			*/
			enum vertexInfo_e
			{
				MAX_TEXCOORDS		= 4,
				VERTEX_COMPONENTS	= 3,
			};

		private:			
			math::Point3f	_point;
			math::Color3f	_color;
			math::Point2f	_texcoords[MAX_TEXCOORDS];
		};

	}
}

#endif // __VERTEX_H__