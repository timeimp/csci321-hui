/*
===============================================================================

	Plane.h
		Implements the world plane class.

===============================================================================
*/

#ifndef __PLANE_H__
#define __PLANE_H__

#include "Math/Vector.h"
#include "Math/Line.h"

namespace sandbox
{
	namespace math
	{

		/*
		===============================================================================
		Plane
			An infinite 2D surface in 3D space.
		===============================================================================
		*/
		class Plane
		{
		public:
							Plane();
							Plane( const Point3f &p, const Vector3f &normal );

			/*
			-------------------------------------------------------------------------
				Accessors.
			-------------------------------------------------------------------------
			*/
			const Point3f &	GetPoint() const		{ return _point; }
			const Vector3f &GetNormal() const		{ return _normal; }

			/*
			-------------------------------------------------------------------------
				Mutators.
			-------------------------------------------------------------------------
			*/
			void			Set( const Point3f &p, const Vector3f &normal );
			void			Rotate( const Angle3f &rotate );

			/*
			-------------------------------------------------------------------------
				Intersection tests.
			-------------------------------------------------------------------------
			*/
			bool			TestPoint( const Point3f &point ) const;
			bool			LineIntersection( const Line3f &line, Point3f *pOut ) const;
			bool			LineSegmentIntersection( const LineSegment3f &line, Point3f *pOut ) const;

		protected:
			Point3f			_point;
			Vector3f		_normal;
			float			_D;
		};

		/*
		===============================================================================
		PlaneSegment
			A plane with a finite range.
		===============================================================================
		*/
		class PlaneSegment : public Plane
		{
		public:
							PlaneSegment();
							PlaneSegment( const Point3f &center, const Vector3f &normal, float xMax, float zMax );

			/*
			-------------------------------------------------------------------------
				Accessors.
			-------------------------------------------------------------------------
			*/
			void			GetExtent( float *xMax, float *zMax ) const;

			/*
			-------------------------------------------------------------------------
				Mutators.
			-------------------------------------------------------------------------
			*/
			void			SetExtent( float xMax, float zMax );

			/*
			-------------------------------------------------------------------------
				Intersection tests.
			-------------------------------------------------------------------------
			*/
			bool			TestPoint( const Point3f &point ) const;
			bool			LineIntersection( const Line3f &line, Point3f *pOut ) const;
			bool			LineSegmentIntersection( const LineSegment3f &line, Point3f *pOut ) const;

		protected:
			float			_xMax, _zMax;
		};

	} // namespace math;
} // namespace sandbox;

#endif // __PLANE_H__