/*
===============================================================================

	Point.h
		Defines the world point classes.

===============================================================================
*/

#ifndef __VECTOR_H__
#define __VECTOR_H__

#include "Math/Point.h"

namespace sandbox
{
	namespace math
	{

		/*
		===============================================================================
		Vector3f
			A vector with 3 components.
		===============================================================================
		*/
		class Vector3f : public Point3f
		{
		public:
							Vector3f( const Pointf<3> &point );
							Vector3f( float x = 0, float y = 0, float z = 0 );

			/*
			-------------------------------------------------------------------------
				Accessors.
			-------------------------------------------------------------------------
			*/
			float			Dot( const Vector3f &other ) const;
			float			Magnitude() const;
			float			MagnitudeSquared() const	{ return ( _point[0] * _point[0] ) + ( _point[1] * _point[1] ) + ( _point[2] * _point[2] ); }
			void			Normalize();
			Vector3f		Normalized() const;
		};

	} // namespace math;
} // namespace sandbox;

#endif // __VECTOR_H__