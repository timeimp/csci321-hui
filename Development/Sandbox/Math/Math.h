/*
===============================================================================

	Math.h
		Misc math helpers.

===============================================================================
*/

#ifndef __MATH_H__
#define __MATH_H__

#include "Math/Vector.h"

#include <math.h>

namespace sandbox
{
	namespace math
	{
		#define gPI		( 3.1415926535897932 )

		/*
		===============================================================================
		d2r
			Converts an angle in degrees to an angle in radians.
		===============================================================================
		*/
		inline float d2r( float d )
		{
			return d * db2fl( gPI ) / 180.0f;
		}

		/*
		===============================================================================
		r2d
			Converts an angle in radians to an angle in degrees.
		===============================================================================
		*/
		inline float r2d( float r )
		{
			return r * 180.0f / db2fl( gPI );
		}

		/*
		===============================================================================
		isqrt
			Finds the inverse square root of the given number.
		===============================================================================
		*/
		inline float isqrt( float r )
		{
			return 1.0f / sqrt( r );
		}

		/*
		===============================================================================
		isqrt
			Finds the inverse square root of the given number.
		===============================================================================
		*/
		inline Angle3f vector_to_angle( const Vector3f &vector )
		{
			Vector3f norm = vector.Normalized();
			return Angle3f( asin( norm.GetZ() ), asin( norm.GetX() ), asin( norm.GetY() ) );
		}

		/*
		===============================================================================
		npow2
			Finds the next highest power of two for the given number.
		===============================================================================
		*/
		template<class T>
		inline T npow2( T num )
		{
			T n = 1;
			while( n <= num )
			{
				n *= 2;
			}
			return n;
		}

	} // namespace math;
} // namespace sandbox;

#endif // __MATH_H__