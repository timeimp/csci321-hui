/*
===============================================================================

	Line.h
		Line container classes.

===============================================================================
*/

#ifndef __LINE_H__
#define __LINE_H__

#include "Math/Vector.h"

namespace sandbox
{
	namespace math
	{

		/*
		===============================================================================
		Line3f
			A never ending line.
		===============================================================================
		*/
		class Line3f
		{
		public:
							Line3f( const Point3f &p, const Vector3f &dir );

			/*
			-------------------------------------------------------------------------
				Accessors.
			-------------------------------------------------------------------------
			*/
			const Point3f &	GetOrigin()	const		{ return _point; }
			const Vector3f &GetDirection() const	{ return _direction; }
			Point3f			At( float scale ) const	{ return _point + ( _direction * scale ); }

		private:
			Point3f			_point;
			Vector3f		_direction;
		};

		/*
		===============================================================================
		LineSegment3f
			An infinite 2D surface in 3D space.
		===============================================================================
		*/
		class LineSegment3f
		{
		public:
							LineSegment3f( const Point3f &p0, const Point3f &p1 );
							LineSegment3f( const Point3f &p, const Vector3f &dir );

			/*
			-------------------------------------------------------------------------
				Accessors.
			-------------------------------------------------------------------------
			*/
			float			Length() const			{ return _direction.Magnitude(); }

			const Point3f &	GetStart() const		{ return _point; }
			const Point3f 	GetEnd() const			{ return _point + _direction; }

			const Vector3f &GetDirection() const	{ return _direction; }

			Point3f			At( float scale ) const;

		private:
			Point3f			_point;
			Vector3f		_direction;
		};

	} // namespace math;
} // namespace sandbox;

#endif // __PLANE_H__