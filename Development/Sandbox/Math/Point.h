/*
===============================================================================

	Point.h
		Defines the world point classes.

===============================================================================
*/

#ifndef __POINT_H__
#define __POINT_H__

namespace sandbox
{
	namespace math
	{
		/*
		===============================================================================
		Point
			Base point class.
		===============================================================================
		*/
		template<int N>
		class Pointf
		{
			friend class Point2f;
			friend class Point3f;
			friend class Point4f;
			friend class Vector3f;

			typedef Pointf<N> PointT;
			
		public:
							Pointf();
							Pointf( float points[N] );

			/*
			-------------------------------------------------------------------------
				Accessors.
			-------------------------------------------------------------------------
			*/
			const float *	Base() const;
			float			DistanceBetween( const Pointf<N> &other );

			/*
			-------------------------------------------------------------------------
				Math operators overloads.
			-------------------------------------------------------------------------
			*/
			PointT			operator-() const;

			PointT			operator+( float scale ) const;
			PointT			operator-( float scale ) const;
			PointT			operator*( float scale ) const;
			PointT			operator/( float scale ) const;

			PointT			operator+( const PointT &other ) const;
			PointT			operator-( const PointT &other ) const;
			PointT			operator*( const PointT &other ) const;
			PointT			operator/( const PointT &other ) const;

			PointT &		operator+=( const PointT &other );
			PointT &		operator-=( const PointT &other );
			PointT &		operator*=( const PointT &other );
			PointT &		operator/=( const PointT &other );


		protected:
			float			_point[N];
		};

		/*
		===============================================================================
		Point2f
			Point with 2 components.
		===============================================================================
		*/
		class Point2f : public Pointf<2>
		{
			friend class Point3f;
			friend class Point4f;

		public:
							Point2f( const Pointf<2> &point );
							Point2f( float x = 0, float y = 0 );

			/*
			-------------------------------------------------------------------------
				Mutators.
			-------------------------------------------------------------------------
			*/
			void			Set( float x, float y );

			/*
			-------------------------------------------------------------------------
				Accessors.
			-------------------------------------------------------------------------
			*/
			inline float	GetX() const			{ return _point[0]; }
			inline float	GetY() const			{ return _point[1]; }
		};

		/*
		===============================================================================
		Point3f
			Point with 3 components.
		===============================================================================
		*/
		class Point3f : public Pointf<3>
		{
			friend class Point4f;

		public:
							Point3f( const Pointf<3> &point );
							Point3f( float x = 0, float y = 0, float z = 0 );
							Point3f( const Point2f &point, float z = 0 );

			/*
			-------------------------------------------------------------------------
				Mutators.
			-------------------------------------------------------------------------
			*/
			void			Set( float x, float y, float z );

			/*
			-------------------------------------------------------------------------
				Accessors.
			-------------------------------------------------------------------------
			*/
			inline float	GetX() const			{ return _point[0]; }
			inline float	GetY() const			{ return _point[1]; }
			inline float	GetZ() const			{ return _point[2]; }
		};

		/*
		===============================================================================
		Point4f
			Point with 4 components.
		===============================================================================
		*/
		class Point4f : public Pointf<4>
		{
		public:
							Point4f( const Pointf<4> &point );
							Point4f( float x = 0, float y = 0, float z = 0, float w = 1.0f );
							Point4f( const Point2f &point, float z = 0, float w = 1.0f );
							Point4f( const Point3f &point, float w = 1.0f );

			/*
			-------------------------------------------------------------------------
				Mutators.
			-------------------------------------------------------------------------
			*/
			void			Set( float x, float y, float z, float w = 1.0f );

			/*
			-------------------------------------------------------------------------
				Accessors.
			-------------------------------------------------------------------------
			*/
			inline float	GetX() const			{ return _point[0]; }
			inline float	GetY() const			{ return _point[1]; }
			inline float	GetZ() const			{ return _point[2]; }
			inline float	GetW() const			{ return _point[3]; }
		};

		typedef Point3f				Angle3f;
		typedef Point3f				Color3f;
		typedef Point4f				Color4f;

	} // namespace math;
} // namespace sandbox;

#endif // __POINT_H__