/*
===============================================================================

	Matrix.h
		Defines the world matrix classes.

===============================================================================
*/

#ifndef __MATRIX_H__
#define __MATRIX_H__

#include "Point.h"

namespace sandbox
{
	namespace math
	{

		/*
		===============================================================================
		Matrix4f
			4x4 matrix.
			Default constructor initializes the Matrix4 instance to the identity matrix.
		===============================================================================
		*/
		class Matrix4f
		{
		public:
							Matrix4f();
							Matrix4f(	float m11, float m12, float m13, float m14,
										float m21, float m22, float m23, float m24,
										float m31, float m32, float m33, float m34,
										float m41, float m42, float m43, float m44 );

			/*
			-------------------------------------------------------------------------
				Utilities.
			-------------------------------------------------------------------------
			*/
			static class Matrix4f BuildRotation( const Angle3f &angles );
			static class Matrix4f BuildTranslation3f( const Point3f &translation );
			static class Matrix4f BuildTranslation4f( const Point3f &translation );
			static class Matrix4f BuildPerspective( float fov, float aspect, float zNear, float zFar );
			static class Matrix4f BuildOrtho( float left, float right, float bottom, float top, float zNear = -1, float zFar = 1 );

			/*
			-------------------------------------------------------------------------
				Accessor.
			-------------------------------------------------------------------------
			*/
			const float *		Get() const;

			/*
			-------------------------------------------------------------------------
				Mutators.
			-------------------------------------------------------------------------
			*/
			void			MakeIdentity();
			void			Transpose();

			/*
			-------------------------------------------------------------------------
				Transformations.
			-------------------------------------------------------------------------
			*/
			void			Translate3f( float x, float y, float z );
			void			TranslatePoint3f( const Point3f &point );
			void			Rotate3f( float angle, float x, float y, float z );
			void			RotateAngle3f( const Angle3f &angles );
			void			Scale( float x, float y, float z );

			/*
			-------------------------------------------------------------------------
				Math operator overloads.
			-------------------------------------------------------------------------
			*/
			Point3f			operator*( const Point3f &point ) const;

			Matrix4f		operator*( float scale ) const;
			Matrix4f		operator/( float scale ) const;

			Matrix4f		operator+( const Matrix4f &rhs ) const;
			Matrix4f		operator-( const Matrix4f &rhs ) const;
			Matrix4f		operator*( const Matrix4f &rhs ) const;
			Matrix4f		operator/( const Matrix4f &rhs ) const;

		protected:
			float			_m[4][4];

		private:
			static void		Multiply( const Matrix4f &lhs, const Matrix4f &rhs, Matrix4f *pOut );
		};

	} // namespace math;
} // namespace sandbox;

#endif // __MATRIX_H__