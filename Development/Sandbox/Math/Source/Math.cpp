/*
===============================================================================

	Math.cpp
		Misc math helpers.

===============================================================================
*/

#include "Framework/Sandbox.h"
#include "Math/Math.h"

using namespace sandbox::math;