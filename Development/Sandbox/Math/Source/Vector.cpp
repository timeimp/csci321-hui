/*
===============================================================================

	Vector.cpp
		Implements the vector classes.

===============================================================================
*/

#include "Framework/Sandbox.h"
#include "Math/Vector.h"
#include "Math/Math.h"

using namespace sandbox::math;

/*
===============================================================================
	Vector3f implementation.
===============================================================================
*/

/*
===============================================================================
Vector3f::Vector3f
	Initializes a vector instance.
===============================================================================
*/
Vector3f::Vector3f( const Pointf<3> &point )
{
	Set( point._point[0], point._point[1], point._point[2] );
}

/*
===============================================================================
Vector3f::Vector3f
	Initializes a vector instance.
===============================================================================
*/
Vector3f::Vector3f( float x, float y, float z )
{
	Set( x, y, z );
}

/*
===============================================================================
Vector3f::Dot
	.
===============================================================================
*/
float Vector3f::Dot( const Vector3f &other ) const
{
	return ( _point[0] * other._point[0] ) + ( _point[1] * other._point[1] ) + ( _point[2] * other._point[2] );
}

/*
===============================================================================
Vector3f::Magnitude
	Determines the length of the vector.
===============================================================================
*/
float Vector3f::Magnitude() const
{
	return sqrt( MagnitudeSquared() );
}

/*
===============================================================================
Vector3f::Normalized
	Scales the components of this vector to make its length equal to 1.
===============================================================================
*/
void Vector3f::Normalize()
{
	*this = Normalized();
}

/*
===============================================================================
Vector3f::Normalized
	Scales the components of this vector to make its length equal to 1.
===============================================================================
*/
Vector3f Vector3f::Normalized() const
{
	Vector3f normalized;
	const float lengthSqr = MagnitudeSquared();
	
	if( lengthSqr == 0.0f )
	{
		normalized.Set( 0.0f, 0.0f, 0.0f );
	}
	else
	{
		const float scale = isqrt( lengthSqr );
		normalized.Set( _point[0] * scale, _point[1] * scale, _point[2] * scale );
	}

	return normalized;
}