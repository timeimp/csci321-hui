/*
===============================================================================

	Line.cpp
		Line container classes.

===============================================================================
*/

#include "Framework/Sandbox.h"
#include "Math/Line.h"

using namespace sandbox::math;

/*
===============================================================================
	LineSegment implementation.
===============================================================================
*/

/*
===============================================================================
Line3f::Line3f
	Constructor which takes a start and end position.
===============================================================================
*/
Line3f::Line3f( const Point3f &p, const Vector3f &dir )
{
	_point = p;
	_direction = dir;
}

/*
===============================================================================
	LineSegment implementation.
===============================================================================
*/

/*
===============================================================================
LineSegment3f::LineSegment3f
	Constructor which takes a start and end position.
===============================================================================
*/
LineSegment3f::LineSegment3f( const Point3f &p0, const Point3f &p1 )
{
	_point = p0;
	_direction = p1 - p0;
}

/*
===============================================================================
LineSegment3f::LineSegment3f
	Constructor which takes a start and end position.
===============================================================================
*/
LineSegment3f::LineSegment3f( const Point3f &p, const Vector3f &dir )
{
	_point = p;
	_direction = dir;
}

/*
===============================================================================
LineSegment3f::At
	Constructor which takes a start and end position.
===============================================================================
*/
Point3f LineSegment3f::At( float scale ) const
{
	scale = sbCLAMP( scale, 0.0f, 1.0f );
	return _point + ( _direction * scale );
}