/*
===============================================================================

	Point.cpp
		Implements the world point classes.

===============================================================================
*/

#include "Framework/Sandbox.h"
#include "Math/Point.h"
#include "Math/Math.h"

using namespace sandbox::math;

/*
===============================================================================
	Point implementation.
===============================================================================
*/

/*
===============================================================================
Point::Point
	Initializes a point instance.
===============================================================================
*/
template<int N>
Pointf<N>::Pointf()
{
	for( int i = 0 ; i < N ; i++ )
	{
		_point[i] = 0;
	}
}

/*
===============================================================================
Point::Point
	Initializes a point instance.
===============================================================================
*/
template<int N>
Pointf<N>::Pointf( float points[N] )
{
	for( int i = 0 ; i < N ; i++ )
	{
		_point[i] = points[i];
	}
}

/*
===============================================================================
Point::Base
	Provides access to the raw point array.
===============================================================================
*/
template<int N>
const float *Pointf<N>::Base() const
{
	return _point;
}

/*
===============================================================================
Point::DistanceBetween
	Finds the distance between two points ( (*this - other).Length() ).
===============================================================================
*/
template<int N>
float Pointf<N>::DistanceBetween( const Pointf<N> &other )
{
	const float dX = _point[0] - other._point[0];
	const float dY = _point[1] - other._point[1];
	const float dZ = _point[2] - other._point[2];

	return isqrt( ( dX * dX ) + ( dY * dY ) + ( dZ * dZ ) );
}

/*
===============================================================================
Point::operator-
	Provides access to the raw point array.
===============================================================================
*/
template<int N>
Pointf<N> Pointf<N>::operator-() const
{
	PointT temp;

	for( int i = 0 ; i < N ; i++ )
	{
		temp._point[i] = -_point[i];
	}

	return temp;
}

/*
===============================================================================
Point::operator+
	Provides access to the raw point array.
===============================================================================
*/
template<int N>
Pointf<N> Pointf<N>::operator+( float scale ) const
{
	PointT temp;

	for( int i = 0 ; i < N ; i++ )
	{
		temp._point[i] = _point[i] + scale;
	}

	return temp;
}

/*
===============================================================================
Point::operator-
	Provides access to the raw point array.
===============================================================================
*/
template<int N>
Pointf<N> Pointf<N>::operator-( float scale ) const
{
	PointT temp;

	for( int i = 0 ; i < N ; i++ )
	{
		temp._point[i] = _point[i] - scale;
	}

	return temp;
}

/*
===============================================================================
Point::operator*
	Provides access to the raw point array.
===============================================================================
*/
template<int N>
Pointf<N> Pointf<N>::operator*( float scale ) const
{
	PointT temp;

	for( int i = 0 ; i < N ; i++ )
	{
		temp._point[i] = _point[i] * scale;
	}

	return temp;
}

/*
===============================================================================
Point::operator/
	Provides access to the raw point array.
===============================================================================
*/
template<int N>
Pointf<N> Pointf<N>::operator/( float scale ) const
{
	PointT temp;

	for( int i = 0 ; i < N ; i++ )
	{
		temp._point[i] = _point[i] / scale;
	}

	return temp;
}

/*
===============================================================================
Point::operator+
	Provides access to the raw point array.
===============================================================================
*/
template<int N>
Pointf<N> Pointf<N>::operator+( const PointT &other ) const
{
	PointT temp;

	for( int i = 0 ; i < N ; i++ )
	{
		temp._point[i] = this->_point[i] + other._point[i];
	}

	return temp;
}

/*
===============================================================================
Point::operator-
	Provides access to the raw point array.
===============================================================================
*/
template<int N>
Pointf<N> Pointf<N>::operator-( const PointT &other ) const
{
	PointT temp;

	for( int i = 0 ; i < N ; i++ )
	{
		temp._point[i] = this->_point[i] - other._point[i];
	}

	return temp;
}

/*
===============================================================================
Point::operator*
	Provides access to the raw point array.
===============================================================================
*/
template<int N>
Pointf<N> Pointf<N>::operator*( const PointT &other ) const
{
	PointT temp;

	for( int i = 0 ; i < N ; i++ )
	{
		temp._point[i] = this->_point[i] * other._point[i];
	}

	return temp;
}

/*
===============================================================================
Point::operator/
	Provides access to the raw point array.
===============================================================================
*/
template<int N>
Pointf<N> Pointf<N>::operator/( const PointT &other ) const
{
	PointT temp;

	for( int i = 0 ; i < N ; i++ )
	{
		temp._point[i] = this->_point[i] / other._point[i];
	}

	return temp;
}

/*
===============================================================================
Point::operator+=
	Provides access to the raw point array.
===============================================================================
*/
template<int N>
Pointf<N> &Pointf<N>::operator+=( const PointT &other )
{
	for( int i = 0 ; i < N ; i++ )
	{
		this->_point[i] += other._point[i];
	}

	return *this;
}

/*
===============================================================================
Point::operator-=
	Provides access to the raw point array.
===============================================================================
*/
template<int N>
Pointf<N> &Pointf<N>::operator-=( const PointT &other )
{
	for( int i = 0 ; i < N ; i++ )
	{
		this->_point[i] -= other._point[i];
	}

	return *this;
}

/*
===============================================================================
Point::operator*=
	Provides access to the raw point array.
===============================================================================
*/
template<int N>
Pointf<N> &Pointf<N>::operator*=( const PointT &other )
{
	for( int i = 0 ; i < N ; i++ )
	{
		this->_point[i] *= other._point[i];
	}

	return *this;
}

/*
===============================================================================
Point::operator/=
	Provides access to the raw point array.
===============================================================================
*/
template<int N>
Pointf<N> &Pointf<N>::operator/=( const PointT &other )
{
	for( int i = 0 ; i < N ; i++ )
	{
		this->_point[i] /= other._point[i];
	}

	return *this;
}

/*
===============================================================================
	Template instanciation.
===============================================================================
*/
template class Pointf<2>;
template class Pointf<3>;
template class Pointf<4>;

/*
===============================================================================
	Point2 implementation.
===============================================================================
*/

/*
===============================================================================
Point2f::Point2f
	Updates a point value.
===============================================================================
*/
Point2f::Point2f( const Pointf<2> &point )
{
	Set( point._point[0], point._point[1] );
}

/*
===============================================================================
Point2::Point2
	Updates a point value.
===============================================================================
*/
Point2f::Point2f( float x, float y )
{
	Set( x, y );
}

/*
===============================================================================
Point2::Set
	Updates a point value.
===============================================================================
*/
void Point2f::Set( float x, float y )
{
	_point[0] = x;
	_point[1] = y;
}

/*
===============================================================================
	Point3 implementation.
===============================================================================
*/

/*
===============================================================================
Point3::Point3
	Updates a point value.
===============================================================================
*/
Point3f::Point3f( const Pointf<3> &point )
{
	Set( point._point[0], point._point[1], point._point[2] );
}

/*
===============================================================================
Point3::Point3
	Updates a point value.
===============================================================================
*/
Point3f::Point3f( float x, float y, float z )
{
	Set( x, y, z );
}

/*
===============================================================================
Point3::Point3
	Updates a point value.
===============================================================================
*/
Point3f::Point3f( const Point2f &point, float z /*= 0*/ )
{
	Set( point._point[0], point._point[1], z );
}

/*
===============================================================================
Point3::Set
	Updates a point value.
===============================================================================
*/
void Point3f::Set( float x, float y, float z )
{
	_point[0] = x;
	_point[1] = y;
	_point[2] = z;
}

/*
===============================================================================
	Point4 implementation.
===============================================================================
*/

/*
===============================================================================
Point4f::Point4f
	Updates a point value.
===============================================================================
*/
Point4f::Point4f( const Pointf<4> &point )
{
	Set( point._point[0], point._point[1], point._point[2], point._point[3] );
}

/*
===============================================================================
Point4::Point4
	Updates a point value.
===============================================================================
*/
Point4f::Point4f( float x, float y, float z, float w )
{
	Set( x, y, z, w );
}

/*
===============================================================================
Point4::Point4
	Updates a point value.
===============================================================================
*/
Point4f::Point4f( const Point2f &point, float z /*= 0*/, float w /*= 0*/ )
{
	Set( point._point[0], point._point[1], z, w );
}

/*
===============================================================================
Point4::Point4
	Updates a point value.
===============================================================================
*/
Point4f::Point4f( const Point3f &point, float w /*= 0*/ )
{
	Set( point._point[0], point._point[1], point._point[2], w );
}

/*
===============================================================================
Point4::Set
	Updates a point value.
===============================================================================
*/
void Point4f::Set( float x, float y, float z, float w )
{
	_point[0] = x;
	_point[1] = y;
	_point[2] = z;
	_point[3] = w;
}