/*
===============================================================================

	Matrix.cpp
		Implements the matrix classes.

===============================================================================
*/

#include "Framework/Sandbox.h"
#include "Math/Matrix.h"

#include <math.h>
#include <string.h>

using namespace sandbox::math;

/*
===============================================================================
	Matrix4 implementation.
===============================================================================
*/

/*
===============================================================================
Matrix4::Matrix4
	Default constructor.
===============================================================================
*/
Matrix4f::Matrix4f()
{
	MakeIdentity();
}

/*
===============================================================================
Matrix4::Matrix4
	Default constructor.
===============================================================================
*/
Matrix4f::Matrix4f(	float m11, float m12, float m13, float m14,
					float m21, float m22, float m23, float m24,
					float m31, float m32, float m33, float m34,
					float m41, float m42, float m43, float m44 )
{
	_m[0][0] = m11;	_m[1][0] = m12;	_m[2][0] = m13;	_m[3][0] = m14;
	_m[0][1] = m21;	_m[1][1] = m22;	_m[2][1] = m23;	_m[3][1] = m24;
	_m[0][2] = m31;	_m[1][2] = m32;	_m[2][2] = m33;	_m[3][2] = m34;
	_m[0][3] = m41;	_m[1][3] = m42;	_m[2][3] = m43;	_m[3][3] = m44;
}

/*
===============================================================================
Matrix4::BuildRotation
	Creates a new rotation matrix.
===============================================================================
*/
Matrix4f Matrix4f::BuildRotation( const Angle3f &angles )
{
	Matrix4f m;
	m.RotateAngle3f( angles );
	return m;
}

/*
===============================================================================
Matrix4::BuildTranslation3f
	Creates a new translation matrix.
===============================================================================
*/
Matrix4f Matrix4f::BuildTranslation3f( const Point3f &translation )
{
	Matrix4f m;
	m.TranslatePoint3f( translation );
	return m;
}

/*
===============================================================================
Matrix4::BuildPerspective
	Creates a new perspective matrix for use with OpenGL.
===============================================================================
*/
Matrix4f Matrix4f::BuildPerspective( float fov, float aspect, float zNear, float zFar )
{
	// Precompute common factors.
	float f = tan( fov / 2.0f ) * zNear;

	// Create a zero matrix.
	Matrix4f temp;
	memset( temp._m, 0, sizeof(temp._m) );

	// Build the matrix.
	temp._m[0][0] = ( 2 * zNear ) / ( ( f * aspect ) - ( -f * aspect ) );
	temp._m[1][1] = ( 2 * zNear ) / ( 2 * f );
	temp._m[2][2] = -( zFar + zNear ) / ( zFar - zNear );
	temp._m[2][3] = -1;
	temp._m[3][2] = -( 2 * zFar * zNear ) / ( zFar - zNear );

	return temp;
}

/*
===============================================================================
Matrix4::BuildOrtho
	Creates a new orthographic projection matrix for use with OpenGL.
===============================================================================
*/
Matrix4f Matrix4f::BuildOrtho( float left, float right, float bottom, float top, float zNear, float zFar  )
{
	Matrix4f temp;

	// Clear the old matrix.
	memset( temp._m, 0, sizeof(temp._m) );

	temp._m[0][0] = 2.0f / ( right - left );
	temp._m[1][1] = 2.0f / ( top - bottom );
	temp._m[2][2] = 2.0f / ( zFar - zNear );
	
	// Generate translations.
	temp._m[3][0] =	-( ( right + left ) / ( right - left ) );
	temp._m[3][1] = -( ( top + bottom ) / ( top - bottom ) );
	temp._m[3][2] = -( ( zFar + zNear ) / ( zFar - zNear ) );

	temp._m[3][3] = 1.0f;

	return temp;
}

/*
===============================================================================
Matrix4::Get
	Provides access to the raw matrix.
===============================================================================
*/
const float *Matrix4f::Get() const
{
	return (float *)_m;
}

/*
===============================================================================
Matrix4::MakeIdentity
	Sets this matrix to the identity matrix.
===============================================================================
*/
void Matrix4f::MakeIdentity()
{
	_m[0][0] = 1;	_m[1][0] = 0;	_m[2][0] = 0;	_m[3][0] = 0;
	_m[0][1] = 0;	_m[1][1] = 1;	_m[2][1] = 0;	_m[3][1] = 0;
	_m[0][2] = 0;	_m[1][2] = 0;	_m[2][2] = 1;	_m[3][2] = 0;
	_m[0][3] = 0;	_m[1][3] = 0;	_m[2][3] = 0;	_m[3][3] = 1;
}

/*
===============================================================================
Matrix4::Transpose
	Swaps rows and columns of the matrix.
===============================================================================
*/
void Matrix4f::Transpose()
{
	Matrix4f transposed = Matrix4f(		_m[0][0], _m[0][1], _m[0][2], _m[0][3],
										_m[1][0], _m[1][1], _m[1][2], _m[1][3],
										_m[2][0], _m[2][1], _m[2][2], _m[2][3],
										_m[3][0], _m[3][1], _m[3][2], _m[3][3] );

	*this = transposed;
}

/*
===============================================================================
Matrix4::Translate
	Multiplies the matrix by the given translation matrix.
===============================================================================
*/
void Matrix4f::Translate3f( float x, float y, float z )
{
	// Build the translation matrix.
	Matrix4f matTranslate(	1, 0, 0, 0,
								0, 1, 0, 0, 
								0, 0, 1, 0,
								x, y, z, 1 );

	Multiply( *this, matTranslate, this );
}

/*
===============================================================================
Matrix4::Translate
	Multiplies the matrix by the given translation matrix.
===============================================================================
*/
void Matrix4f::TranslatePoint3f( const Point3f &point )
{
	Translate3f( point.GetX(), point.GetY(), point.GetZ() );
}

/*
===============================================================================
Matrix4::Rotate
	Multiplies the matrix by the given rotation matrix.
===============================================================================
*/
void Matrix4f::Rotate3f( float angle, float x, float y, float z )
{
	// Precompute commonly used factors.
	const float xSqr = x * x;
	const float ySqr = y * y;
	const float zSqr = z * z;

	const float xy = x * y;
	const float xz = x * z;
	const float yz = y * z;
	
	const float c = cos( angle );
	register const float oneMinusC = 1 - c;

	const float s = sin( angle );
	const float xs = x * s;
	const float ys = y * s;
	const float zs = z * s;

	// Build the rotation matrix.
	Matrix4f matRotate;
	matRotate._m[0][0] = xSqr * oneMinusC + c;
	matRotate._m[0][1] = xy * oneMinusC + zs;
	matRotate._m[0][2] = xz * oneMinusC - ys;

	matRotate._m[1][0] = xy * oneMinusC - zs;
	matRotate._m[1][1] = ySqr * oneMinusC + c;
	matRotate._m[1][2] = yz * oneMinusC + xs;

	matRotate._m[2][0] = xz * oneMinusC + ys;
	matRotate._m[2][1] = yz * oneMinusC - xs;
	matRotate._m[2][2] = zSqr * oneMinusC + c;

	// Apply the rotation.
	Multiply( *this, matRotate, this );
}

/*
===============================================================================
Matrix4::Rotate
	Multiplies the matrix by the given rotation matrix.
===============================================================================
*/
void Matrix4f::RotateAngle3f( const Angle3f &point )
{
	Rotate3f( point.GetX(), 1.0f, 0.0f, 0.0f );
	Rotate3f( point.GetY(), 0.0f, 1.0f, 0.0f );
	Rotate3f( point.GetZ(), 0.0f, 0.0f, 1.0f );
}

/*
===============================================================================
Matrix4::Scale
	Multiplies the matrix by the given scale matrix.
===============================================================================
*/
void Matrix4f::Scale( float x, float y, float z )
{
	// Build the scale matrix.
	Matrix4f matScale(	x, 0, 0, 0,
							0, y, 0, 0,
							0, 0, z, 0,
							0, 0, 0, 1 );

	Multiply( *this, matScale, this );
}

/*
===============================================================================
Matrix4::operator*
	Multiplies the given vector by this matrix.
===============================================================================
*/
Point3f Matrix4f::operator*( const Point3f &point ) const
{	
	const float x = point.GetX();
	const float y = point.GetY();
	const float z = point.GetZ();

	float result[4];

	for( int i = 0 ; i < 3 ; i++ )
	{
		result[i] = ( _m[i][0] * x ) + ( _m[i][1] * y ) + ( _m[i][2] * z ) + ( _m[i][3] * 1.0f );
	}

	return Point3f( result[0], result[1], result[2] );
}

/*
===============================================================================
Matrix4::operator*
	Scales this matrix by a value.
===============================================================================
*/
Matrix4f Matrix4f::operator*( float scale ) const
{
	Matrix4f temp;

	for( int x = 0 ; x < 4 ; x++ )
	{
		for( int y = 0 ; y < 4 ; y++ )
		{
			temp._m[x][y] = _m[x][y] * scale;
		}
	}

	return temp;
}

/*
===============================================================================
Matrix4::operator/
	Divides this matrix by a value.
===============================================================================
*/
Matrix4f Matrix4f::operator/( float scale ) const
{
	Matrix4f temp;

	for( int x = 0 ; x < 4 ; x++ )
	{
		for( int y = 0 ; y < 4 ; y++ )
		{
			temp._m[x][y] = _m[x][y] / scale;
		}
	}

	return temp;
}

/*
===============================================================================
Matrix4::operator+
	Returns the addition of 2 matrices.
===============================================================================
*/
Matrix4f Matrix4f::operator+( const Matrix4f &rhs ) const
{
	Matrix4f temp;

	for( int x = 0 ; x < 4 ; x++ )
	{
		for( int y = 0 ; y < 4 ; y++ )
		{
			temp._m[x][y] = _m[x][y] + rhs._m[x][y];
		}
	}

	return temp;
}

/*
===============================================================================
Matrix4::operator-
	Returns the subtraction of 2 matrices.
===============================================================================
*/
Matrix4f Matrix4f::operator-( const Matrix4f &rhs ) const
{
	Matrix4f temp;

	for( int x = 0 ; x < 4 ; x++ )
	{
		for( int y = 0 ; y < 4 ; y++ )
		{
			temp._m[x][y] = _m[x][y] - rhs._m[x][y];
		}
	}

	return temp;
}


/*
===============================================================================
Matrix4::operator*
	Returns the product of 2 matrices.
===============================================================================
*/
Matrix4f Matrix4f::operator*( const Matrix4f &rhs ) const
{
	Matrix4f temp;
	Multiply( *this, rhs, &temp );
	return temp;
}

/*
===============================================================================
Matrix4::operator/
	Returns the division of 2 matrices.
===============================================================================
*/
Matrix4f Matrix4f::operator/( const Matrix4f &rhs ) const
{
	// TODO: *this * rhs.Inverse();
	return Matrix4f();
}

/*
===============================================================================
Matrix4::Multiply
	Multiplies the given matrices and stores the result in `pOut`.
===============================================================================
*/
void Matrix4f::Multiply( const Matrix4f &lhs, const Matrix4f &rhs, Matrix4f *pOut )
{
	Matrix4f temp;

	for( int x = 0 ; x < 4 ; x++ )
	{
		for( int y = 0; y < 4; y++ )
		{
			temp._m[x][y] =	( lhs._m[x][0] * rhs._m[0][y] ) +
							( lhs._m[x][1] * rhs._m[1][y] ) +
							( lhs._m[x][2] * rhs._m[2][y] ) +
							( lhs._m[x][3] * rhs._m[3][y] );
		}
	}

	*pOut = temp;
}