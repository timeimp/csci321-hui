/*
===============================================================================

	Plane.cpp
		Implements the world plane class.

===============================================================================
*/

#include "Framework/Sandbox.h"
#include "Math/Plane.h"
#include "Math/Matrix.h"
#include "Math/Math.h"

#include <iostream>
#include <math.h>

using namespace std;
using namespace sandbox::math;

/*
===============================================================================
	Plane implementation.
===============================================================================
*/

/*
===============================================================================
Plane::Plane
	Default constructor.
===============================================================================
*/
Plane::Plane()
{
	_point.Set( 0.0f, 0.0f, 0.0f );
	_normal.Set( 0.0f, 0.0f, 0.0f );
	_D = 0.0f;
}

/*
===============================================================================
Plane::Plane
	
===============================================================================
*/
Plane::Plane( const Point3f &p, const Vector3f &normal )
{
	Set( p, normal );
}

/*
===============================================================================
Plane::Set
	Sets the plane state.
===============================================================================
*/
void Plane::Set( const Point3f &p, const Vector3f &normal )
{
	_point = p;
	_normal = normal;

	_D = -_normal.Dot( p );
}

/*
===============================================================================
Plane::Rotate
	Rotates the plane to a new orientation.
===============================================================================
*/
void Plane::Rotate( const Angle3f &rotate )
{
	Matrix4f rotationMatrix = Matrix4f::BuildRotation( rotate );
	_normal = rotationMatrix * _normal;
}

/*
===============================================================================
Plane::TestPoint
	Determines whether a point lies on the plane.
===============================================================================
*/
bool Plane::TestPoint( const Point3f &point ) const
{
	Vector3f delta = _point - point;
	return delta.Dot( _normal ) == 0.0f;
}

/*
===============================================================================
Plane::LineIntersection
	Determines where on a plane an intersection with a line occurs.
===============================================================================
*/
bool Plane::LineIntersection( const Line3f &line, Point3f *pOut ) const
{
	const float normDotLinePoint = _normal.Dot( line.GetOrigin() );
	const float normDotLineDir = _normal.Dot( line.GetDirection() );

	// Get the point of intersection as a scale of the line direction.
	const float scale = -( normDotLinePoint + _D ) / normDotLineDir;

	// Find the point on the line given the direction scale.
	*pOut = line.At( scale );
	return true;
}

/*
===============================================================================
Plane::LineSegmentIntersection
	Determines where on a plane an intersection with a line occurs.
===============================================================================
*/
bool Plane::LineSegmentIntersection( const LineSegment3f &line, Point3f *pOut ) const
{
	const float normDotLinePoint = _normal.Dot( line.GetStart() );
	const float normDotLineDir = _normal.Dot( line.GetDirection() );

	// Get the point of intersection as a scale of the line direction.
	const float scale = -( normDotLinePoint + _D ) / normDotLineDir;

	// Ensure the direction scale is a fraction of the line length.
	if( scale < 0.0f || scale > 1.0f )
	{
		return false;
	}

	// Find the point on the line given the direction scale.
	*pOut = line.At( scale );
	return true;
}

/*
===============================================================================
	PlaneSegment implementation.
===============================================================================
*/

/*
===============================================================================
PlaneSegment::PlaneSegment
	Default constructor.
===============================================================================
*/
PlaneSegment::PlaneSegment()
{
	_point.Set( 0.0f, 0.0f, 0.0f );
	_normal.Set( 0.0f, 0.0f, 0.0f );
	_D = 0.0f;

	_xMax = _zMax = 0.0f;
}

/*
===============================================================================
PlaneSegment::PlaneSegment
	
===============================================================================
*/
PlaneSegment::PlaneSegment( const Point3f &center, const Vector3f &normal, float xMax, float zMax )
{
	Set( center, normal );
	SetExtent( xMax, zMax );
}

/*
===============================================================================
PlaneSegment::GetExtent
	Provides access to the plane size.
===============================================================================
*/
void PlaneSegment::GetExtent( float *xMax, float *zMax ) const
{
	if( xMax )
	{
		*xMax = _xMax;
	}

	if( zMax )
	{
		*zMax = _zMax;
	}
}

/*
===============================================================================
PlaneSegment::SetExtent
	Sets the maximum X and Y values the plane can extend.
===============================================================================
*/
void PlaneSegment::SetExtent( float xMax, float zMax )
{
	_xMax = xMax;
	_zMax = zMax;
}

/*
===============================================================================
PlaneSegment::TestPoint
	Determines whether a point lies on the plane.
===============================================================================
*/
bool PlaneSegment::TestPoint( const Point3f &point ) const
{
	if( !Plane::TestPoint( point ) )
	{
		// This given point doesn't even exist inside the plane.
		return false;
	}

	// Make of copy of the point to test, and rotate it into the XZ plane.
	const Angle3f planeRotation = -vector_to_angle( _normal );
	const Matrix4f rotationMatrix = Matrix4f::BuildRotation( planeRotation );
	const Point3f alignedPoint = rotationMatrix * point;

	// Store the position at which our point was rotated on the XZ plane.
	const float alignedPointX = alignedPoint.GetX();
	const float alignedPointZ = alignedPoint.GetZ();

	const float halfX = _xMax / 2.0f;
	const float halfY = _zMax / 2.0f;

	// Build the min and max XZ plane extents.
	const float xMin = _point.GetX() - halfX;
	const float xMax = xMin + _xMax;
	const float zMin = _point.GetY() - halfY;
	const float zMax = zMin + _zMax;

	// Ensure the point falls within the XZ boundary.
	if( ( alignedPointX < xMin || alignedPointX > xMax ) || ( alignedPointZ < zMin || alignedPointZ > zMax ) )
	{
		return false;
	}

	return true;
}

/*
===============================================================================
PlaneSegment::LineIntersection
	Determines where on a plane an intersection with a line occurs.
===============================================================================
*/
bool PlaneSegment::LineIntersection( const Line3f &line, Point3f *pOut ) const
{
	// Get the intersection position.
	Point3f intersection;
	bool intersected = Plane::LineIntersection( line, &intersection );

	if( !intersected )
	{
		return false;
	}

	// Ensure the intersection position belongs to this plane.
	if( TestPoint( intersection ) )
	{
		*pOut = intersection;
		return true;
	}

	return false;
}

/*
===============================================================================
PlaneSegment::LineSegmentIntersection
	Determines where on a plane an intersection with a line occurs.
===============================================================================
*/
bool PlaneSegment::LineSegmentIntersection( const LineSegment3f &line, Point3f *pOut ) const
{
	// Get the intersection position.
	Point3f intersection;
	bool intersected = Plane::LineSegmentIntersection( line, &intersection );

	if( !intersected )
	{
		return false;
	}

	// Ensure the intersection position belongs to this plane.
	if( TestPoint( intersection ) )
	{
		*pOut = intersection;
		return true;
	}

	return false;
}