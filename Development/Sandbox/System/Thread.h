/*
===============================================================================

	Thread.h
		Implements thread classes (mutex, thread, etc).

===============================================================================
*/

#ifndef __THREAD_H__
#define __THREAD_H__

namespace sandbox
{
	namespace system
	{

		class Mutex
		{
		public:
			/*
			-------------------------------------------------------------------------
				Creation\destruction.
			-------------------------------------------------------------------------
			*/
			virtual			~Mutex()	{ }
			static Mutex *	Create();

			/*
			-------------------------------------------------------------------------
				Mutex control.
			-------------------------------------------------------------------------
			*/
			virtual void	Lock() = 0;
			virtual bool	TryLock() = 0;
			virtual void	Unlock() = 0;

			/*
			-------------------------------------------------------------------------
				Accessors.
			-------------------------------------------------------------------------
			*/
			virtual bool	IsLocked() const = 0;
		};

	} // namespace system;
} // namespace sandbox;

#endif // __THREAD_H__