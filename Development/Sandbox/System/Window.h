/*
===============================================================================

	Window.h
		Public window interface.

===============================================================================
*/

#ifndef __WINDOW_H__
#define __WINDOW_H__

#include <string>

namespace sandbox
{
	namespace system
	{
		/*
		===============================================================================
		Window
			Platform neutral window interface.
		===============================================================================
		*/
		class Window
		{
		public:
			/*
			-------------------------------------------------------------------------
				Creation\destruction.
			-------------------------------------------------------------------------
			*/
			virtual			~Window()	{ }

			static Window *	Create( int xPos, int yPos, int width, int height, const std::string &title );

			/*
			-------------------------------------------------------------------------
				Mutators.
			-------------------------------------------------------------------------
			*/
			virtual bool	SetPosition( int xPos, int yPos ) = 0;
			virtual bool	SetSize( int width, int height ) = 0;
			virtual bool	SetFullscreen( bool fullscreen ) = 0;
			virtual bool	Center() = 0;

			/*
			-------------------------------------------------------------------------
				Accessors.
			-------------------------------------------------------------------------
			*/
			virtual float	GetAspectRatio() const = 0;
			virtual void	GetPosition( int *pX, int *pY ) const = 0;
			virtual void	GetSize( int *pX, int *pY ) const = 0;
			virtual int		GetWidth() const = 0;
			virtual int		GetHeight() const = 0;
			virtual bool	IsFullscreen() const = 0;

			/*
			-------------------------------------------------------------------------
				Platform specific code.
			-------------------------------------------------------------------------
			*/
			virtual void *	GetHandle() const = 0;
			virtual bool	RunEvents() = 0;
		};

	} // namespace system;
} // namespace sandbox;

#endif // __WINDOW_H__