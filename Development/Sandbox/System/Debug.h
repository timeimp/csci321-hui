/*
===============================================================================

	Debug.h
		Functions and macros which assist with debugging.

===============================================================================
*/

#ifndef __DEBUG_H__
#define __DEBUG_H__

/*
===============================================================================
	Prototypes.
===============================================================================
*/
void	assertion_failure( const char *pszExpression, const char *pszFile, int nLine );
void	message_box( const char *pszMessage );
void	breakpoint();

/*
===============================================================================
	Helpful macros.
===============================================================================
*/

// Triggers a breakpoint with the given expression is false.
#define sbAssert( expr )														\
	if( !( expr ) )																\
	{																			\
		assertion_failure( #expr, __FILE__, __LINE__ );							\
	}

// Displays the given error message if the given expression is false.
#define sbError( expr, message )												\
	if( !( expr ) )																\
	{																			\
		message_box( message );													\
	}

#endif // __DEBUG_H__