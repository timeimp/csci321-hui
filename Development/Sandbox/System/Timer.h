/*
===============================================================================

	Timer.h
		Platform timekeeping.

===============================================================================
*/

#ifndef __TIMER_H__
#define __TIMER_H__

namespace sandbox
{
	namespace system
	{
		typedef unsigned long long tick_t;

		/*
		===============================================================================
		Timer
			Keeps track of the application time.
		===============================================================================
		*/
		class Timer
		{
		public:
			static void		Start();
			static void		Stop();

			static tick_t	AppTime();
		};

	}
}

#endif // __TIMER_H__