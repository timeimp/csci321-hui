/*
===============================================================================

	Win32_Window.cpp
		Win32 Window class implementation.

===============================================================================
*/

#include "Framework/Sandbox.h"
#include "System/Window.h"

#include <Windows.h>

using namespace std;
using namespace sandbox::system;

/*
===============================================================================
	Private prototypes.
===============================================================================
*/
static LRESULT WINAPI process_window_events( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam );

/*
===============================================================================
Window_Win32
	Win32 implementation of the Window interface.
===============================================================================
*/
class Window_Win32 : public Window
{
public:
	/*
	-------------------------------------------------------------------------
		Creation\destruction.
	-------------------------------------------------------------------------
	*/
					Window_Win32( int xPos, int yPos, int width, int height, const std::string &title );
	virtual			~Window_Win32();

	/*
	-------------------------------------------------------------------------
		Mutators.
	-------------------------------------------------------------------------
	*/
	virtual bool	SetPosition( int xPos, int yPos );
	virtual bool	SetSize( int width, int height );
	virtual bool	SetFullscreen( bool fullscreen );
	virtual bool	Center();

	/*
	-------------------------------------------------------------------------
		Accessors.
	-------------------------------------------------------------------------
	*/
	virtual float	GetAspectRatio() const;
	virtual void	GetPosition( int *pX, int *pY ) const;
	virtual void	GetSize( int *pX, int *pY ) const;
	virtual int		GetWidth() const				{ return _nWidth; }
	virtual int		GetHeight() const				{ return _nHeight; }
	virtual bool	IsFullscreen() const;

	/*
	-------------------------------------------------------------------------
		Platform specific code.
	-------------------------------------------------------------------------
	*/
	virtual void *	GetHandle() const;
	virtual bool	RunEvents();

private:
	/*
	-------------------------------------------------------------------------
		Window creation.
	-------------------------------------------------------------------------
	*/
	void			RegisterWindowClass();
	void			MakeWindow();

	/*
	-------------------------------------------------------------------------
		Utilities.
	-------------------------------------------------------------------------
	*/
	DWORD			GetStyle( bool fullscreen );
	void			AdjustClientArea( int baseWidth, int baseHeight, int *pWidth, int *pHeight );
	bool			GoFullscreen();
	bool			GoWindowed();

private:
	/*
	-------------------------------------------------------------------------
		Window state.
	-------------------------------------------------------------------------
	*/
	int			_nXPos;
	int			_nYPos;
	int			_nWidth;
	int			_nHeight;
	bool			_bFullscreen;
	string			_strTitle;

	HWND			_hWnd;
};

/*
===============================================================================
	Window implemenation
===============================================================================
*/

/*
===============================================================================
Window::Create
	Creates a new Win32 window instance.
===============================================================================
*/
Window *Window::Create( int xPos, int yPos, int width, int height, const std::string &title )
{
	return new Window_Win32( xPos, yPos, width, height, title );
}

/*
===============================================================================
	Window_Win32 implemenation
===============================================================================
*/

/*
===============================================================================
Window_Win32::Window_Win32
	Registers the new window with Windows.
===============================================================================
*/
Window_Win32::Window_Win32( int xPos, int yPos, int width, int height, const std::string &title ) :
	_nXPos( xPos ),
	_nYPos( yPos ),
	_nWidth( width ),
	_nHeight( height ),
	_strTitle( title ),
	_bFullscreen( false ),
	_hWnd( NULL )
{
	RegisterWindowClass();
	MakeWindow();
}

/*
===============================================================================
Window_Win32::~Window_Win32
	Cleans this window instance.
===============================================================================
*/
Window_Win32::~Window_Win32()
{
	if( _bFullscreen )
	{
		GoWindowed();
	}

	HINSTANCE hInstance = GetModuleHandle( NULL );

	DestroyWindow( _hWnd );
	UnregisterClass( _strTitle.c_str(), hInstance );
}

/*
===============================================================================
Window_Win32::SetPosition
	Repositions the window on screen.
===============================================================================
*/
bool Window_Win32::SetPosition( int xPos, int yPos )
{
	int clientWidth, clientHeight;

	// Get the dimensions of the window client area.
	AdjustClientArea( _nWidth, _nHeight, &clientWidth, &clientHeight );

	BOOL success = MoveWindow( _hWnd, xPos, yPos, clientWidth, clientHeight, FALSE );
	if( success )
	{
		// Update internal window state.
		_nXPos = xPos;
		_nYPos = yPos;
		return true;
	}

	// Failed to set new position, revert to the old position.
	success = MoveWindow( _hWnd, _nXPos, _nYPos, clientWidth, clientHeight, FALSE );

	// If this fails then Windows failed to reset the window position.
	sbAssert( success );

	return false;
}

/*
===============================================================================
Window_Win32::SetSize
	Sets the horizontal and vertical dimensions of the window.
===============================================================================
*/
bool Window_Win32::SetSize( int width, int height )
{
	int clientWidth, clientHeight;

	// Get the dimensions of the window client area.
	AdjustClientArea( width, height, &clientWidth, &clientHeight );

	BOOL success = MoveWindow( _hWnd, _nXPos, _nYPos, clientWidth, clientHeight, FALSE );
	if( success )
	{
		// Update internal window state.
		_nWidth = width;
		_nHeight = height;
		return true;
	}

	// Failed to set new position, revert to the old position.
	AdjustClientArea( _nWidth, _nHeight, &clientWidth, &clientHeight );
	success = MoveWindow( _hWnd, _nXPos, _nYPos, clientWidth, clientHeight, FALSE );

	// If this fails then Windows failed to reset the window position.
	sbAssert( success );

	return false;
}

/*
===============================================================================
Window_Win32::SetFullscreen
	Toggles the window between fullscreen and windowed mode.
===============================================================================
*/
bool Window_Win32::SetFullscreen( bool fullscreen )
{
	if( fullscreen && !_bFullscreen )
	{
		return GoFullscreen();
	}
	else if( !fullscreen && _bFullscreen )
	{
		return GoWindowed();
	}

	return false;
}

/*
===============================================================================
Window_Win32::Center
	Centers the window on screen.
===============================================================================
*/
bool Window_Win32::Center()
{
	int clientWidth, clientHeight;
	AdjustClientArea( _nWidth, _nHeight, &clientWidth, &clientHeight );

	int screenWidth = GetSystemMetrics( SM_CXSCREEN );
	int screenHeight = GetSystemMetrics( SM_CYSCREEN );

	int xPos = ( screenWidth - clientWidth ) / 2;
	int yPos = ( screenHeight - clientHeight ) / 2;

	return SetPosition( xPos, yPos );
}

/*
===============================================================================
Window_Win32::GetAspectRatio
	Determines the windows aspect ratio.
===============================================================================
*/
float Window_Win32::GetAspectRatio() const
{
	return i2fl( _nWidth ) / i2fl( _nHeight );
}

/*
===============================================================================
Window_Win32::GetPosition
	Provides access to the current window position.
===============================================================================
*/
void Window_Win32::GetPosition( int *pX, int *pY ) const
{
	if( pX )
	{
		*pX = _nXPos;
	}

	if( pY )
	{
		*pY = _nYPos;
	}
}

/*
===============================================================================
Window_Win32::GetSize
	Provides access to the current window size.
===============================================================================
*/
void Window_Win32::GetSize( int *pWidth, int *pHeight ) const
{
	if( pWidth )
	{
		*pWidth = _nWidth;
	}

	if( pHeight )
	{
		*pHeight = _nHeight;
	}
}

/*
===============================================================================
Window_Win32::IsFullscreen
	Determines if the window is fullscreen.
===============================================================================
*/
bool Window_Win32::IsFullscreen() const
{
	return _bFullscreen;
}

/*
===============================================================================
Window_Win32::GetHandle
	Provides access to the Windows window handle.
===============================================================================
*/
void *Window_Win32::GetHandle() const
{
	return _hWnd;
}

/*
===============================================================================
Window_Win32::RunEvents
	Dispatches window events.
===============================================================================
*/
bool Window_Win32::RunEvents()
{
	static MSG msg;

	/* Process all window messages */
	while( PeekMessage( &msg, NULL, 0U, 0U, PM_REMOVE ) )
	{
		TranslateMessage( &msg );
		DispatchMessage( &msg );
	}

	if( msg.message == WM_QUIT )
	{
		return false;
	}

	return true;
}

/*
===============================================================================
Window_Win32::RegisterWindowClass
	Registers the window type with Windows.
===============================================================================
*/
void Window_Win32::RegisterWindowClass()
{
	WNDCLASSEX wc;

	wc.cbSize			= sizeof( WNDCLASSEX );
	wc.style			= CS_OWNDC;
	wc.lpfnWndProc		= process_window_events;
	wc.cbClsExtra		= 0L;
	wc.cbWndExtra		= 0L;
	wc.hInstance		= GetModuleHandle( NULL );
	wc.hIcon			= NULL;
	wc.hCursor			= NULL;
	wc.hbrBackground	= NULL;
	wc.lpszMenuName		= NULL;
	wc.lpszClassName	= _strTitle.c_str();
	wc.hIconSm			= NULL;

	RegisterClassEx( &wc );
}

/*
===============================================================================
Window_Win32::MakeWindow
	Requests that Windows create our window.
===============================================================================
*/
void Window_Win32::MakeWindow()
{
	int clientWidth, clientHeight;
	DWORD style;

	style = GetStyle( _bFullscreen );
	AdjustClientArea( _nWidth, _nHeight, &clientWidth, &clientHeight );

	// Create the window instance.
	_hWnd = CreateWindow( _strTitle.c_str(), _strTitle.c_str(), style, _nXPos, _nYPos, clientWidth, clientHeight, NULL, NULL, NULL, NULL );
	sbAssert( _hWnd );

	// Refresh the window.
	ShowWindow( _hWnd, SW_SHOWDEFAULT );
	UpdateWindow( _hWnd );
}

/*
===============================================================================
Window_Win32::GetStyle
	Determines the window style needed for the given configuration.
===============================================================================
*/
DWORD Window_Win32::GetStyle( bool fullscreen )
{
	if( fullscreen )
	{
		return WS_EX_TOPMOST | WS_POPUP;
	}
	else
	{
		return WS_POPUP | WS_CAPTION | WS_SYSMENU;
	}
}

/*
===============================================================================
Window_Win32::MakeWindow
	Rescales the window size so the client area is the requested window size.
===============================================================================
*/
void Window_Win32::AdjustClientArea( int baseWidth, int baseHeight, int *pWidth, int *pHeight )
{
	if( _bFullscreen )
	{
		*pWidth = baseWidth;
		*pHeight = baseHeight;
		return;
	}

	RECT rect;

	rect.left = 0;
	rect.top = 0;
	rect.right= baseWidth;
	rect.bottom= baseHeight;

	// Adjust the input width\height so that the window client area is the requested size.
	DWORD style = GetStyle( FALSE );
	AdjustWindowRect( &rect, style, FALSE );

	*pWidth= rect.right - rect.left;
	*pHeight= rect.bottom - rect.top;
}

/*
===============================================================================
Window_Win32::GoFullscreen
	Changes the desktop resolution to match the window dimensions.
===============================================================================
*/
bool Window_Win32::GoFullscreen()
{
	DEVMODE devMode;
	memset( &devMode, 0, sizeof(devMode));

	// Configure the new device mode.
	devMode.dmSize = sizeof(devMode);
	devMode.dmPelsWidth = _nWidth;
	devMode.dmPelsHeight = _nHeight;
	devMode.dmBitsPerPel = 32;
	devMode.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

	// Set our new device mode. 
	// CDS_FULLSCREEN gets rid of the start bar.
	DWORD error = ChangeDisplaySettings( &devMode, CDS_FULLSCREEN );
	if( error != DISP_CHANGE_SUCCESSFUL )
	{
		// Failed to go to fullscreen, attempt to revert to the default Windows mode.
		error = ChangeDisplaySettings( NULL, 0 );
		sbAssert( error == DISP_CHANGE_SUCCESSFUL );
		return FALSE;
	}

	// Update the window style.
	DWORD style = GetStyle( true );
	error = SetWindowLongPtr( _hWnd, GWL_STYLE, style );

	// Ensure the window appears in the fullscreen region.
	sbAssert( SetPosition( 0, 0 ) );

	ShowWindow( _hWnd, SW_SHOWNORMAL );
	ShowCursor( FALSE );

	return TRUE;
}

/*
===============================================================================
Window_Win32::GoWindowed
	Changes the desktop resolution to match the Windows default resolution.
===============================================================================
*/
bool Window_Win32::GoWindowed()
{
	DWORD error = ChangeDisplaySettings( NULL, 0 );
	sbAssert( error == DISP_CHANGE_SUCCESSFUL );

	// Refresh the window properties.
	sbAssert( SetSize( _nWidth, _nHeight ) );
	sbAssert( SetPosition( _nXPos, _nYPos ) );
	Center();

	// Update the window style.
	DWORD style = GetStyle( true );
	error = SetWindowLongPtr( _hWnd, GWL_STYLE, style );

	ShowWindow( _hWnd, SW_SHOWNORMAL );
	ShowCursor( TRUE );

	return true;
}

/*
===============================================================================
	Private code.
===============================================================================
*/

/*
===============================================================================
process_window_events
	Handles all incoming window messages
===============================================================================
*/
static LRESULT WINAPI process_window_events( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
	switch( msg )
	{
		case WM_CREATE:
			return 0;

		case WM_DESTROY:
			PostQuitMessage( 0 );
			return 0;

		default:
			break;
	}

	return DefWindowProc( hWnd, msg, wParam, lParam );
}