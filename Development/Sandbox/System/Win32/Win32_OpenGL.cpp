/*
===============================================================================

	Win32_OpenGL.cpp
		Windows specific OpenGL functionality.

===============================================================================
*/

#include "Framework/Sandbox.h"
#include "System/OpenGL.h"

#include <iostream>
#include <Windows.h>

using namespace std;
using namespace sandbox::system;
using namespace sandbox::render;

/*
===============================================================================
	Global variables.
===============================================================================
*/
static HDC			gDeviceContext = NULL;
static HGLRC		gRenderContext = NULL;

Window *GlContext::_pWindow = NULL;

namespace sandbox
{
	namespace render
	{
		PFNGLACTIVETEXTUREPROC glActiveTexture = NULL;
		PFNGLATTACHSHADERPROC glAttachShader = NULL;
		PFNGLBEGINQUERYPROC glBeginQuery = NULL;
		PFNGLBINDATTRIBLOCATIONPROC glBindAttribLocation = NULL;
		PFNGLBINDBUFFERPROC glBindBuffer = NULL;
		PFNGLBLENDCOLORPROC glBlendColor = NULL;
		PFNGLBLENDEQUATIONPROC glBlendEquation = NULL;
		PFNGLBLENDEQUATIONSEPARATEPROC glBlendEquationSeparate = NULL;
		PFNGLBLENDFUNCSEPARATEPROC glBlendFuncSeparate = NULL;
		PFNGLBUFFERDATAPROC glBufferData = NULL;
		PFNGLBUFFERSUBDATAPROC glBufferSubData = NULL;
		PFNGLCLIENTACTIVETEXTUREPROC glClientActiveTexture = NULL;
		PFNGLCOMPILESHADERPROC glCompileShader = NULL;
		PFNGLCOMPRESSEDTEXIMAGE1DPROC glCompressedTexImage1D = NULL;
		PFNGLCOMPRESSEDTEXIMAGE2DPROC glCompressedTexImage2D = NULL;
		PFNGLCOMPRESSEDTEXIMAGE3DPROC glCompressedTexImage3D = NULL;
		PFNGLCOMPRESSEDTEXSUBIMAGE1DPROC glCompressedTexSubImage1D = NULL;
		PFNGLCOMPRESSEDTEXSUBIMAGE2DPROC glCompressedTexSubImage2D = NULL;
		PFNGLCOMPRESSEDTEXSUBIMAGE3DPROC glCompressedTexSubImage3D = NULL;
		PFNGLCOPYTEXSUBIMAGE3DPROC glCopyTexSubImage3D = NULL;
		PFNGLCREATEPROGRAMPROC glCreateProgram = NULL;
		PFNGLCREATESHADERPROC glCreateShader = NULL;
		PFNGLDELETEBUFFERSPROC glDeleteBuffers = NULL;
		PFNGLDELETEPROGRAMPROC glDeleteProgram = NULL;
		PFNGLDELETEQUERIESPROC glDeleteQueries = NULL;
		PFNGLDELETESHADERPROC glDeleteShader = NULL;
		PFNGLDETACHSHADERPROC glDetachShader = NULL;
		PFNGLDISABLEVERTEXATTRIBARRAYPROC glDisableVertexAttribArray = NULL;
		PFNGLDRAWBUFFERSPROC glDrawBuffers = NULL;
		PFNGLDRAWRANGEELEMENTSPROC glDrawRangeElements = NULL;
		PFNGLENABLEVERTEXATTRIBARRAYPROC glEnableVertexAttribArray = NULL;
		PFNGLENDQUERYPROC glEndQuery = NULL;
		PFNGLFOGCOORDDPROC glFogCoordd = NULL;
		PFNGLFOGCOORDFPROC glFogCoordf = NULL;
		PFNGLFOGCOORDDVPROC glFogCoorddv = NULL;
		PFNGLFOGCOORDFVPROC glFogCoordfv = NULL;
		PFNGLFOGCOORDPOINTERPROC glFogCoordPointer = NULL;
		PFNGLGENBUFFERSPROC glGenBuffers = NULL;
		PFNGLGENQUERIESPROC glGenQueries = NULL;
		PFNGLGENERATEMIPMAPEXTPROC glGenerateMipmap = NULL;
		PFNGLGETACTIVEATTRIBPROC glGetActiveAttrib = NULL;
		PFNGLGETACTIVEUNIFORMPROC glGetActiveUniform = NULL;
		PFNGLGETATTACHEDSHADERSPROC glGetAttachedShaders = NULL;
		PFNGLGETATTRIBLOCATIONPROC glGetAttribLocation = NULL;
		PFNGLGETBUFFERPARAMETERIVPROC glGetBufferParameteriv = NULL;
		PFNGLGETBUFFERPOINTERVPROC glGetBufferPointerv = NULL;
		PFNGLGETBUFFERSUBDATAPROC glGetBufferSubData = NULL;
		PFNGLGETCOMPRESSEDTEXIMAGEPROC glGetCompressedTexImage = NULL;
		PFNGLGETPROGRAMIVPROC glGetProgramiv = NULL;
		PFNGLGETPROGRAMINFOLOGPROC glGetProgramInfoLog = NULL;
		PFNGLGETQUERYOBJECTIVPROC glGetQueryObjectiv = NULL;
		PFNGLGETQUERYOBJECTUIVPROC glGetQueryObjectuiv = NULL;
		PFNGLGETQUERYIVPROC glGetQueryiv = NULL;
		PFNGLGETSHADERIVPROC glGetShaderiv = NULL;
		PFNGLGETSHADERINFOLOGPROC glGetShaderInfoLog = NULL;
		PFNGLGETSHADERSOURCEPROC glGetShaderSource = NULL;
		PFNGLGETUNIFORMFVPROC glGetUniformfv = NULL;
		PFNGLGETUNIFORMIVPROC glGetUniformiv = NULL;
		PFNGLGETUNIFORMLOCATIONPROC glGetUniformLocation = NULL;
		PFNGLGETVERTEXATTRIBDVPROC glGetVertexAttribdv = NULL;
		PFNGLGETVERTEXATTRIBFVPROC glGetVertexAttribfv = NULL;
		PFNGLGETVERTEXATTRIBIVPROC glGetVertexAttribiv = NULL;
		PFNGLGETVERTEXATTRIBPOINTERVPROC glGetVertexAttribPointerv = NULL;
		PFNGLISBUFFERPROC glIsBuffer = NULL;
		PFNGLISPROGRAMPROC glIsProgram = NULL;
		PFNGLISQUERYPROC glIsQuery = NULL;
		PFNGLISSHADERPROC glIsShader = NULL;
		PFNGLLINKPROGRAMPROC glLinkProgram = NULL;
		PFNGLLOADTRANSPOSEMATRIXDPROC glLoadTransposeMatrixd = NULL;
		PFNGLLOADTRANSPOSEMATRIXFPROC glLoadTransposeMatrixf = NULL;
		PFNGLMAPBUFFERPROC glMapBuffer = NULL;
		PFNGLMULTTRANSPOSEMATRIXDPROC glMultTransposeMatrixd = NULL;
		PFNGLMULTTRANSPOSEMATRIXFPROC glMultTransposeMatrixf = NULL;
		PFNGLMULTIDRAWARRAYSPROC glMultiDrawArrays = NULL;
		PFNGLMULTIDRAWELEMENTSPROC glMultiDrawElements = NULL;
		PFNGLMULTITEXCOORD1SPROC glMultiTexCoord1s = NULL;
		PFNGLMULTITEXCOORD1IPROC glMultiTexCoord1i = NULL;
		PFNGLMULTITEXCOORD1FPROC glMultiTexCoord1f = NULL;
		PFNGLMULTITEXCOORD1DPROC glMultiTexCoord1d = NULL;
		PFNGLMULTITEXCOORD2SPROC glMultiTexCoord2s = NULL;
		PFNGLMULTITEXCOORD2IPROC glMultiTexCoord2i = NULL;
		PFNGLMULTITEXCOORD2FPROC glMultiTexCoord2f = NULL;
		PFNGLMULTITEXCOORD2DPROC glMultiTexCoord2d = NULL;
		PFNGLMULTITEXCOORD3SPROC glMultiTexCoord3s = NULL;
		PFNGLMULTITEXCOORD3IPROC glMultiTexCoord3i = NULL;
		PFNGLMULTITEXCOORD3FPROC glMultiTexCoord3f = NULL;
		PFNGLMULTITEXCOORD3DPROC glMultiTexCoord3d = NULL;
		PFNGLMULTITEXCOORD4SPROC glMultiTexCoord4s = NULL;
		PFNGLMULTITEXCOORD4IPROC glMultiTexCoord4i = NULL;
		PFNGLMULTITEXCOORD4FPROC glMultiTexCoord4f = NULL;
		PFNGLMULTITEXCOORD4DPROC glMultiTexCoord4d = NULL;
		PFNGLMULTITEXCOORD1SVPROC glMultiTexCoord1sv = NULL;
		PFNGLMULTITEXCOORD1IVPROC glMultiTexCoord1iv = NULL;
		PFNGLMULTITEXCOORD1FVPROC glMultiTexCoord1fv = NULL;
		PFNGLMULTITEXCOORD1DVPROC glMultiTexCoord1dv = NULL;
		PFNGLMULTITEXCOORD2SVPROC glMultiTexCoord2sv = NULL;
		PFNGLMULTITEXCOORD2IVPROC glMultiTexCoord2iv = NULL;
		PFNGLMULTITEXCOORD2FVPROC glMultiTexCoord2fv = NULL;
		PFNGLMULTITEXCOORD2DVPROC glMultiTexCoord2dv = NULL;
		PFNGLMULTITEXCOORD3SVPROC glMultiTexCoord3sv = NULL;
		PFNGLMULTITEXCOORD3IVPROC glMultiTexCoord3iv = NULL;
		PFNGLMULTITEXCOORD3FVPROC glMultiTexCoord3fv = NULL;
		PFNGLMULTITEXCOORD3DVPROC glMultiTexCoord3dv = NULL;
		PFNGLMULTITEXCOORD4SVPROC glMultiTexCoord4sv = NULL;
		PFNGLMULTITEXCOORD4IVPROC glMultiTexCoord4iv = NULL;
		PFNGLMULTITEXCOORD4FVPROC glMultiTexCoord4fv = NULL;
		PFNGLMULTITEXCOORD4DVPROC glMultiTexCoord4dv = NULL;
		PFNGLPOINTPARAMETERFPROC glPointParameterf = NULL;
		PFNGLPOINTPARAMETERIPROC glPointParameteri = NULL;
		PFNGLPOINTPARAMETERFVPROC glPointParameterfv = NULL;
		PFNGLPOINTPARAMETERIVPROC glPointParameteriv = NULL;
		PFNGLSAMPLECOVERAGEPROC glSampleCoverage = NULL;
		PFNGLSECONDARYCOLOR3BPROC glSecondaryColor3b = NULL;
		PFNGLSECONDARYCOLOR3SPROC glSecondaryColor3s = NULL;
		PFNGLSECONDARYCOLOR3IPROC glSecondaryColor3i = NULL;
		PFNGLSECONDARYCOLOR3FPROC glSecondaryColor3f = NULL;
		PFNGLSECONDARYCOLOR3DPROC glSecondaryColor3d = NULL;
		PFNGLSECONDARYCOLOR3UBPROC glSecondaryColor3ub = NULL;
		PFNGLSECONDARYCOLOR3USPROC glSecondaryColor3us = NULL;
		PFNGLSECONDARYCOLOR3UIPROC glSecondaryColor3ui = NULL;
		PFNGLSECONDARYCOLOR3BVPROC glSecondaryColor3bv = NULL;
		PFNGLSECONDARYCOLOR3SVPROC glSecondaryColor3sv = NULL;
		PFNGLSECONDARYCOLOR3IVPROC glSecondaryColor3iv = NULL;
		PFNGLSECONDARYCOLOR3FVPROC glSecondaryColor3fv = NULL;
		PFNGLSECONDARYCOLOR3DVPROC glSecondaryColor3dv = NULL;
		PFNGLSECONDARYCOLOR3UBVPROC glSecondaryColor3ubv = NULL;
		PFNGLSECONDARYCOLOR3USVPROC glSecondaryColor3usv = NULL;
		PFNGLSECONDARYCOLOR3UIVPROC glSecondaryColor3uiv = NULL;
		PFNGLSECONDARYCOLORPOINTERPROC glSecondaryColorPointer = NULL;
		PFNGLSHADERSOURCEPROC glShaderSource = NULL;
		PFNGLSTENCILFUNCSEPARATEPROC glStencilFuncSeparate = NULL;
		PFNGLSTENCILMASKSEPARATEPROC glStencilMaskSeparate = NULL;
		PFNGLSTENCILOPSEPARATEPROC glStencilOpSeparate = NULL;
		PFNGLTEXIMAGE3DPROC glTexImage3D = NULL;
		PFNGLTEXSUBIMAGE3DPROC glTexSubImage3D = NULL;
		PFNGLUNIFORM1FPROC glUniform1f = NULL;
		PFNGLUNIFORM1IPROC glUniform1i = NULL;
		PFNGLUNIFORM2FPROC glUniform2f = NULL;
		PFNGLUNIFORM2IPROC glUniform2i = NULL;
		PFNGLUNIFORM3FPROC glUniform3f = NULL;
		PFNGLUNIFORM3IPROC glUniform3i = NULL;
		PFNGLUNIFORM4FPROC glUniform4f = NULL;
		PFNGLUNIFORM4IPROC glUniform4i = NULL;
		PFNGLUNIFORM1FVPROC glUniform1fv = NULL;
		PFNGLUNIFORM1IVPROC glUniform1iv = NULL;
		PFNGLUNIFORM2FVPROC glUniform2fv = NULL;
		PFNGLUNIFORM2IVPROC glUniform2iv = NULL;
		PFNGLUNIFORM3FVPROC glUniform3fv = NULL;
		PFNGLUNIFORM3IVPROC glUniform3iv = NULL;
		PFNGLUNIFORM4FVPROC glUniform4fv = NULL;
		PFNGLUNIFORM4IVPROC glUniform4iv = NULL;
		PFNGLUNIFORMMATRIX2FVPROC glUniformMatrix2fv = NULL;
		PFNGLUNIFORMMATRIX3FVPROC glUniformMatrix3fv = NULL;
		PFNGLUNIFORMMATRIX4FVPROC glUniformMatrix4fv = NULL;
		PFNGLUNIFORMMATRIX2X3FVPROC glUniformMatrix2x3fv = NULL;
		PFNGLUNIFORMMATRIX3X2FVPROC glUniformMatrix3x2fv = NULL;
		PFNGLUNIFORMMATRIX2X4FVPROC glUniformMatrix2x4fv = NULL;
		PFNGLUNIFORMMATRIX4X2FVPROC glUniformMatrix4x2fv = NULL;
		PFNGLUNIFORMMATRIX3X4FVPROC glUniformMatrix3x4fv = NULL;
		PFNGLUNIFORMMATRIX4X3FVPROC glUniformMatrix4x3fv = NULL;
		PFNGLUNMAPBUFFERPROC glUnmapBuffer = NULL;
		PFNGLUSEPROGRAMPROC glUseProgram = NULL;
		PFNGLVALIDATEPROGRAMPROC glValidateProgram = NULL;
		PFNGLVERTEXATTRIB1FPROC glVertexAttrib1f = NULL;
		PFNGLVERTEXATTRIB1SPROC glVertexAttrib1s = NULL;
		PFNGLVERTEXATTRIB1DPROC glVertexAttrib1d = NULL;
		PFNGLVERTEXATTRIB2FPROC glVertexAttrib2f = NULL;
		PFNGLVERTEXATTRIB2SPROC glVertexAttrib2s = NULL;
		PFNGLVERTEXATTRIB2DPROC glVertexAttrib2d = NULL;
		PFNGLVERTEXATTRIB3FPROC glVertexAttrib3f = NULL;
		PFNGLVERTEXATTRIB3SPROC glVertexAttrib3s = NULL;
		PFNGLVERTEXATTRIB3DPROC glVertexAttrib3d = NULL;
		PFNGLVERTEXATTRIB4FPROC glVertexAttrib4f = NULL;
		PFNGLVERTEXATTRIB4SPROC glVertexAttrib4s = NULL;
		PFNGLVERTEXATTRIB4DPROC glVertexAttrib4d = NULL;
		PFNGLVERTEXATTRIB4NUBPROC glVertexAttrib4Nub = NULL;
		PFNGLVERTEXATTRIB1FVPROC glVertexAttrib1fv = NULL;
		PFNGLVERTEXATTRIB1SVPROC glVertexAttrib1sv = NULL;
		PFNGLVERTEXATTRIB1DVPROC glVertexAttrib1dv = NULL;
		PFNGLVERTEXATTRIB2FVPROC glVertexAttrib2fv = NULL;
		PFNGLVERTEXATTRIB2SVPROC glVertexAttrib2sv = NULL;
		PFNGLVERTEXATTRIB2DVPROC glVertexAttrib2dv = NULL;
		PFNGLVERTEXATTRIB3FVPROC glVertexAttrib3fv = NULL;
		PFNGLVERTEXATTRIB3SVPROC glVertexAttrib3sv = NULL;
		PFNGLVERTEXATTRIB3DVPROC glVertexAttrib3dv = NULL;
		PFNGLVERTEXATTRIB4FVPROC glVertexAttrib4fv = NULL;
		PFNGLVERTEXATTRIB4SVPROC glVertexAttrib4sv = NULL;
		PFNGLVERTEXATTRIB4DVPROC glVertexAttrib4dv = NULL;
		PFNGLVERTEXATTRIB4IVPROC glVertexAttrib4iv = NULL;
		PFNGLVERTEXATTRIB4BVPROC glVertexAttrib4bv = NULL;
		PFNGLVERTEXATTRIB4UBVPROC glVertexAttrib4ubv = NULL;
		PFNGLVERTEXATTRIB4USVPROC glVertexAttrib4usv = NULL;
		PFNGLVERTEXATTRIB4UIVPROC glVertexAttrib4uiv = NULL;
		PFNGLVERTEXATTRIB4NBVPROC glVertexAttrib4Nbv = NULL;
		PFNGLVERTEXATTRIB4NSVPROC glVertexAttrib4Nsv = NULL;
		PFNGLVERTEXATTRIB4NIVPROC glVertexAttrib4Niv = NULL;
		PFNGLVERTEXATTRIB4NUBVPROC glVertexAttrib4Nubv = NULL;
		PFNGLVERTEXATTRIB4NUSVPROC glVertexAttrib4Nusv = NULL;
		PFNGLVERTEXATTRIB4NUIVPROC glVertexAttrib4Nuiv = NULL;
		PFNGLVERTEXATTRIBPOINTERPROC glVertexAttribPointer = NULL;
		PFNGLWINDOWPOS2SPROC glWindowPos2s = NULL;
		PFNGLWINDOWPOS2IPROC glWindowPos2i = NULL;
		PFNGLWINDOWPOS2FPROC glWindowPos2f = NULL;
		PFNGLWINDOWPOS2DPROC glWindowPos2d = NULL;
		PFNGLWINDOWPOS3SPROC glWindowPos3s = NULL;
		PFNGLWINDOWPOS3IPROC glWindowPos3i = NULL;
		PFNGLWINDOWPOS3FPROC glWindowPos3f = NULL;

		PFNGLGENFRAMEBUFFERSEXTPROC glGenFramebuffersEXT = NULL;
		PFNGLBINDFRAMEBUFFEREXTPROC glBindFramebufferEXT = NULL;
		PFNGLFRAMEBUFFERTEXTURE2DEXTPROC glFramebufferTexture2DEXT = NULL;
		PFNGLDELETEFRAMEBUFFERSEXTPROC glDeleteFramebuffersEXT = NULL;
		PFNGLISFRAMEBUFFEREXTPROC glIsFramebufferEXT = NULL;
		PFNGLCHECKFRAMEBUFFERSTATUSEXTPROC glCheckFramebufferStatusEXT = NULL;
		PFNGLFRAMEBUFFERRENDERBUFFEREXTPROC glFramebufferRenderbufferEXT = NULL;
		PFNGLGENRENDERBUFFERSEXTPROC glGenRenderbuffersEXT = NULL;
		PFNGLBINDRENDERBUFFEREXTPROC glBindRenderbufferEXT = NULL;
		PFNGLRENDERBUFFERSTORAGEEXTPROC glRenderbufferStorageEXT = NULL;
		PFNGLDELETERENDERBUFFERSEXTPROC glDeleteRenderbuffersEXT = NULL;
	} // namespace render;
} // namespace sandbox;

/*
===============================================================================
	Private prototypes.
===============================================================================
*/
static void set_pixel_format();
static void create_context();
static void retrieve_function_pointers();

/*
===============================================================================
	GlContext implementation.
===============================================================================
*/

/*
===========================================================================
GlContext::Create
	Readies OpenGL for use in the application.
===========================================================================
*/
void GlContext::Create( Window *pWindow )
{
	_pWindow = pWindow;

	// Create the device context.
	HWND hWnd = (HWND)_pWindow->GetHandle();
	gDeviceContext = GetDC( hWnd );

	set_pixel_format();
	create_context();
	retrieve_function_pointers();

	glClearColor( 0.0f, 0.0, 0.0f, 1.0f );
}

/*
===========================================================================
GlContext::Destroy
	Destroys the active OpenGL context.
===========================================================================
*/
void GlContext::Destroy()
{
	// Set no OpenGL context to be active.
	wglMakeCurrent( gDeviceContext, NULL );

	HWND hWnd = (HWND)_pWindow->GetHandle();

	// Clean resources.
	wglDeleteContext( gRenderContext );
	ReleaseDC( hWnd, gDeviceContext );
	DeleteDC( gDeviceContext );
}

/*
===========================================================================
GlContext::Present
	Swaps the front and back buffers of the given window.
===========================================================================
*/
void GlContext::Present()
{
	SwapBuffers( gDeviceContext );

	// Redraw the entire window.
	HWND hWnd = (HWND)_pWindow->GetHandle();
	ValidateRect( hWnd, NULL );
}

/*
===========================================================================
	Private code
===========================================================================
*/

/*
===========================================================================
set_pixel_format
	Sets the backbuffer pixel format.
===========================================================================
*/
static void set_pixel_format()
{
	PIXELFORMATDESCRIPTOR pfd;
	int format;

	ZeroMemory( &pfd, sizeof(pfd) );

	/* Set the pixel format for the DC */
	pfd.nSize = sizeof( pfd );
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cDepthBits = 24;
	pfd.cStencilBits = 8;
	pfd.iLayerType = PFD_MAIN_PLANE;

	format = ChoosePixelFormat( gDeviceContext, &pfd );

	BOOL success = SetPixelFormat( gDeviceContext, format, &pfd );
	sbAssert( success );
}

/*
===========================================================================
create_context
	Creates an OpenGL context (used to issue OpenGL commands).
===========================================================================
*/
static void create_context()
{
	gRenderContext = wglCreateContext( gDeviceContext );
	wglMakeCurrent( gDeviceContext, gRenderContext );
}

/*
===========================================================================
retrieve_function_pointers
	Initializes all OpenGL functions.
===========================================================================
*/
static void retrieve_function_pointers()
{
	glActiveTexture = (PFNGLACTIVETEXTUREPROC)wglGetProcAddress( "glActiveTexture" );
	sbError( glActiveTexture, "Failed to load \"glActiveTexture\"" );

	glAttachShader = (PFNGLATTACHSHADERPROC)wglGetProcAddress( "glAttachShader" );
	sbError( glAttachShader, "Failed to load \"glAttachShader\"" );

	glBeginQuery = (PFNGLBEGINQUERYPROC)wglGetProcAddress( "glBeginQuery" );
	sbError( glBeginQuery, "Failed to load \"glBeginQuery\"" );

	glBindAttribLocation = (PFNGLBINDATTRIBLOCATIONPROC)wglGetProcAddress( "glBindAttribLocation" );
	sbError( glBindAttribLocation, "Failed to load \"glBindAttribLocation\"" );

	glBindBuffer = (PFNGLBINDBUFFERPROC)wglGetProcAddress( "glBindBuffer" );
	sbError( glBindBuffer, "Failed to load \"glBindBuffer\"" );

	glBlendColor = (PFNGLBLENDCOLORPROC)wglGetProcAddress( "glBlendColor" );
	sbError( glBlendColor, "Failed to load \"glBlendColor\"" );

	glBlendEquation = (PFNGLBLENDEQUATIONPROC)wglGetProcAddress( "glBlendEquation" );
	sbError( glBlendEquation, "Failed to load \"glBlendEquation\"" );

	glBlendEquationSeparate = (PFNGLBLENDEQUATIONSEPARATEPROC)wglGetProcAddress( "glBlendEquationSeparate" );
	sbError( glBlendEquationSeparate, "Failed to load \"glBlendEquationSeparate\"" );

	glBlendFuncSeparate = (PFNGLBLENDFUNCSEPARATEPROC)wglGetProcAddress( "glBlendFuncSeparate" );
	sbError( glBlendFuncSeparate, "Failed to load \"glBlendFuncSeparate\"" );

	glBufferData = (PFNGLBUFFERDATAPROC)wglGetProcAddress( "glBufferData" );
	sbError( glBufferData, "Failed to load \"glBufferData\"" );

	glBufferSubData = (PFNGLBUFFERSUBDATAPROC)wglGetProcAddress( "glBufferSubData" );
	sbError( glBufferSubData, "Failed to load \"glBufferSubData\"" );

	glClientActiveTexture = (PFNGLCLIENTACTIVETEXTUREPROC)wglGetProcAddress( "glClientActiveTexture" );
	sbError( glClientActiveTexture, "Failed to load \"glClientActiveTexture\"" );

	glCompileShader = (PFNGLCOMPILESHADERPROC)wglGetProcAddress( "glCompileShader" );
	sbError( glCompileShader, "Failed to load \"glCompileShader\"" );

	glCompressedTexImage1D = (PFNGLCOMPRESSEDTEXIMAGE1DPROC)wglGetProcAddress( "glCompressedTexImage1D" );
	sbError( glCompressedTexImage1D, "Failed to load \"glCompressedTexImage1D\"" );

	glCompressedTexImage2D = (PFNGLCOMPRESSEDTEXIMAGE2DPROC)wglGetProcAddress( "glCompressedTexImage2D" );
	sbError( glCompressedTexImage2D, "Failed to load \"glCompressedTexImage2D\"" );

	glCompressedTexImage3D = (PFNGLCOMPRESSEDTEXIMAGE3DPROC)wglGetProcAddress( "glCompressedTexImage3D" );
	sbError( glCompressedTexImage3D, "Failed to load \"glCompressedTexImage3D\"" );

	glCompressedTexSubImage1D = (PFNGLCOMPRESSEDTEXSUBIMAGE1DPROC)wglGetProcAddress( "glCompressedTexSubImage1D" );
	sbError( glCompressedTexSubImage1D, "Failed to load \"glCompressedTexSubImage1D\"" );

	glCompressedTexSubImage2D = (PFNGLCOMPRESSEDTEXSUBIMAGE2DPROC)wglGetProcAddress( "glCompressedTexSubImage2D" );
	sbError( glCompressedTexSubImage2D, "Failed to load \"glCompressedTexSubImage2D\"" );

	glCompressedTexSubImage3D = (PFNGLCOMPRESSEDTEXSUBIMAGE3DPROC)wglGetProcAddress( "glCompressedTexSubImage3D" );
	sbError( glCompressedTexSubImage3D, "Failed to load \"glCompressedTexSubImage3D\"" );

	glCopyTexSubImage3D = (PFNGLCOPYTEXSUBIMAGE3DPROC)wglGetProcAddress( "glCopyTexSubImage3D" );
	sbError( glCopyTexSubImage3D, "Failed to load \"glCopyTexSubImage3D\"" );

	glCreateProgram = (PFNGLCREATEPROGRAMPROC)wglGetProcAddress( "glCreateProgram" );
	sbError( glCreateProgram, "Failed to load \"glCreateProgram\"" );

	glCreateShader = (PFNGLCREATESHADERPROC)wglGetProcAddress( "glCreateShader" );
	sbError( glCreateShader, "Failed to load \"glCreateShader\"" );

	glDeleteBuffers = (PFNGLDELETEBUFFERSPROC)wglGetProcAddress( "glDeleteBuffers" );
	sbError( glDeleteBuffers, "Failed to load \"glDeleteBuffers\"" );

	glDeleteProgram = (PFNGLDELETEPROGRAMPROC)wglGetProcAddress( "glDeleteProgram" );
	sbError( glDeleteProgram, "Failed to load \"glDeleteProgram\"" );

	glDeleteQueries = (PFNGLDELETEQUERIESPROC)wglGetProcAddress( "glDeleteQueries" );
	sbError( glDeleteQueries, "Failed to load \"glDeleteQueries\"" );

	glDeleteShader = (PFNGLDELETESHADERPROC)wglGetProcAddress( "glDeleteShader" );
	sbError( glDeleteShader, "Failed to load \"glDeleteShader\"" );

	glDetachShader = (PFNGLDETACHSHADERPROC)wglGetProcAddress( "glDetachShader" );
	sbError( glDetachShader, "Failed to load \"glDetachShader\"" );

	glDisableVertexAttribArray = (PFNGLDISABLEVERTEXATTRIBARRAYPROC)wglGetProcAddress( "glDisableVertexAttribArray" );
	sbError( glDisableVertexAttribArray, "Failed to load \"glDisableVertexAttribArray\"" );

	glDrawBuffers = (PFNGLDRAWBUFFERSPROC)wglGetProcAddress( "glDrawBuffers" );
	sbError( glDrawBuffers, "Failed to load \"glDrawBuffers\"" );

	glDrawRangeElements = (PFNGLDRAWRANGEELEMENTSPROC)wglGetProcAddress( "glDrawRangeElements" );
	sbError( glDrawRangeElements, "Failed to load \"glDrawRangeElements\"" );

	glEnableVertexAttribArray = (PFNGLENABLEVERTEXATTRIBARRAYPROC)wglGetProcAddress( "glEnableVertexAttribArray" );
	sbError( glEnableVertexAttribArray, "Failed to load \"glEnableVertexAttribArray\"" );

	glEndQuery = (PFNGLENDQUERYPROC)wglGetProcAddress( "glEndQuery" );
	sbError( glEndQuery, "Failed to load \"glEndQuery\"" );

	glFogCoordd = (PFNGLFOGCOORDDPROC)wglGetProcAddress( "glFogCoordd" );
	sbError( glFogCoordd, "Failed to load \"glFogCoordd\"" );

	glFogCoordf = (PFNGLFOGCOORDFPROC)wglGetProcAddress( "glFogCoordf" );
	sbError( glFogCoordf, "Failed to load \"glFogCoordf\"" );

	glFogCoorddv = (PFNGLFOGCOORDDVPROC)wglGetProcAddress( "glFogCoorddv" );
	sbError( glFogCoorddv, "Failed to load \"glFogCoorddv\"" );

	glFogCoordfv = (PFNGLFOGCOORDFVPROC)wglGetProcAddress( "glFogCoordfv" );
	sbError( glFogCoordfv, "Failed to load \"glFogCoordfv\"" );

	glFogCoordPointer = (PFNGLFOGCOORDPOINTERPROC)wglGetProcAddress( "glFogCoordPointer" );
	sbError( glFogCoordPointer, "Failed to load \"glFogCoordPointer\"" );

	glGenBuffers = (PFNGLGENBUFFERSPROC)wglGetProcAddress( "glGenBuffers" );
	sbError( glGenBuffers, "Failed to load \"glGenBuffers\"" );

	glGenQueries = (PFNGLGENQUERIESPROC)wglGetProcAddress( "glGenQueries" );
	sbError( glGenQueries, "Failed to load \"glGenQueries\"" );

	glGetActiveAttrib = (PFNGLGETACTIVEATTRIBPROC)wglGetProcAddress( "glGetActiveAttrib" );
	sbError( glGetActiveAttrib, "Failed to load \"glGetActiveAttrib\"" );

	glGetActiveUniform = (PFNGLGETACTIVEUNIFORMPROC)wglGetProcAddress( "glGetActiveUniform" );
	sbError( glGetActiveUniform, "Failed to load \"glGetActiveUniform\"" );

	glGetAttachedShaders = (PFNGLGETATTACHEDSHADERSPROC)wglGetProcAddress( "glGetAttachedShaders" );
	sbError( glGetAttachedShaders, "Failed to load \"glGetAttachedShaders\"" );

	glGetAttribLocation = (PFNGLGETATTRIBLOCATIONPROC)wglGetProcAddress( "glGetAttribLocation" );
	sbError( glGetAttribLocation, "Failed to load \"glGetAttribLocation\"" );

	glGetBufferParameteriv = (PFNGLGETBUFFERPARAMETERIVPROC)wglGetProcAddress( "glGetBufferParameteriv" );
	sbError( glGetBufferParameteriv, "Failed to load \"glGetBufferParameteriv\"" );

	glGetBufferPointerv = (PFNGLGETBUFFERPOINTERVPROC)wglGetProcAddress( "glGetBufferPointerv" );
	sbError( glGetBufferPointerv, "Failed to load \"glGetBufferPointerv\"" );

	glGetBufferSubData = (PFNGLGETBUFFERSUBDATAPROC)wglGetProcAddress( "glGetBufferSubData" );
	sbError( glGetBufferSubData, "Failed to load \"glGetBufferSubData\"" );

	glGetCompressedTexImage = (PFNGLGETCOMPRESSEDTEXIMAGEPROC)wglGetProcAddress( "glGetCompressedTexImage" );
	sbError( glGetCompressedTexImage, "Failed to load \"glGetCompressedTexImage\"" );

	glGetProgramiv = (PFNGLGETPROGRAMIVPROC)wglGetProcAddress( "glGetProgramiv" );
	sbError( glGetProgramiv, "Failed to load \"glGetProgramiv\"" );

	glGetProgramInfoLog = (PFNGLGETPROGRAMINFOLOGPROC)wglGetProcAddress( "glGetProgramInfoLog" );
	sbError( glGetProgramInfoLog, "Failed to load \"glGetProgramInfoLog\"" );

	glGetQueryObjectiv = (PFNGLGETQUERYOBJECTIVPROC)wglGetProcAddress( "glGetQueryObjectiv" );
	sbError( glGetQueryObjectiv, "Failed to load \"glGetQueryObjectiv\"" );

	glGetQueryObjectuiv = (PFNGLGETQUERYOBJECTUIVPROC)wglGetProcAddress( "glGetQueryObjectuiv" );
	sbError( glGetQueryObjectuiv, "Failed to load \"glGetQueryObjectuiv\"" );

	glGetQueryiv = (PFNGLGETQUERYIVPROC)wglGetProcAddress( "glGetQueryiv" );
	sbError( glGetQueryiv, "Failed to load \"glGetQueryiv\"" );

	glGetShaderiv = (PFNGLGETSHADERIVPROC)wglGetProcAddress( "glGetShaderiv" );
	sbError( glGetShaderiv, "Failed to load \"glGetShaderiv\"" );

	glGetShaderInfoLog = (PFNGLGETSHADERINFOLOGPROC)wglGetProcAddress( "glGetShaderInfoLog" );
	sbError( glGetShaderInfoLog, "Failed to load \"glGetShaderInfoLog\"" );

	glGetShaderSource = (PFNGLGETSHADERSOURCEPROC)wglGetProcAddress( "glGetShaderSource" );
	sbError( glGetShaderSource, "Failed to load \"glGetShaderSource\"" );

	glGetUniformfv = (PFNGLGETUNIFORMFVPROC)wglGetProcAddress( "glGetUniformfv" );
	sbError( glGetUniformfv, "Failed to load \"glGetUniformfv\"" );

	glGetUniformiv = (PFNGLGETUNIFORMIVPROC)wglGetProcAddress( "glGetUniformiv" );
	sbError( glGetUniformiv, "Failed to load \"glGetUniformiv\"" );

	glGetUniformLocation = (PFNGLGETUNIFORMLOCATIONPROC)wglGetProcAddress( "glGetUniformLocation" );
	sbError( glGetUniformLocation, "Failed to load \"glGetUniformLocation\"" );

	glGetVertexAttribdv = (PFNGLGETVERTEXATTRIBDVPROC)wglGetProcAddress( "glGetVertexAttribdv" );
	sbError( glGetVertexAttribdv, "Failed to load \"glGetVertexAttribdv\"" );

	glGetVertexAttribfv = (PFNGLGETVERTEXATTRIBFVPROC)wglGetProcAddress( "glGetVertexAttribfv" );
	sbError( glGetVertexAttribfv, "Failed to load \"glGetVertexAttribfv\"" );

	glGetVertexAttribiv = (PFNGLGETVERTEXATTRIBIVPROC)wglGetProcAddress( "glGetVertexAttribiv" );
	sbError( glGetVertexAttribiv, "Failed to load \"glGetVertexAttribiv\"" );

	glGetVertexAttribPointerv = (PFNGLGETVERTEXATTRIBPOINTERVPROC)wglGetProcAddress( "glGetVertexAttribPointerv" );
	sbError( glGetVertexAttribPointerv, "Failed to load \"glGetVertexAttribPointerv\"" );

	glIsBuffer = (PFNGLISBUFFERPROC)wglGetProcAddress( "glIsBuffer" );
	sbError( glIsBuffer, "Failed to load \"glIsBuffer\"" );

	glIsProgram = (PFNGLISPROGRAMPROC)wglGetProcAddress( "glIsProgram" );
	sbError( glIsProgram, "Failed to load \"glIsProgram\"" );

	glIsQuery = (PFNGLISQUERYPROC)wglGetProcAddress( "glIsQuery" );
	sbError( glIsQuery, "Failed to load \"glIsQuery\"" );

	glIsShader = (PFNGLISSHADERPROC)wglGetProcAddress( "glIsShader" );
	sbError( glIsShader, "Failed to load \"glIsShader\"" );

	glLinkProgram = (PFNGLLINKPROGRAMPROC)wglGetProcAddress( "glLinkProgram" );
	sbError( glLinkProgram, "Failed to load \"glLinkProgram\"" );

	glLoadTransposeMatrixd = (PFNGLLOADTRANSPOSEMATRIXDPROC)wglGetProcAddress( "glLoadTransposeMatrixd" );
	sbError( glLoadTransposeMatrixd, "Failed to load \"glLoadTransposeMatrixd\"" );

	glLoadTransposeMatrixf = (PFNGLLOADTRANSPOSEMATRIXFPROC)wglGetProcAddress( "glLoadTransposeMatrixf" );
	sbError( glLoadTransposeMatrixf, "Failed to load \"glLoadTransposeMatrixf\"" );

	glMapBuffer = (PFNGLMAPBUFFERPROC)wglGetProcAddress( "glMapBuffer" );
	sbError( glMapBuffer, "Failed to load \"glMapBuffer\"" );

	glMultTransposeMatrixd = (PFNGLMULTTRANSPOSEMATRIXDPROC)wglGetProcAddress( "glMultTransposeMatrixd" );
	sbError( glMultTransposeMatrixd, "Failed to load \"glMultTransposeMatrixd\"" );

	glMultTransposeMatrixf = (PFNGLMULTTRANSPOSEMATRIXFPROC)wglGetProcAddress( "glMultTransposeMatrixf" );
	sbError( glMultTransposeMatrixf, "Failed to load \"glMultTransposeMatrixf\"" );

	glMultiDrawArrays = (PFNGLMULTIDRAWARRAYSPROC)wglGetProcAddress( "glMultiDrawArrays" );
	sbError( glMultiDrawArrays, "Failed to load \"glMultiDrawArrays\"" );

	glMultiDrawElements = (PFNGLMULTIDRAWELEMENTSPROC)wglGetProcAddress( "glMultiDrawElements" );
	sbError( glMultiDrawElements, "Failed to load \"glMultiDrawElements\"" );

	glMultiTexCoord1s = (PFNGLMULTITEXCOORD1SPROC)wglGetProcAddress( "glMultiTexCoord1s" );
	sbError( glMultiTexCoord1s, "Failed to load \"glMultiTexCoord1s\"" );

	glMultiTexCoord1i = (PFNGLMULTITEXCOORD1IPROC)wglGetProcAddress( "glMultiTexCoord1i" );
	sbError( glMultiTexCoord1i, "Failed to load \"glMultiTexCoord1i\"" );

	glMultiTexCoord1f = (PFNGLMULTITEXCOORD1FPROC)wglGetProcAddress( "glMultiTexCoord1f" );
	sbError( glMultiTexCoord1f, "Failed to load \"glMultiTexCoord1f\"" );

	glMultiTexCoord1d = (PFNGLMULTITEXCOORD1DPROC)wglGetProcAddress( "glMultiTexCoord1d" );
	sbError( glMultiTexCoord1d, "Failed to load \"glMultiTexCoord1d\"" );

	glMultiTexCoord2s = (PFNGLMULTITEXCOORD2SPROC)wglGetProcAddress( "glMultiTexCoord2s" );
	sbError( glMultiTexCoord2s, "Failed to load \"glMultiTexCoord2s\"" );

	glMultiTexCoord2i = (PFNGLMULTITEXCOORD2IPROC)wglGetProcAddress( "glMultiTexCoord2i" );
	sbError( glMultiTexCoord2i, "Failed to load \"glMultiTexCoord2i\"" );

	glMultiTexCoord2f = (PFNGLMULTITEXCOORD2FPROC)wglGetProcAddress( "glMultiTexCoord2f" );
	sbError( glMultiTexCoord2f, "Failed to load \"glMultiTexCoord2f\"" );

	glMultiTexCoord2d = (PFNGLMULTITEXCOORD2DPROC)wglGetProcAddress( "glMultiTexCoord2d" );
	sbError( glMultiTexCoord2d, "Failed to load \"glMultiTexCoord2d\"" );

	glMultiTexCoord3s = (PFNGLMULTITEXCOORD3SPROC)wglGetProcAddress( "glMultiTexCoord3s" );
	sbError( glMultiTexCoord3s, "Failed to load \"glMultiTexCoord3s\"" );

	glMultiTexCoord3i = (PFNGLMULTITEXCOORD3IPROC)wglGetProcAddress( "glMultiTexCoord3i" );
	sbError( glMultiTexCoord3i, "Failed to load \"glMultiTexCoord3i\"" );

	glMultiTexCoord3f = (PFNGLMULTITEXCOORD3FPROC)wglGetProcAddress( "glMultiTexCoord3f" );
	sbError( glMultiTexCoord3f, "Failed to load \"glMultiTexCoord3f\"" );

	glMultiTexCoord3d = (PFNGLMULTITEXCOORD3DPROC)wglGetProcAddress( "glMultiTexCoord3d" );
	sbError( glMultiTexCoord3d, "Failed to load \"glMultiTexCoord3d\"" );

	glMultiTexCoord4s = (PFNGLMULTITEXCOORD4SPROC)wglGetProcAddress( "glMultiTexCoord4s" );
	sbError( glMultiTexCoord4s, "Failed to load \"glMultiTexCoord4s\"" );

	glMultiTexCoord4i = (PFNGLMULTITEXCOORD4IPROC)wglGetProcAddress( "glMultiTexCoord4i" );
	sbError( glMultiTexCoord4i, "Failed to load \"glMultiTexCoord4i\"" );

	glMultiTexCoord4f = (PFNGLMULTITEXCOORD4FPROC)wglGetProcAddress( "glMultiTexCoord4f" );
	sbError( glMultiTexCoord4f, "Failed to load \"glMultiTexCoord4f\"" );

	glMultiTexCoord4d = (PFNGLMULTITEXCOORD4DPROC)wglGetProcAddress( "glMultiTexCoord4d" );
	sbError( glMultiTexCoord4d, "Failed to load \"glMultiTexCoord4d\"" );

	glMultiTexCoord1sv = (PFNGLMULTITEXCOORD1SVPROC)wglGetProcAddress( "glMultiTexCoord1sv" );
	sbError( glMultiTexCoord1sv, "Failed to load \"glMultiTexCoord1sv\"" );

	glMultiTexCoord1iv = (PFNGLMULTITEXCOORD1IVPROC)wglGetProcAddress( "glMultiTexCoord1iv" );
	sbError( glMultiTexCoord1iv, "Failed to load \"glMultiTexCoord1iv\"" );

	glMultiTexCoord1fv = (PFNGLMULTITEXCOORD1FVPROC)wglGetProcAddress( "glMultiTexCoord1fv" );
	sbError( glMultiTexCoord1fv, "Failed to load \"glMultiTexCoord1fv\"" );

	glMultiTexCoord1dv = (PFNGLMULTITEXCOORD1DVPROC)wglGetProcAddress( "glMultiTexCoord1dv" );
	sbError( glMultiTexCoord1dv, "Failed to load \"glMultiTexCoord1dv\"" );

	glMultiTexCoord2sv = (PFNGLMULTITEXCOORD2SVPROC)wglGetProcAddress( "glMultiTexCoord2sv" );
	sbError( glMultiTexCoord2sv, "Failed to load \"glMultiTexCoord2sv\"" );

	glMultiTexCoord2iv = (PFNGLMULTITEXCOORD2IVPROC)wglGetProcAddress( "glMultiTexCoord2iv" );
	sbError( glMultiTexCoord2iv, "Failed to load \"glMultiTexCoord2iv\"" );

	glMultiTexCoord2fv = (PFNGLMULTITEXCOORD2FVPROC)wglGetProcAddress( "glMultiTexCoord2fv" );
	sbError( glMultiTexCoord2fv, "Failed to load \"glMultiTexCoord2fv\"" );

	glMultiTexCoord2dv = (PFNGLMULTITEXCOORD2DVPROC)wglGetProcAddress( "glMultiTexCoord2dv" );
	sbError( glMultiTexCoord2dv, "Failed to load \"glMultiTexCoord2dv\"" );

	glMultiTexCoord3sv = (PFNGLMULTITEXCOORD3SVPROC)wglGetProcAddress( "glMultiTexCoord3sv" );
	sbError( glMultiTexCoord3sv, "Failed to load \"glMultiTexCoord3sv\"" );

	glMultiTexCoord3iv = (PFNGLMULTITEXCOORD3IVPROC)wglGetProcAddress( "glMultiTexCoord3iv" );
	sbError( glMultiTexCoord3iv, "Failed to load \"glMultiTexCoord3iv\"" );

	glMultiTexCoord3fv = (PFNGLMULTITEXCOORD3FVPROC)wglGetProcAddress( "glMultiTexCoord3fv" );
	sbError( glMultiTexCoord3fv, "Failed to load \"glMultiTexCoord3fv\"" );

	glMultiTexCoord3dv = (PFNGLMULTITEXCOORD3DVPROC)wglGetProcAddress( "glMultiTexCoord3dv" );
	sbError( glMultiTexCoord3dv, "Failed to load \"glMultiTexCoord3dv\"" );

	glMultiTexCoord4sv = (PFNGLMULTITEXCOORD4SVPROC)wglGetProcAddress( "glMultiTexCoord4sv" );
	sbError( glMultiTexCoord4sv, "Failed to load \"glMultiTexCoord4sv\"" );

	glMultiTexCoord4iv = (PFNGLMULTITEXCOORD4IVPROC)wglGetProcAddress( "glMultiTexCoord4iv" );
	sbError( glMultiTexCoord4iv, "Failed to load \"glMultiTexCoord4iv\"" );

	glMultiTexCoord4fv = (PFNGLMULTITEXCOORD4FVPROC)wglGetProcAddress( "glMultiTexCoord4fv" );
	sbError( glMultiTexCoord4fv, "Failed to load \"glMultiTexCoord4fv\"" );

	glMultiTexCoord4dv = (PFNGLMULTITEXCOORD4DVPROC)wglGetProcAddress( "glMultiTexCoord4dv" );
	sbError( glMultiTexCoord4dv, "Failed to load \"glMultiTexCoord4dv\"" );

	glPointParameterf = (PFNGLPOINTPARAMETERFPROC)wglGetProcAddress( "glPointParameterf" );
	sbError( glPointParameterf, "Failed to load \"glPointParameterf\"" );

	glPointParameteri = (PFNGLPOINTPARAMETERIPROC)wglGetProcAddress( "glPointParameteri" );
	sbError( glPointParameteri, "Failed to load \"glPointParameteri\"" );

	glPointParameterfv = (PFNGLPOINTPARAMETERFVPROC)wglGetProcAddress( "glPointParameterfv" );
	sbError( glPointParameterfv, "Failed to load \"glPointParameterfv\"" );

	glPointParameteriv = (PFNGLPOINTPARAMETERIVPROC)wglGetProcAddress( "glPointParameteriv" );
	sbError( glPointParameteriv, "Failed to load \"glPointParameteriv\"" );

	glSampleCoverage = (PFNGLSAMPLECOVERAGEPROC)wglGetProcAddress( "glSampleCoverage" );
	sbError( glSampleCoverage, "Failed to load \"glSampleCoverage\"" );

	glSecondaryColor3b = (PFNGLSECONDARYCOLOR3BPROC)wglGetProcAddress( "glSecondaryColor3b" );
	sbError( glSecondaryColor3b, "Failed to load \"glSecondaryColor3b\"" );

	glSecondaryColor3s = (PFNGLSECONDARYCOLOR3SPROC)wglGetProcAddress( "glSecondaryColor3s" );
	sbError( glSecondaryColor3s, "Failed to load \"glSecondaryColor3s\"" );

	glSecondaryColor3i = (PFNGLSECONDARYCOLOR3IPROC)wglGetProcAddress( "glSecondaryColor3i" );
	sbError( glSecondaryColor3i, "Failed to load \"glSecondaryColor3i\"" );

	glSecondaryColor3f = (PFNGLSECONDARYCOLOR3FPROC)wglGetProcAddress( "glSecondaryColor3f" );
	sbError( glSecondaryColor3f, "Failed to load \"glSecondaryColor3f\"" );

	glSecondaryColor3d = (PFNGLSECONDARYCOLOR3DPROC)wglGetProcAddress( "glSecondaryColor3d" );
	sbError( glSecondaryColor3d, "Failed to load \"glSecondaryColor3d\"" );

	glSecondaryColor3ub = (PFNGLSECONDARYCOLOR3UBPROC)wglGetProcAddress( "glSecondaryColor3ub" );
	sbError( glSecondaryColor3ub, "Failed to load \"glSecondaryColor3ub\"" );

	glSecondaryColor3us = (PFNGLSECONDARYCOLOR3USPROC)wglGetProcAddress( "glSecondaryColor3us" );
	sbError( glSecondaryColor3us, "Failed to load \"glSecondaryColor3us\"" );

	glSecondaryColor3ui = (PFNGLSECONDARYCOLOR3UIPROC)wglGetProcAddress( "glSecondaryColor3ui" );
	sbError( glSecondaryColor3ui, "Failed to load \"glSecondaryColor3ui\"" );

	glSecondaryColor3bv = (PFNGLSECONDARYCOLOR3BVPROC)wglGetProcAddress( "glSecondaryColor3bv" );
	sbError( glSecondaryColor3bv, "Failed to load \"glSecondaryColor3bv\"" );

	glSecondaryColor3sv = (PFNGLSECONDARYCOLOR3SVPROC)wglGetProcAddress( "glSecondaryColor3sv" );
	sbError( glSecondaryColor3sv, "Failed to load \"glSecondaryColor3sv\"" );

	glSecondaryColor3iv = (PFNGLSECONDARYCOLOR3IVPROC)wglGetProcAddress( "glSecondaryColor3iv" );
	sbError( glSecondaryColor3iv, "Failed to load \"glSecondaryColor3iv\"" );

	glSecondaryColor3fv = (PFNGLSECONDARYCOLOR3FVPROC)wglGetProcAddress( "glSecondaryColor3fv" );
	sbError( glSecondaryColor3fv, "Failed to load \"glSecondaryColor3fv\"" );

	glSecondaryColor3dv = (PFNGLSECONDARYCOLOR3DVPROC)wglGetProcAddress( "glSecondaryColor3dv" );
	sbError( glSecondaryColor3dv, "Failed to load \"glSecondaryColor3dv\"" );

	glSecondaryColor3ubv = (PFNGLSECONDARYCOLOR3UBVPROC)wglGetProcAddress( "glSecondaryColor3ubv" );
	sbError( glSecondaryColor3ubv, "Failed to load \"glSecondaryColor3ubv\"" );

	glSecondaryColor3usv = (PFNGLSECONDARYCOLOR3USVPROC)wglGetProcAddress( "glSecondaryColor3usv" );
	sbError( glSecondaryColor3usv, "Failed to load \"glSecondaryColor3usv\"" );

	glSecondaryColor3uiv = (PFNGLSECONDARYCOLOR3UIVPROC)wglGetProcAddress( "glSecondaryColor3uiv" );
	sbError( glSecondaryColor3uiv, "Failed to load \"glSecondaryColor3uiv\"" );

	glSecondaryColorPointer = (PFNGLSECONDARYCOLORPOINTERPROC)wglGetProcAddress( "glSecondaryColorPointer" );
	sbError( glSecondaryColorPointer, "Failed to load \"glSecondaryColorPointer\"" );

	glShaderSource = (PFNGLSHADERSOURCEPROC)wglGetProcAddress( "glShaderSource" );
	sbError( glShaderSource, "Failed to load \"glShaderSource\"" );

	glStencilFuncSeparate = (PFNGLSTENCILFUNCSEPARATEPROC)wglGetProcAddress( "glStencilFuncSeparate" );
	sbError( glStencilFuncSeparate, "Failed to load \"glStencilFuncSeparate\"" );

	glStencilMaskSeparate = (PFNGLSTENCILMASKSEPARATEPROC)wglGetProcAddress( "glStencilMaskSeparate" );
	sbError( glStencilMaskSeparate, "Failed to load \"glStencilMaskSeparate\"" );

	glStencilOpSeparate = (PFNGLSTENCILOPSEPARATEPROC)wglGetProcAddress( "glStencilOpSeparate" );
	sbError( glStencilOpSeparate, "Failed to load \"glStencilOpSeparate\"" );

	glTexImage3D = (PFNGLTEXIMAGE3DPROC)wglGetProcAddress( "glTexImage3D" );
	sbError( glTexImage3D, "Failed to load \"glTexImage3D\"" );

	glTexSubImage3D = (PFNGLTEXSUBIMAGE3DPROC)wglGetProcAddress( "glTexSubImage3D" );
	sbError( glTexSubImage3D, "Failed to load \"glTexSubImage3D\"" );

	glUniform1f = (PFNGLUNIFORM1FPROC)wglGetProcAddress( "glUniform1f" );
	sbError( glUniform1f, "Failed to load \"glUniform1f\"" );

	glUniform1i = (PFNGLUNIFORM1IPROC)wglGetProcAddress( "glUniform1i" );
	sbError( glUniform1i, "Failed to load \"glUniform1i\"" );

	glUniform2f = (PFNGLUNIFORM2FPROC)wglGetProcAddress( "glUniform2f" );
	sbError( glUniform2f, "Failed to load \"glUniform2f\"" );

	glUniform2i = (PFNGLUNIFORM2IPROC)wglGetProcAddress( "glUniform2i" );
	sbError( glUniform2i, "Failed to load \"glUniform2i\"" );

	glUniform3f = (PFNGLUNIFORM3FPROC)wglGetProcAddress( "glUniform3f" );
	sbError( glUniform3f, "Failed to load \"glUniform3f\"" );

	glUniform3i = (PFNGLUNIFORM3IPROC)wglGetProcAddress( "glUniform3i" );
	sbError( glUniform3i, "Failed to load \"glUniform3i\"" );

	glUniform4f = (PFNGLUNIFORM4FPROC)wglGetProcAddress( "glUniform4f" );
	sbError( glUniform4f, "Failed to load \"glUniform4f\"" );

	glUniform4i = (PFNGLUNIFORM4IPROC)wglGetProcAddress( "glUniform4i" );
	sbError( glUniform4i, "Failed to load \"glUniform4i\"" );

	glUniform1fv = (PFNGLUNIFORM1FVPROC)wglGetProcAddress( "glUniform1fv" );
	sbError( glUniform1fv, "Failed to load \"glUniform1fv\"" );

	glUniform1iv = (PFNGLUNIFORM1IVPROC)wglGetProcAddress( "glUniform1iv" );
	sbError( glUniform1iv, "Failed to load \"glUniform1iv\"" );

	glUniform2fv = (PFNGLUNIFORM2FVPROC)wglGetProcAddress( "glUniform2fv" );
	sbError( glUniform2fv, "Failed to load \"glUniform2fv\"" );

	glUniform2iv = (PFNGLUNIFORM2IVPROC)wglGetProcAddress( "glUniform2iv" );
	sbError( glUniform2iv, "Failed to load \"glUniform2iv\"" );

	glUniform3fv = (PFNGLUNIFORM3FVPROC)wglGetProcAddress( "glUniform3fv" );
	sbError( glUniform3fv, "Failed to load \"glUniform3fv\"" );

	glUniform3iv = (PFNGLUNIFORM3IVPROC)wglGetProcAddress( "glUniform3iv" );
	sbError( glUniform3iv, "Failed to load \"glUniform3iv\"" );

	glUniform4fv = (PFNGLUNIFORM4FVPROC)wglGetProcAddress( "glUniform4fv" );
	sbError( glUniform4fv, "Failed to load \"glUniform4fv\"" );

	glUniform4iv = (PFNGLUNIFORM4IVPROC)wglGetProcAddress( "glUniform4iv" );
	sbError( glUniform4iv, "Failed to load \"glUniform4iv\"" );

	glUniformMatrix2fv = (PFNGLUNIFORMMATRIX2FVPROC)wglGetProcAddress( "glUniformMatrix2fv" );
	sbError( glUniformMatrix2fv, "Failed to load \"glUniformMatrix2fv\"" );

	glUniformMatrix3fv = (PFNGLUNIFORMMATRIX3FVPROC)wglGetProcAddress( "glUniformMatrix3fv" );
	sbError( glUniformMatrix3fv, "Failed to load \"glUniformMatrix3fv\"" );

	glUniformMatrix4fv = (PFNGLUNIFORMMATRIX4FVPROC)wglGetProcAddress( "glUniformMatrix4fv" );
	sbError( glUniformMatrix4fv, "Failed to load \"glUniformMatrix4fv\"" );

	glUniformMatrix2x3fv = (PFNGLUNIFORMMATRIX2X3FVPROC)wglGetProcAddress( "glUniformMatrix2x3fv" );
	sbError( glUniformMatrix2x3fv, "Failed to load \"glUniformMatrix2x3fv\"" );

	glUniformMatrix3x2fv = (PFNGLUNIFORMMATRIX3X2FVPROC)wglGetProcAddress( "glUniformMatrix3x2fv" );
	sbError( glUniformMatrix3x2fv, "Failed to load \"glUniformMatrix3x2fv\"" );

	glUniformMatrix2x4fv = (PFNGLUNIFORMMATRIX2X4FVPROC)wglGetProcAddress( "glUniformMatrix2x4fv" );
	sbError( glUniformMatrix2x4fv, "Failed to load \"glUniformMatrix2x4fv\"" );

	glUniformMatrix4x2fv = (PFNGLUNIFORMMATRIX4X2FVPROC)wglGetProcAddress( "glUniformMatrix4x2fv" );
	sbError( glUniformMatrix4x2fv, "Failed to load \"glUniformMatrix4x2fv\"" );

	glUniformMatrix3x4fv = (PFNGLUNIFORMMATRIX3X4FVPROC)wglGetProcAddress( "glUniformMatrix3x4fv" );
	sbError( glUniformMatrix3x4fv, "Failed to load \"glUniformMatrix3x4fv\"" );

	glUniformMatrix4x3fv = (PFNGLUNIFORMMATRIX4X3FVPROC)wglGetProcAddress( "glUniformMatrix4x3fv" );
	sbError( glUniformMatrix4x3fv, "Failed to load \"glUniformMatrix4x3fv\"" );

	glUnmapBuffer = (PFNGLUNMAPBUFFERPROC)wglGetProcAddress( "glUnmapBuffer" );
	sbError( glUnmapBuffer, "Failed to load \"glUnmapBuffer\"" );

	glUseProgram = (PFNGLUSEPROGRAMPROC)wglGetProcAddress( "glUseProgram" );
	sbError( glUseProgram, "Failed to load \"glUseProgram\"" );

	glValidateProgram = (PFNGLVALIDATEPROGRAMPROC)wglGetProcAddress( "glValidateProgram" );
	sbError( glValidateProgram, "Failed to load \"glValidateProgram\"" );

	glVertexAttrib1f = (PFNGLVERTEXATTRIB1FPROC)wglGetProcAddress( "glVertexAttrib1f" );
	sbError( glVertexAttrib1f, "Failed to load \"glVertexAttrib1f\"" );

	glVertexAttrib1s = (PFNGLVERTEXATTRIB1SPROC)wglGetProcAddress( "glVertexAttrib1s" );
	sbError( glVertexAttrib1s, "Failed to load \"glVertexAttrib1s\"" );

	glVertexAttrib1d = (PFNGLVERTEXATTRIB1DPROC)wglGetProcAddress( "glVertexAttrib1d" );
	sbError( glVertexAttrib1d, "Failed to load \"glVertexAttrib1d\"" );

	glVertexAttrib2f = (PFNGLVERTEXATTRIB2FPROC)wglGetProcAddress( "glVertexAttrib2f" );
	sbError( glVertexAttrib2f, "Failed to load \"glVertexAttrib2f\"" );

	glVertexAttrib2s = (PFNGLVERTEXATTRIB2SPROC)wglGetProcAddress( "glVertexAttrib2s" );
	sbError( glVertexAttrib2s, "Failed to load \"glVertexAttrib2s\"" );

	glVertexAttrib2d = (PFNGLVERTEXATTRIB2DPROC)wglGetProcAddress( "glVertexAttrib2d" );
	sbError( glVertexAttrib2d, "Failed to load \"glVertexAttrib2d\"" );

	glVertexAttrib3f = (PFNGLVERTEXATTRIB3FPROC)wglGetProcAddress( "glVertexAttrib3f" );
	sbError( glVertexAttrib3f, "Failed to load \"glVertexAttrib3f\"" );

	glVertexAttrib3s = (PFNGLVERTEXATTRIB3SPROC)wglGetProcAddress( "glVertexAttrib3s" );
	sbError( glVertexAttrib3s, "Failed to load \"glVertexAttrib3s\"" );

	glVertexAttrib3d = (PFNGLVERTEXATTRIB3DPROC)wglGetProcAddress( "glVertexAttrib3d" );
	sbError( glVertexAttrib3d, "Failed to load \"glVertexAttrib3d\"" );

	glVertexAttrib4f = (PFNGLVERTEXATTRIB4FPROC)wglGetProcAddress( "glVertexAttrib4f" );
	sbError( glVertexAttrib4f, "Failed to load \"glVertexAttrib4f\"" );

	glVertexAttrib4s = (PFNGLVERTEXATTRIB4SPROC)wglGetProcAddress( "glVertexAttrib4s" );
	sbError( glVertexAttrib4s, "Failed to load \"glVertexAttrib4s\"" );

	glVertexAttrib4d = (PFNGLVERTEXATTRIB4DPROC)wglGetProcAddress( "glVertexAttrib4d" );
	sbError( glVertexAttrib4d, "Failed to load \"glVertexAttrib4d\"" );

	glVertexAttrib4Nub = (PFNGLVERTEXATTRIB4NUBPROC)wglGetProcAddress( "glVertexAttrib4Nub" );
	sbError( glVertexAttrib4Nub, "Failed to load \"glVertexAttrib4Nub\"" );

	glVertexAttrib1fv = (PFNGLVERTEXATTRIB1FVPROC)wglGetProcAddress( "glVertexAttrib1fv" );
	sbError( glVertexAttrib1fv, "Failed to load \"glVertexAttrib1fv\"" );

	glVertexAttrib1sv = (PFNGLVERTEXATTRIB1SVPROC)wglGetProcAddress( "glVertexAttrib1sv" );
	sbError( glVertexAttrib1sv, "Failed to load \"glVertexAttrib1sv\"" );

	glVertexAttrib1dv = (PFNGLVERTEXATTRIB1DVPROC)wglGetProcAddress( "glVertexAttrib1dv" );
	sbError( glVertexAttrib1dv, "Failed to load \"glVertexAttrib1dv\"" );

	glVertexAttrib2fv = (PFNGLVERTEXATTRIB2FVPROC)wglGetProcAddress( "glVertexAttrib2fv" );
	sbError( glVertexAttrib2fv, "Failed to load \"glVertexAttrib2fv\"" );

	glVertexAttrib2sv = (PFNGLVERTEXATTRIB2SVPROC)wglGetProcAddress( "glVertexAttrib2sv" );
	sbError( glVertexAttrib2sv, "Failed to load \"glVertexAttrib2sv\"" );

	glVertexAttrib2dv = (PFNGLVERTEXATTRIB2DVPROC)wglGetProcAddress( "glVertexAttrib2dv" );
	sbError( glVertexAttrib2dv, "Failed to load \"glVertexAttrib2dv\"" );

	glVertexAttrib3fv = (PFNGLVERTEXATTRIB3FVPROC)wglGetProcAddress( "glVertexAttrib3fv" );
	sbError( glVertexAttrib3fv, "Failed to load \"glVertexAttrib3fv\"" );

	glVertexAttrib3sv = (PFNGLVERTEXATTRIB3SVPROC)wglGetProcAddress( "glVertexAttrib3sv" );
	sbError( glVertexAttrib3sv, "Failed to load \"glVertexAttrib3sv\"" );

	glVertexAttrib3dv = (PFNGLVERTEXATTRIB3DVPROC)wglGetProcAddress( "glVertexAttrib3dv" );
	sbError( glVertexAttrib3dv, "Failed to load \"glVertexAttrib3dv\"" );

	glVertexAttrib4fv = (PFNGLVERTEXATTRIB4FVPROC)wglGetProcAddress( "glVertexAttrib4fv" );
	sbError( glVertexAttrib4fv, "Failed to load \"glVertexAttrib4fv\"" );

	glVertexAttrib4sv = (PFNGLVERTEXATTRIB4SVPROC)wglGetProcAddress( "glVertexAttrib4sv" );
	sbError( glVertexAttrib4sv, "Failed to load \"glVertexAttrib4sv\"" );

	glVertexAttrib4dv = (PFNGLVERTEXATTRIB4DVPROC)wglGetProcAddress( "glVertexAttrib4dv" );
	sbError( glVertexAttrib4dv, "Failed to load \"glVertexAttrib4dv\"" );

	glVertexAttrib4iv = (PFNGLVERTEXATTRIB4IVPROC)wglGetProcAddress( "glVertexAttrib4iv" );
	sbError( glVertexAttrib4iv, "Failed to load \"glVertexAttrib4iv\"" );

	glVertexAttrib4bv = (PFNGLVERTEXATTRIB4BVPROC)wglGetProcAddress( "glVertexAttrib4bv" );
	sbError( glVertexAttrib4bv, "Failed to load \"glVertexAttrib4bv\"" );

	glVertexAttrib4ubv = (PFNGLVERTEXATTRIB4UBVPROC)wglGetProcAddress( "glVertexAttrib4ubv" );
	sbError( glVertexAttrib4ubv, "Failed to load \"glVertexAttrib4ubv\"" );

	glVertexAttrib4usv = (PFNGLVERTEXATTRIB4USVPROC)wglGetProcAddress( "glVertexAttrib4usv" );
	sbError( glVertexAttrib4usv, "Failed to load \"glVertexAttrib4usv\"" );

	glVertexAttrib4uiv = (PFNGLVERTEXATTRIB4UIVPROC)wglGetProcAddress( "glVertexAttrib4uiv" );
	sbError( glVertexAttrib4uiv, "Failed to load \"glVertexAttrib4uiv\"" );

	glVertexAttrib4Nbv = (PFNGLVERTEXATTRIB4NBVPROC)wglGetProcAddress( "glVertexAttrib4Nbv" );
	sbError( glVertexAttrib4Nbv, "Failed to load \"glVertexAttrib4Nbv\"" );

	glVertexAttrib4Nsv = (PFNGLVERTEXATTRIB4NSVPROC)wglGetProcAddress( "glVertexAttrib4Nsv" );
	sbError( glVertexAttrib4Nsv, "Failed to load \"glVertexAttrib4Nsv\"" );

	glVertexAttrib4Niv = (PFNGLVERTEXATTRIB4NIVPROC)wglGetProcAddress( "glVertexAttrib4Niv" );
	sbError( glVertexAttrib4Niv, "Failed to load \"glVertexAttrib4Niv\"" );

	glVertexAttrib4Nubv = (PFNGLVERTEXATTRIB4NUBVPROC)wglGetProcAddress( "glVertexAttrib4Nubv" );
	sbError( glVertexAttrib4Nubv, "Failed to load \"glVertexAttrib4Nubv\"" );

	glVertexAttrib4Nusv = (PFNGLVERTEXATTRIB4NUSVPROC)wglGetProcAddress( "glVertexAttrib4Nusv" );
	sbError( glVertexAttrib4Nusv, "Failed to load \"glVertexAttrib4Nusv\"" );

	glVertexAttrib4Nuiv = (PFNGLVERTEXATTRIB4NUIVPROC)wglGetProcAddress( "glVertexAttrib4Nuiv" );
	sbError( glVertexAttrib4Nuiv, "Failed to load \"glVertexAttrib4Nuiv\"" );

	glVertexAttribPointer = (PFNGLVERTEXATTRIBPOINTERPROC)wglGetProcAddress( "glVertexAttribPointer" );
	sbError( glVertexAttribPointer, "Failed to load \"glVertexAttribPointer\"" );

	glWindowPos2s = (PFNGLWINDOWPOS2SPROC)wglGetProcAddress( "glWindowPos2s" );
	sbError( glWindowPos2s, "Failed to load \"glWindowPos2s\"" );

	glWindowPos2i = (PFNGLWINDOWPOS2IPROC)wglGetProcAddress( "glWindowPos2i" );
	sbError( glWindowPos2i, "Failed to load \"glWindowPos2i\"" );

	glWindowPos2f = (PFNGLWINDOWPOS2FPROC)wglGetProcAddress( "glWindowPos2f" );
	sbError( glWindowPos2f, "Failed to load \"glWindowPos2f\"" );

	glWindowPos2d = (PFNGLWINDOWPOS2DPROC)wglGetProcAddress( "glWindowPos2d" );
	sbError( glWindowPos2d, "Failed to load \"glWindowPos2d\"" );

	glWindowPos3s = (PFNGLWINDOWPOS3SPROC)wglGetProcAddress( "glWindowPos3s" );
	sbError( glWindowPos3s, "Failed to load \"glWindowPos3s\"" );

	glWindowPos3i = (PFNGLWINDOWPOS3IPROC)wglGetProcAddress( "glWindowPos3i" );
	sbError( glWindowPos3i, "Failed to load \"glWindowPos3i\"" );

	glWindowPos3f = (PFNGLWINDOWPOS3FPROC)wglGetProcAddress( "glWindowPos3f" );
	sbError( glWindowPos3f, "Failed to load \"glWindowPos3f\"" );

	glGenFramebuffersEXT = (PFNGLGENFRAMEBUFFERSEXTPROC)wglGetProcAddress( "glGenFramebuffersEXT" );
	sbError( glGenFramebuffersEXT, "Failed to load \"glGenFramebuffersEXT\"" );

	glBindFramebufferEXT = (PFNGLBINDFRAMEBUFFEREXTPROC)wglGetProcAddress( "glBindFramebufferEXT" );
	sbError( glBindFramebufferEXT, "Failed to load \"glBindFramebufferEXT\"" );

	glFramebufferTexture2DEXT = (PFNGLFRAMEBUFFERTEXTURE2DEXTPROC)wglGetProcAddress( "glFramebufferTexture2DEXT" );
	sbError( glFramebufferTexture2DEXT, "Failed to load \"glFramebufferTexture2DEXT\"" );

	glDeleteFramebuffersEXT = (PFNGLDELETEFRAMEBUFFERSEXTPROC)wglGetProcAddress( "glDeleteFramebuffersEXT" );
	sbError( glDeleteFramebuffersEXT, "Failed to load \"glDeleteFramebuffersEXT\"" );

	glIsFramebufferEXT = (PFNGLISFRAMEBUFFEREXTPROC)wglGetProcAddress( "glIsFramebufferEXT" );
	sbError( glIsFramebufferEXT, "Failed to load \"glIsFramebufferEXT\"" );
	
	glCheckFramebufferStatusEXT = (PFNGLCHECKFRAMEBUFFERSTATUSEXTPROC)wglGetProcAddress( "glCheckFramebufferStatusEXT" );
	sbError( glCheckFramebufferStatusEXT, "Failed to load \"glCheckFramebufferStatusEXT\"" );

	glFramebufferRenderbufferEXT = (PFNGLFRAMEBUFFERRENDERBUFFEREXTPROC)wglGetProcAddress( "glFramebufferRenderbufferEXT" );
	sbError( glFramebufferRenderbufferEXT, "Failed to load \"glFramebufferRenderbufferEXT\"" );

	glGenRenderbuffersEXT = (PFNGLGENRENDERBUFFERSEXTPROC)wglGetProcAddress( "glGenRenderbuffersEXT" );
	sbError( glGenRenderbuffersEXT, "Failed to load \"glGenRenderbuffersEXT\"" );

	glBindRenderbufferEXT = (PFNGLBINDRENDERBUFFEREXTPROC)wglGetProcAddress( "glBindRenderbufferEXT" );
	sbError( glBindRenderbufferEXT, "Failed to load \"glBindRenderbufferEXT\"" );

	glRenderbufferStorageEXT = (PFNGLRENDERBUFFERSTORAGEEXTPROC)wglGetProcAddress( "glRenderbufferStorageEXT" );
	sbError( glRenderbufferStorageEXT, "Failed to load \"glRenderbufferStorageEXT\"" );

	glDeleteRenderbuffersEXT = (PFNGLDELETERENDERBUFFERSEXTPROC)wglGetProcAddress( "glDeleteRenderbuffersEXT" );
	sbError( glRenderbufferStorageEXT, "Failed to load \"glDeleteRenderbuffersEXT\"" );
}