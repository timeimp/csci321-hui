/*
===============================================================================

	Win32_Debug.h
		Functions and macros which assist with debugging.

===============================================================================
*/

#include "Framework/Sandbox.h"
#include "System/Timer.h"

#include <Windows.h>

using namespace sandbox::system;

/*
===============================================================================
	Global variables.
===============================================================================
*/
static LARGE_INTEGER gTimerFrequency;
static LARGE_INTEGER gTimerStart;

/*
===============================================================================
	Timer implemenation.
===============================================================================
*/

/*
===============================================================================
Timer::Start
	Begins the application timer.
===============================================================================
*/
void Timer::Start()
{
	QueryPerformanceFrequency( &gTimerFrequency );
	QueryPerformanceCounter( &gTimerStart );
}

/*
===============================================================================
Timer::Stop
	Stops the application timer.
===============================================================================
*/
void Timer::Stop()
{
	// Time stops for no man.
}

/*
===============================================================================
Timer::AppTime
	Returns the number of ticks that have elapsed since the application was launched.
===============================================================================
*/
tick_t Timer::AppTime()
{
	LARGE_INTEGER currentTime;
	QueryPerformanceCounter( &currentTime );
	return (tick_t)( (currentTime.QuadPart - gTimerStart.QuadPart) / (gTimerFrequency.QuadPart / 1000) );
}