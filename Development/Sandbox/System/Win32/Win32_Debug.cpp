/*
===============================================================================

	Win32_Debug.h
		Functions and macros which assist with debugging.

===============================================================================
*/

#include "Framework/Sandbox.h"

#include <string>
#include <sstream>
#include <Windows.h>

using namespace std;

/*
===============================================================================
	Code.
===============================================================================
*/

/*
===============================================================================
assertion_failure
	Alerts the user that there is something in the engine that has failed.
===============================================================================
*/
void assertion_failure( const char *pszExpression, const char *pszFile, int nLine )
{
	// Create the dialog box message.
	ostringstream sout;
	sout << "\"" << pszExpression << "\" failed!" << endl;
	sout << "Location: " << pszFile << " @ line " << nLine << endl;
	sout << endl;
	sout << "Attach debugger?" << endl;

	// Retrieve the message from the string stream.
	string error = sout.str();

	// Display the message.
	int nButton = MessageBox( NULL, error.c_str(), "Assertion Failure", MB_YESNO | MB_ICONERROR );

	if( nButton == IDYES )
	{
		breakpoint();
	}
	else
	{
		exit( 0 );
	}
}

/*
===============================================================================
message_box
	Alerts the user that there is something in the engine that has failed.
===============================================================================
*/
void message_box( const char *pszMessage )
{
	MessageBox( NULL, pszMessage, "Alert", MB_OK | MB_ICONWARNING );
}

/*
===============================================================================
breakpoint
	Causes program execution to stop and the source code to open.
===============================================================================
*/
void breakpoint()
{
	_asm
	{
		int		3
	}
}