/*
===============================================================================

	Thread.h
		Implements thread classes (mutex, thread, etc).

===============================================================================
*/

#include "Framework/Sandbox.h"
#include "System/Thread.h"

#include <Windows.h>

using namespace sandbox::system;

/*
===============================================================================
Mutex_Win32
	Win32 implementation of the Mutex interface.
===============================================================================
*/
class Mutex_Win32 : public Mutex
{
public:
					Mutex_Win32();
					~Mutex_Win32();

	/*
	-------------------------------------------------------------------------
		Mutex implementation.
	-------------------------------------------------------------------------
	*/
	virtual void	Lock();
	virtual bool	TryLock();
	virtual void	Unlock();
	virtual bool	IsLocked() const;

private:
	CRITICAL_SECTION	_cs;
	bool			_locked;
};

/*
===============================================================================
	Mutex implementation.
===============================================================================
*/

/*
===============================================================================
Mutex::Create
	Creates a new mutex instance.
===============================================================================
*/
Mutex *Mutex::Create()
{
	return new Mutex_Win32();
}

/*
===============================================================================
	Mutex_Win32 implementation.
===============================================================================
*/

/*
===============================================================================
Mutex_Win32::Mutex_Win32
	Initializes mutex instance.
===============================================================================
*/
Mutex_Win32::Mutex_Win32()
{
	BOOL success = InitializeCriticalSectionAndSpinCount( &_cs, 0x00000400 );
	sbAssert( success );

	_locked = false;
}

/*
===============================================================================
Mutex_Win32::~Mutex_Win32
	Cleans a mutex instance.
===============================================================================
*/
Mutex_Win32::~Mutex_Win32()
{
	DeleteCriticalSection( &_cs );
}

/*
===============================================================================
Mutex_Win32::Lock
	Locks the mutex. Blocks until the lock operation has been completed.
===============================================================================
*/
void Mutex_Win32::Lock()
{
	EnterCriticalSection( &_cs );
	_locked = true;
}

/*
===============================================================================
Mutex_Win32::TryLock
	Returns `false` is this mutex is already locked.
===============================================================================
*/
bool Mutex_Win32::TryLock()
{
	BOOL success = TryEnterCriticalSection( &_cs );

	if( success )
	{
		_locked = true;
		return true;
	}

	return false;
}

/*
===============================================================================
Mutex_Win32::Unlock
	Releases the mutex.
===============================================================================
*/
void Mutex_Win32::Unlock()
{
	_locked = false;
	LeaveCriticalSection( &_cs );
}

/*
===============================================================================
Mutex_Win32::IsLocked
	Determines if the mutex is locked.
===============================================================================
*/
bool Mutex_Win32::IsLocked() const
{
	return _locked;
}