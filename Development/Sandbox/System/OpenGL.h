/*
===============================================================================

	OpenGL.h
		Platform neutral OpenGL interface.

===============================================================================
*/

#ifndef __OPENGL_H__
#define __OPENGL_H__

#include "System/Window.h"

#ifdef sbPLATFORM_WINDOWS
	#include <Windows.h>
#endif

namespace sandbox
{
	namespace render
	{

		#include <gl/GL.h>

		#include "OpenGL/glext.h"

#ifdef sbPLATFORM_WINDOWS
		#include "System/Win32/Win32_OpenGL_Funcs.h"
#endif

		/*
		===============================================================================
		GlContext
			High level OpenGL context management.
		===============================================================================
		*/
		class GlContext
		{
		public:
			static void		Create( system::Window *pWindow );
			static void		Destroy();
			static void		Present();

		private:
			static system::Window *_pWindow;
		};

	} // namespace system;
} // namespace sandbox;

#endif // __OPENGL_H__