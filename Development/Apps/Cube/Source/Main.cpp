/*
===============================================================================

	Main.cpp
		A `Hello World` application for the Sandbox.

===============================================================================
*/

#include "Framework/Sandbox.h"
#include "Framework/Application.h"
#include "System/OpenGL.h"
#include "Render/Render.h"
#include "Render/Primitive.h"
#include "Math/Matrix.h"
#include "Math/Point.h"
#include "Math/Math.h"
#include "Drivers/OculusRift.h"

#include <fstream>

using namespace std;
using namespace sandbox::framework;
using namespace sandbox::system;
using namespace sandbox::render;
using namespace sandbox::drivers;
using namespace sandbox::math;

// TODO:
//	* Oculus warp shader.
//	* Proper Leap layer.
//
//	* Window Manager
//		* Processes Leap gestures.
//		* List of running windows.
//		* Alt-tab?
//	* Window
//		* Move, resize, minimize.
//	* Widgets
//		* Button, label, image.

/*
===============================================================================
CubeApplication
	Runs the application.
===============================================================================
*/
class CubeApplication : public Application
{
public:
	/*
	-------------------------------------------------------------------------
		 Creation\destruction.
	-------------------------------------------------------------------------
	*/
					CubeApplication( const appInit_t &init );

	/*
	-------------------------------------------------------------------------
		 Sandbox interface implementation.
	-------------------------------------------------------------------------
	*/
	virtual bool	AppInitialize();
	virtual bool	AppFrame();
	virtual void	AppDestroy();

private:
	/*
	-------------------------------------------------------------------------
		 Initialization.
	-------------------------------------------------------------------------
	*/
	void			SetupCamera();
	void			LoadDefaultProgram();
	void			LoadWarpProgram();
	void			InitWarpFb();

	void			RenderScene( OculusRift::oculusView_e view, const viewport_t &viewport, const viewport_t &fullscreenViewport );
	float			GetCubeRotation( float rps ) const;
	Color4f			GetCubeColor( float rps ) const;
	void			SetOculusWarpUniforms( OculusRift::oculusView_e view, const viewport_t &viewport, const viewport_t &fullscreenViewport );

	Camera			_mainCamera;
	Program			_warpProgram;
	Program			_generalProgram;
	Framebuffer		_warpFb;
};

/*
===============================================================================
CubeApplication::CubeApplication
	Default constructor.
===============================================================================
*/
CubeApplication::CubeApplication( const appInit_t &init ) : 
	Application( init )
{
}

/*
===============================================================================
CubeApplication::AppInitialize
	Allocates all resources for the application.
===============================================================================
*/
bool CubeApplication::AppInitialize()
{
	LoadDefaultProgram();
	LoadWarpProgram();
	InitWarpFb();
	return true;
}

/*
===============================================================================
CubeApplication::AppFrame
	Renders a single frame to the screen.
===============================================================================
*/
bool CubeApplication::AppFrame()
{
	const Window *pWindow = AppWindow();
	const bool OCULUS = true;

	const int windowWidth = pWindow->GetWidth();
	const int windowHeight = pWindow->GetHeight();

	Render &render = Render::Get();
	glClearColor( 1.0f, 0.0f, 0.0f, 1.0f );
	render.Clear( Render::CLEAR_COLOR_BIT | Render::CLEAR_DEPTH_BIT );
	glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );

	const viewport_t fullscreenVp( 0, 0, windowWidth, windowHeight );

	if( !OCULUS )
	{
		const viewport_t viewport( 0, 0, windowWidth, windowHeight );
		RenderScene( OculusRift::VIEW_CENTER, viewport, fullscreenVp );
	}
	else
	{
		viewport_t viewport( 0, 0, windowWidth / 2, windowHeight );
		RenderScene( OculusRift::VIEW_LEFT, viewport, fullscreenVp );

		viewport.Set( windowWidth / 2, 0, windowWidth / 2, windowHeight );
		RenderScene( OculusRift::VIEW_RIGHT, viewport, fullscreenVp );
	}

	return true;
}

/*
===============================================================================
CubeApplication::AppDestroy
	Releases all resources.
===============================================================================
*/
void CubeApplication::AppDestroy()
{

}

/*
===============================================================================
CubeApplication::SetupCamera
	Sets the camera default values.
===============================================================================
*/
void CubeApplication::SetupCamera()
{
	// Default position is the world origin.
	Point3f position( 0.0f, 0.0f, 0.0f );
	_mainCamera.SetPosition( position );

	// Default angles are straight ahead.
	Angle3f angles( 0.0f, 0.0f, 0.0f );
	_mainCamera.SetAngles( angles );

	// Misc values.
	_mainCamera.SetFov( d2r( 65.0f ) );
	_mainCamera.SetViewportFullscreen( true );
	_mainCamera.SetClippingPlane( 1.0f, 9999.9f );
}

/*
===============================================================================
CubeApplication::LoadDefaultProgram
	Loads the general purpose shader program into memory.
===============================================================================
*/
void CubeApplication::LoadDefaultProgram()
{
	const string vertexPath = "Data\\Shaders\\Default.vert";
	const string fragPath = "Data\\Shaders\\Default.frag";

	VertexShader vertex( vertexPath );
	sbAssert( vertex.IsValid() );

	FragmentShader frag( fragPath );
	sbAssert( frag.IsValid() );

	const string uniforms = "";
	_generalProgram.Init( "Default", vertex, frag, uniforms );
}

/*
===============================================================================
CubeApplication::LoadWarpProgram
	Loads the Oculus Warp shader program into memory.
===============================================================================
*/
void CubeApplication::LoadWarpProgram()
{
	const string vertexPath = "Data\\Shaders\\OculusWarp.vert";
	const string fragPath = "Data\\Shaders\\OculusWarp.frag";

	VertexShader vertex( vertexPath );
	sbAssert( vertex.IsValid() );

	FragmentShader frag( fragPath );
	sbAssert( frag.IsValid() );

	const string uniforms = "Texm;LensCenter;ScreenCenter;Scale;ScaleIn;HmdWarpParam;Texture0;";
	_warpProgram.Init( "OculusWarp", vertex, frag, uniforms );
}

/*
===============================================================================
CubeApplication::InitWarpFb
	Readies the cube framebuffer for use.
===============================================================================
*/
void CubeApplication::InitWarpFb()
{
	const Window *pWindow = AppWindow();
	_warpFb.Init( pWindow->GetWidth(), pWindow->GetHeight(), true );
}

/*
===============================================================================
CubeApplication::RenderScene
	Draws the scene given the oculus mode.
===============================================================================
*/
void CubeApplication::RenderScene( OculusRift::oculusView_e view, const viewport_t &viewport, const viewport_t &fullscreenViewport )
{
	Render &render = Render::Get();

	// Configure the camera.
	_mainCamera.SetViewport( viewport );
	_mainCamera.SetViewportFullscreen( false );
	render.SetCameraActive( _mainCamera );

	if( view != OculusRift::VIEW_CENTER )
	{
		// We only need to bind the warp FB in Oculus mode.
		// Default rendering goes straight to the screen.
		render.UnbindTexture();
		render.BindFramebuffer( _warpFb );
		render.Clear( Render::CLEAR_COLOR_BIT | Render::CLEAR_DEPTH_BIT );
	}

	// Setup rendering.
	render.BindProgram( _generalProgram );

	// Draw the cube.
	float rad = GetCubeRotation( 5.0f );
	Point3f center( 0.0f, 0.0f, -2.5f );
	Angle3f angles( rad, rad, rad );
	Color4f color = GetCubeColor( 0.0f );
	Primitive::Get().DrawRectangularPrism( center, angles, color, 0.5f, 2.0f, 0.5f );

	if( view != OculusRift::VIEW_CENTER )
	{
		// Apply the view warp in Oculus mode.
		render.UnbindFramebuffer();
		render.BindProgram( _warpProgram );

		Point2f tc1 = _warpFb.GetMaxTc();
		Point2f tc0;

		// HACKHACK: select the TCs of the current view on the FB.
		if( view == OculusRift::VIEW_LEFT )
		{
			tc0.Set( 0.0f, 0.0f );
			tc1.Set( tc1.GetX() / 2, tc1.GetY() );
		}
		else
		{
			tc0.Set( tc1.GetX() / 2, 0.0f );
			tc1.Set( tc1.GetX(), tc1.GetY() );
		}

		const Color4f color( 1.0f, 1.0f, 1.0f, 1.0f );

		SetOculusWarpUniforms( view, viewport, fullscreenViewport );

		render.DrawTexturedScreenOverlay( *_warpFb.GetTexture(), tc0, tc1, color );
	}
}

/*
===============================================================================
CubeApplication::GetCubeRotation
	Determines the current rotation of the cube given the current time and
	rotation speed.
===============================================================================
*/
float CubeApplication::GetCubeRotation( float rps ) const
{
	const tick_t ticksPerRotation = (tick_t)( rps * Application::TICKS_PER_SECOND );
	float frac = ( TickCount() % ticksPerRotation ) / i2fl( ticksPerRotation );
	return frac * ( 2.0f * db2fl( gPI ) );
}

/*
===============================================================================
CubeApplication::GetCubeColor
	Determines the cube color given the current time and rotation speed.
===============================================================================
*/
Color4f CubeApplication::GetCubeColor( float rps ) const
{
	static float r = 0.66f;
	static float g = 0.33f;
	static float b = 0.0f;

	r += 0.003f;
	g += 0.006f;
	b += 0.009f;

	// Use the cosine wave to modify the colors.
	// Transform the result from [-1, -1] to [0, 1].
	float cosr = ( cos( r ) + 1.0f ) / 2.0f;
	float cosg = ( cos( g ) + 1.0f ) / 2.0f;
	float cosb = ( cos( b ) + 1.0f ) / 2.0f;

	return Color4f( cosr, cosg, cosb, 1.0f );
}

/*
===============================================================================
CubeApplication::SetOculusWarpUniforms
	Uploads uniforms to the Oculus Warp shader.
===============================================================================
*/
void CubeApplication::SetOculusWarpUniforms( OculusRift::oculusView_e view, const viewport_t &viewport, const viewport_t &fullscreenViewport )
{
	Render &render = Render::Get();
	render.BindProgram( _warpProgram );

	OculusRift *pRift = GetOculusRift();
	float k[4];
	float x, y;

	pRift->SetDistortionViewport( viewport, fullscreenViewport );

	_warpProgram.SetUniform1i( "Texture0", 0 );

	pRift->GetDistortionLensCenter( &x, &y );
	_warpProgram.SetUniform2f( "LensCenter", x, y );

	pRift->GetDistortionScreenCenter( &x, &y );
	_warpProgram.SetUniform2f( "ScreenCenter", x, y );

	pRift->GetDistortionScale( &x, &y );
	_warpProgram.SetUniform2f( "Scale", x, y );

	pRift->GetDistortionScaleIn( &x, &y );
	_warpProgram.SetUniform2f( "ScaleIn", x, y );

	pRift->GetDistortionK( k );
	_warpProgram.SetUniform4fv( "HmdWarpParam", k );

	Matrix4f m = pRift->GetDistortionMatrix();
	_warpProgram.SetUniformMatrix4f( "Texm", m );
}

/*
===============================================================================
main
	Application entry point.
===============================================================================
*/
int main( int argc, char **argv )
{
	// Configure initialization parameters.
	appInit_t init;
	init.windowWidth = 1280;
	init.windowHeight = 800;
	init.windowTitle = "Cube";
	init.argc = argc;
	init.argv = argv;

	return app_main<CubeApplication>( init );
}