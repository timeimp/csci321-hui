/*
===============================================================================

	Main.cpp
		Demo application for Sandbox which displays all fingers detected by
		the Leap Motion sensor.

===============================================================================
*/

#include "Framework/Sandbox.h"
#include "Framework/Application.h"
#include "System/OpenGL.h"
#include "Render/Render.h"
#include "Render/Primitive.h"
#include "Math/Matrix.h"
#include "Math/Point.h"
#include "Math/Math.h"
#include "Math/Plane.h"
#include "Drivers/LeapMotion.h"

#include <iostream>

using namespace std;
using namespace Leap;
using namespace sandbox::framework;
using namespace sandbox::system;
using namespace sandbox::render;
using namespace sandbox::math;
using namespace sandbox::drivers;

/*
===============================================================================
LeapApplication
	Runs the application.
===============================================================================
*/
class LeapApplication : public Application
{
public:
	/*
	-------------------------------------------------------------------------
		 Creation\destruction.
	-------------------------------------------------------------------------
	*/
					LeapApplication( const appInit_t &init );

	/*
	-------------------------------------------------------------------------
		 Sandbox interface implementation.
	-------------------------------------------------------------------------
	*/
	virtual bool	AppInitialize();
	virtual bool	AppFrame();
	virtual void	AppDestroy();

private:
	/*
	-------------------------------------------------------------------------
		 Initialization.
	-------------------------------------------------------------------------
	*/
	void			SetupCamera();
	void			SetupDrawingBoard();
	void			LoadDefaultProgram();

	/*
	-------------------------------------------------------------------------
		 Frame processing.
	-------------------------------------------------------------------------
	*/
	void			ProcessAllGestures();
	void			ProcessSingleGesture( Gesture *pGesture );

	void			RenderAllHands();
	void			RenderSingleHand( const LeapHand &hand, const LeapHand &lastFrame );
	void			RenderAllFingers( const LeapHand &hand, const Point3f &handPosition );
	void			RenderSingleFinger( const LeapPointable &finger, const Point3f &handPosition, const Point3f &positionOffset );
	void			RenderDrawingPlaneIntersection( const LineSegment3f &fingerBone, const LineSegment3f &handBone );

	void			RenderDrawingBoard();

	/*
	-------------------------------------------------------------------------
		 Private accessors.
	-------------------------------------------------------------------------
	*/
	const LeapHand &	GetPreviousHand( int id );

	// Leap variables.
	LeapMotion		_leap;
	LeapFrame		_previousFrame;
	static const LeapHand EMPTY_HAND;

	// Drawing board.
	PlaneSegment	_drawingBoard;
	int				_fingersIntersectingBoard;

	// Misc rendering.
	Camera			_mainCamera;
	Program			_generalProgram;
};

const LeapHand LeapApplication::EMPTY_HAND;

/*
===============================================================================
LeapApplication::LeapApplication
	Default constructor.
===============================================================================
*/
LeapApplication::LeapApplication( const appInit_t &init ) : 
	Application( init )
{
}

/*
===============================================================================
LeapApplication::AppInitialize
	Allocates all resources for the application.
===============================================================================
*/
bool LeapApplication::AppInitialize()
{
	SetupCamera();
	SetupDrawingBoard();
	LoadDefaultProgram();
	return true;
}

/*
===============================================================================
LeapApplication::AppFrame
	Renders a single frame to the screen.
===============================================================================
*/
bool LeapApplication::AppFrame()
{
	Render &render = Render::Get();

	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	render.BindProgram( _generalProgram );
	render.SetCameraActive( _mainCamera );

	// Reset the drawing board state.
	_fingersIntersectingBoard = 0;

	ProcessAllGestures();
	RenderAllHands();
	RenderDrawingBoard();

	return true;
}

/*
===============================================================================
LeapApplication::AppDestroy
	Releases all resources.
===============================================================================
*/
void LeapApplication::AppDestroy()
{

}

/*
===============================================================================
LeapApplication::SetupCamera
	Sets the camera default values.
===============================================================================
*/
void LeapApplication::SetupCamera()
{
	// Default position is the world origin.
	Point3f position( 0.0f, 0.0f, 0.0f );
	_mainCamera.SetPosition( position );

	// Default angles are straight ahead.
	Angle3f angles( 0.0f, 0.0f, 0.0f );
	_mainCamera.SetAngles( angles );

	// Misc values.
	_mainCamera.SetFov( d2r( 65.0f ) );
	_mainCamera.SetViewportFullscreen( true );
	_mainCamera.SetClippingPlane( 1.0f, 9999.9f );
}

/*
===============================================================================
LeapApplication::SetupDrawingBoard
	Builds the drawing plane.
===============================================================================
*/
void LeapApplication::SetupDrawingBoard()
{
	// Place the drawing plane 50cm in front of the user.
	Point3f point( 0.0f, 0.0f, -25.0f );

	// Make the board face the user.
	Vector3f normal( 0.0f, 0.0f, 1.0f );

	_drawingBoard.Set( point, normal );

	// Make the drawing board measure 48cm * 30cm.
	_drawingBoard.SetExtent( 48.0f, 30.0f );
}

/*
===============================================================================
LeapApplication::LoadDefaultProgram
	Loads the general purpose shader program into memory.
===============================================================================
*/
void LeapApplication::LoadDefaultProgram()
{
	const string vertexPath = "Data\\Shaders\\Default.vert";
	const string fragPath = "Data\\Shaders\\Default.frag";

	VertexShader vertex( vertexPath );
	sbAssert( vertex.IsValid() );

	FragmentShader frag( fragPath );
	sbAssert( frag.IsValid() );

	const string uniforms = "";
	_generalProgram.Init( "Default", vertex, frag, uniforms );
}

/*
===============================================================================
LeapApplication::ProcessAllGestures
	Read all gestures from the Leap gesture queue.
===============================================================================
*/
void LeapApplication::ProcessAllGestures()
{
	// Get the gesture queue.
	LeapGestureQueue *pQueue = _leap.GetGestureQueue();
	pQueue->Lock();

	// Process all gestures.
	Gesture gesture = pQueue->Pop();
	while( gesture.isValid() )
	{
		ProcessSingleGesture( &gesture );

		// Get the next gesture.
		gesture = pQueue->Pop();
	}

	// Release the queue.
	pQueue->Unlock();
}

/*
===============================================================================
LeapApplication::ProcessSingleGesture
	Read all gestures from the Leap gesture queue.
===============================================================================
*/
void LeapApplication::ProcessSingleGesture( Gesture *pGesture )
{
	switch( pGesture->type() )
	{
		case Leap::Gesture::TYPE_SWIPE:
			cout << "TYPE_SWIPE" << endl;
			break;

		case Leap::Gesture::TYPE_CIRCLE:
			cout << "TYPE_CIRCLE" << endl;
			break;

		case Leap::Gesture::TYPE_SCREEN_TAP:
			cout << "TYPE_SCREEN_TAP" << endl;
			break;

		case Leap::Gesture::TYPE_KEY_TAP:
			cout << "TYPE_KEY_TAP" << endl;
			break;
	}
}

/*
===============================================================================
LeapApplication::RenderAllHands
	Read all gestures from the Leap gesture queue.
===============================================================================
*/
void LeapApplication::RenderAllHands()
{
	LeapFrame frame = _leap.GetFrame();
	const vector<LeapHand> &hands = frame.GetHands();

	for( unsigned int i = 0 ; i < hands.size() ; i++ )
	{
		const LeapHand &previousHand = GetPreviousHand( hands[i].GetId() );
		RenderSingleHand( hands[i], previousHand );
	}

	_previousFrame = frame;
}

/*
===============================================================================
LeapApplication::RenderSingleHand
	Draws a single detected hand to the screen.
===============================================================================
*/
void LeapApplication::RenderSingleHand( const LeapHand &hand, const LeapHand &lastFrame )
{
	// Average out the position from the this frame and the last.
	Point3f position = hand.GetPosition();
	Angle3f angles/* = hand.GetAngles()*/;

	const float XRANGE = 12.5f;
	const float YRANGE = 20.0f;
	const float ZRANGE = 10.0f;

	// Use the position of the cube as the basis of its color.
	// Transform the result of cos( x ) from [-1, 1] to [0, 1].
	float r = ( cos( position.GetX() / XRANGE ) + 1.0f ) / 2.0f;
	float g = ( cos( position.GetY() / YRANGE ) + 1.0f ) / 2.0f;
	float b = ( cos( position.GetZ() / ZRANGE ) + 1.0f ) / 2.0f;

	// Y displacement:	lower the rendering position of the cube to force users to keep 
	//					their hands inside the Leaps upside-down view pyramid.
	// Z displacement:	move the cube into the screen so that the cube is still in front 
	//					of the camera when the users hand is leaving the FOV.
	position += Point3f( 0.0f, -30.0f, -25.0f );

	// Draw cubes representing hands.
	Color4f color( r, g, b, 1.0f );
	Primitive &prim = Primitive::Get();
	prim.DrawCube( position, angles, color, 1.0f );

	// Draw the fingers associated with the hand.
	RenderAllFingers( hand, position );
}

/*
===============================================================================
LeapApplication::RenderFingers
	Draws all fingers currently detected on the given hand.
===============================================================================
*/
void LeapApplication::RenderAllFingers( const LeapHand &hand, const Point3f &handPosition )
{
	const Point3f offset = hand.GetPosition() - handPosition;
	const vector<LeapPointable> &pointables = hand.GetPointables();
	
	for( unsigned int i = 0 ; i < pointables.size() ; i++ )
	{
		const LeapPointable pointable = pointables[i];

		if( pointable.GetType() != LeapPointable::POINTABLE_FINGER )
		{
			continue;
		}

		RenderSingleFinger( pointable, handPosition, offset );
	}
}

/*
===============================================================================
LeapApplication::RenderSingleFinger
	Draws the finger as a single bone connected to the hand.
===============================================================================
*/
void LeapApplication::RenderSingleFinger( const LeapPointable &finger, const Point3f &handPosition, const Point3f &positionOffset )
{
	// Setup rendering parameters.
	const Point3f fingerTip = finger.GetTipPosition() - positionOffset;
	const Point3f fingerBase = finger.GetBasePosition() - positionOffset;
	Color4f color( 0.0f, 1.0f, 0.0f, 1.0f );
	Angle3f angles( 0.0f, 0.0f, 0.0f );

	// Draw the finger tip.
	Primitive &prim = Primitive::Get();
	prim.DrawCube( fingerTip, angles, color, 0.5f );

	// Draw the finger bone.
	color.Set( 0.0f, 0.0f, 1.0f, 1.0f );
	prim.DrawLine( fingerTip, fingerBase, color );

	// Connect the finger to the hand center.
	color.Set( 0.0f, 0.5f, 1.0f, 1.0f );
	prim.DrawLine( fingerBase, handPosition, color );

	// Check if this finger intersects with the drawing plane.
	LineSegment3f fingerBone( fingerTip, fingerBase );
	LineSegment3f handBone( fingerBase, handPosition );
	RenderDrawingPlaneIntersection( fingerBone, handBone );
}

/*
===============================================================================
LeapApplication::RenderDrawingPlaneIntersection
	Draws a dot in the position of the finger and drawing board intersection.
===============================================================================
*/
void LeapApplication::RenderDrawingPlaneIntersection( const LineSegment3f &fingerLine, const LineSegment3f &handLine )
{
	Point3f point;

	// Check for an intersection in the finger bone.
	bool intersection = _drawingBoard.LineSegmentIntersection( fingerLine, &point );

	if( !intersection )
	{
		// No finger intersection, check the hand bone.
		intersection = _drawingBoard.LineSegmentIntersection( handLine, &point );
	}

	// If either of the two intersected, visualize the intersected position.
	if( intersection )
	{
		Color4f color( 1.0f, 0.0f, 0.0f, 1.0f );
		Angle3f angles( 0.0f, 0.0f, 0.0f );

		// Draw a small cube where the fingers intersect with the plane.
		Primitive &prim = Primitive::Get();
		prim.DrawCube( point, angles, color, 0.5f );

		_fingersIntersectingBoard++;
	}
}

/*
===============================================================================
LeapApplication::RenderDrawingBoard
	Draws the drawing board with a small history intersections.
===============================================================================
*/
void LeapApplication::RenderDrawingBoard()
{
	// Determine the color the board should be drawn.
	// The board has a base color of ( 0, 0.5, 0 ).
	// For every finger intersecting the board all color components get 0.05x brighter.
	const float fingerColor = 0.05f * _fingersIntersectingBoard;
	const Color4f finalColor( fingerColor, 0.5f + fingerColor, fingerColor, 1.0f );

	// Draw the board.
	Primitive &prim = Primitive::Get();
	prim.DrawPlaneSegment( _drawingBoard, finalColor );
}

/*
===============================================================================
LeapApplication::GetPreviousHand
	Finds a hands state in the previous frame.
===============================================================================
*/
const LeapHand &LeapApplication::GetPreviousHand( int id )
{
	const vector<LeapHand> &hands = _previousFrame.GetHands();

	for( unsigned int i = 0 ; i < hands.size() ; i++ )
	{
		if( hands[i].GetId() == id )
		{
			return hands[i];
		}
	}

	// Didn't find the id we were looking for. We leave empty handed.
	return EMPTY_HAND;
}

/*
===============================================================================
main
	Application entry point.
===============================================================================
*/
int main( int argc, char **argv )
{
	// Configure initialization parameters.
	appInit_t init;
	init.windowWidth = 1280;
	init.windowHeight = 800;
	init.windowTitle = "Hello Leap";
	init.argc = argc;
	init.argv = argv;

	return app_main<LeapApplication>( init );
}